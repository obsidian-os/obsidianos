# Copyright (C) 2020-2021 Nicolas Fouquet
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see https://www.gnu.org/licenses.

include mk/colors.sh

all: msg clean user compile compile_rust_normal link cdrom qemu
test: msg clean user compile compile_rust_test link cdrom qemu_test
test_ci: msg clean user compile compile_rust_test link cdrom_ci qemu_test

user:
	@cd userspace ; sh mk.sh

compile:
	@nasm -f elf64 kernel/src/arch/x86_64/boot.asm -o build/asm/boot.o
	@nasm -f elf64 kernel/src/arch/x86_64/multiboot.asm -o build/asm/multiboot.o
	@nasm -f elf64 kernel/src/arch/x86_64/long_mode_init.asm -o build/asm/long_mode_init.o
	@nasm -f elf64 kernel/src/arch/x86_64/timer_int.asm -o build/asm/timer_int.o

compile_rust_normal:
	@cd kernel && RUST_TARGET_PATH="$(shell pwd)" xargo build --target x86_64-qemu-ObsidianOS && cd ..
	@cp kernel/target/x86_64-qemu-ObsidianOS/debug/libobsidianos_kernel.a build

compile_rust_test:
	@echo "${YELLOW}You are running tests!${NORMAL}" | tr -d "'"
	@cd kernel && RUST_TARGET_PATH="$(shell pwd)" xargo build --target x86_64-qemu-ObsidianOS --features test && cd ..
	@cp kernel/target/x86_64-qemu-ObsidianOS/debug/libobsidianos_kernel.a build

link:
	@sh mk/link.sh kernel

cdrom:
	@echo "${RED}We need to mount the disk in the /dev/loop0 path, so you will be prompted for your sudo password${NORMAL}" | tr -d "'"

	@# Make sure that loop devices are unplugged
	@sudo losetup -D

	@mkdir -p build/root
	@fallocate -l 1G build/disk.iso

	@echo -e "o\nn\np\n1\n\n\nw" | sudo fdisk -u -C2000000 -S63 -H16 build/disk.iso > /dev/null 2> /dev/null # Partition the disk

	@sudo losetup -o1048576 /dev/loop0 build/disk.iso
	@sudo mke2fs /dev/loop0 > /dev/null 2> /dev/null # Create an ext2 filesystem
	@sudo mount /dev/loop0 build/root

	@sudo cp -r root/* build/root/ # Copy files
	@sudo grub-install --root-directory=build/root --boot-directory=build/root/boot --no-floppy --modules="normal part_msdos ext2 multiboot biosdisk" build/disk.iso > "grub_log.txt" 2>&1
	@sudo cp build/kernel.bin build/root/boot/kernel.bin

	@sudo umount /dev/loop0
	@sudo losetup -d /dev/loop0

	@sudo chown $(USER):$(USER) build/disk.iso

cdrom_ci:
	@# Make sure that loop devices are unplugged
	@losetup -D

	@mkdir -p build/root
	@fallocate -l 1G build/disk.iso

	@echo -e "o\nn\np\n1\n\n\nw" | fdisk -u -C2000000 -S63 -H16 build/disk.iso > /dev/null 2> /dev/null # Partition the disk

	@losetup -o1048576 /dev/loop0 build/disk.iso
	@mke2fs /dev/loop0 > /dev/null 2> /dev/null # Create an ext2 filesystem
	@mount /dev/loop0 build/root

	@cp -r root/* build/root/ # Copy files
	@grub-install --root-directory=build/root --boot-directory=build/root/boot --no-floppy --modules="normal part_msdos ext2 multiboot biosdisk" build/disk.iso > "grub_log.txt" 2>&1
	@cp build/kernel.bin build/root/boot/kernel.bin

	@umount /dev/loop0
	@losetup -d /dev/loop0

qemu:
		@qemu-system-x86_64 -enable-kvm -m 3G -device ahci,id=ahci0\
		-drive if=none,file=build/disk.iso,format=raw,id=drive-sata0-0-0\
		-device ide-hd,bus=ahci0.0,drive=drive-sata0-0-0,id=sata0-0-0\
		-usb\
		-device usb-mouse\
		-serial stdio\
		-boot d

qemu_test:
		@echo "${YELLOW}Launch tests${NORMAL}" | tr -d "'"
		@-qemu-system-x86_64 -m 3G -device ahci,id=ahci0\
		-drive if=none,file=build/disk.iso,format=raw,id=drive-sata0-0-0\
		-device ide-hd,bus=ahci0.0,drive=drive-sata0-0-0,id=sata0-0-0\
		-serial stdio\
		-boot d\
		-display none\
		-device isa-debug-exit,iobase=0xf4,iosize=0x04

debug:
		@qemu-system-x86_64 -enable-kvm -m 3G -device ahci,id=ahci0\
		-drive if=none,file=build/disk.iso,format=raw,id=drive-sata0-0-0\
		-device ide-hd,bus=ahci0.0,drive=drive-sata0-0-0,id=sata0-0-0\
		-serial stdio\
		-boot d\
		-s\
		-S

debug-triple-fault:
		@qemu-system-x86_64 -m 3G -device ahci,id=ahci0\
		-drive if=none,file=build/disk.iso,format=raw,id=drive-sata0-0-0\
		-device ide-hd,bus=ahci0.0,drive=drive-sata0-0-0,id=sata0-0-0\
		-usb\
		-device usb-mouse\
		-serial stdio\
		-boot d\
		-d int\
		-no-reboot

clean:
	@rm -rf build
	@mkdir -p build/asm

	@rm -rf root/bin
	@mkdir -p root/bin

msg:
	@echo "${BLUE}################################${NORMAL}" | tr -d "'"
	@echo "${BLUE}#${GREEN} Thank you to use ObsidianOS! ${BLUE}#${NORMAL}" | tr -d "'"
	@echo "${BLUE}################################${NORMAL}" | tr -d "'"

install:
	sh mk/install.sh
