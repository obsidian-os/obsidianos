# ObsidianOS ![GPL](https://img.shields.io/badge/license-GPL%20(%3E%3D%203)-green) [![pipeline status](https://gitlab.com/obsidian-os/obsidianos/badges/master/pipeline.svg)](https://gitlab.com/obsidian-os/obsidianos/-/commits/master)
ObsidianOS is a POSIX compliant and multitasking OS written in Rust under GPL-3.0-or-later license.

### Installation
##### Build from source
You can build this OS from source. It takes ~15 minutes. To do this you can
follow the instructions below:

1. Clone the ObsidianOS repository: `git clone https://gitlab.com/obsidian-os/obsidianos`
2. Install all needed packages and compile the OS: `make install`
3. During the installation of Rustup (the Rust toolchain installer), you will be
   prompted to check if the options are good. Normally, you have just to choose
   `1) Proceed with installation (default)` by typing `1`.

Tested in (with `uname -a`):

* Linux 5.5.0-1-amd64 #1 SMP Debian 5.5.13-2 (2020-03-30) x86\_64 GNU/Linux
* Linux 4.4.0-169-generic #198-Ubuntu SMP Tue Nov 12 10:38:00 UTC 2019 x86\_64 GNU/Linux
* Linux 4.15.0-70-generic #79-Ubuntu SMP Tue Nov 12 10:36:11 UTC 2019 x86\_64 GNU/Linux  
Thanks to [Vagrant](https://www.vagrantup.com/).

##### Use the ISO
The ISO is built with Gitlab CI pipelines. You can download the latest ISO [here](https://gitlab.com/obsidian-os/obsidianos/-/jobs/artifacts/master/download?job=iso).
After the download, you can see the result in a virtual machine (QEMU for now)
by using the command (in the directory where there is the ISO):
```
qemu-system-x86_64 -enable-kvm -m 3G -device ahci,id=ahci0\
                -drive if=none,file=disk.iso,format=raw,id=drive-sata0-0-0\
                -device ide-hd,bus=ahci0.0,drive=drive-sata0-0-0,id=sata0-0-0\
                -usb\
                -device usb-mouse\
                -serial stdio\
                -boot d
```

### Development
If you need to get the documentation of the ObsidianOS crate, you can see it [here](https://obsidian-os.gitlab.io/obsidianos/obsidianos_kernel/).

### License
ObsidianOS is released under the GPL license.
