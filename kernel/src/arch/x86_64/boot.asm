; Copyright (C) 2020-2021 Nicolas Fouquet
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program. If not, see https://www.gnu.org/licenses.

global start
extern long_mode_start

section .text
bits 32
start:
    mov esp, stack_top - 0xc0000000
    mov edi, ebx

    call check_multiboot
    call check_cpuid
    call check_long_mode

    call set_up_page_tables
    call enable_paging

next:
    ; load the stack
    mov esp, stack_top

    ; load the 64-bit GDT
    lgdt [gdt64.pointer - 0xc0000000]

    jmp gdt64.code:long_mode_start
    hlt

; --- Check if the multiboot standard is supported ---
check_multiboot:
    cmp eax, 0x36d76289
    jne no_multiboot
    ret

check_cpuid:
    ; Check if CPUID is supported by attempting to flip the ID bit (bit 21)
    ; in the FLAGS register. If we can flip it, CPUID is available.

    ; Copy FLAGS in to EAX via stack
    pushfd
    pop eax

    ; Copy to ECX as well for comparing later on
    mov ecx, eax

    ; Flip the ID bit
    xor eax, 1 << 21

    ; Copy EAX to FLAGS via the stack
    push eax
    popfd

    ; Copy FLAGS back to EAX (with the flipped bit if CPUID is supported)
    pushfd
    pop eax

    ; Restore FLAGS from the old version stored in ECX (i.e. flipping the
    ; ID bit back if it was ever flipped).
    push ecx
    popfd

    ; Compare EAX and ECX. If they are equal then that means the bit
    ; wasn't flipped, and CPUID isn't supported.
    cmp eax, ecx
    je no_cpuid
    ret

check_long_mode:
    ; test if extended processor info in available
    mov eax, 0x80000000    ; implicit argument for cpuid
    cpuid                  ; get highest supported argument
    cmp eax, 0x80000001    ; it needs to be at least 0x80000001
    jb no_long_mode       ; if it's less, the CPU is too old for long mode

    ; use extended info to test if long mode is available
    mov eax, 0x80000001    ; argument for extended processor info
    cpuid                  ; returns various feature bits in ecx and edx
    test edx, 1 << 29      ; test if the LM-bit is set in the D-register
    jz no_long_mode       ; If it's not set, there is no long mode
    ret

set_up_page_tables:
    ; map P4 table recursively
    mov eax, p4_table - 0xc0000000
    or eax, 0b11 ; present + writable
    mov [(p4_table - 0xc0000000) + 511 * 8], eax

    ; map first P4 entry to P3 table
    mov eax, p3_table - 0xc0000000
    or eax, 0b11 ; present + writable
    mov [p4_table - 0xc0000000], eax

    ; map third P3 entry to P2 table
    mov eax, p2_table - 0xc0000000
    or eax, 0b11 ; present + writable
    mov [(p3_table - 0xc0000000) + 3 * 8], eax ; use the higher half kernel from 3G
    mov [(p3_table - 0xc0000000)], eax         ; keep the lower half kernel at 0. We will unmap it later (in memory/mod.rs:remap_kernel)

    ; map each P2 entry to a huge 2MiB page
    mov ecx, 0         ; counter variable
.map_p2_table:
    ; map ecx-th P2 entry to a huge page that starts at address 2MiB*ecx
    mov eax, 0x200000  ; 2MiB
    mul ecx            ; start address of ecx-th page
    or eax, 0b10000011 ; present + writable + huge
    mov [(p2_table - 0xc0000000) + ecx * 8], eax ; map ecx-th entry

    inc ecx            ; increase counter
    cmp ecx, 16        ; the kernel size is 0x1e00000 (see memory/mod.rs:KERNEL_SIZE), so 0x1e00000 / 0x200000 = 15. We need also to map the kernel heap, so 15 + 1 = 16
    jne .map_p2_table  ; else map the next entry

    ret

enable_paging:
    ; load P4 to cr3 register (cpu uses this to access the P4 table)
    mov eax, (p4_table - 0xc0000000)
    mov cr3, eax

    ; enable PAE-flag in cr4 (Physical Address Extension)
    mov eax, cr4
    or eax, 1 << 5
    mov cr4, eax

    ; set the long mode bit in the EFER MSR (model specific register)
    mov ecx, 0xC0000080
    rdmsr
    or eax, 1 << 8
    wrmsr

    ; enable paging in the cr0 register
    mov eax, cr0
    or eax, 1 << 31
    mov cr0, eax

    jmp next

; --- Errors ---
error:
    hlt

no_multiboot:
    mov al, "0"
    jmp error

no_cpuid:
    mov al, "1"
    jmp error

no_long_mode:
    mov al, "2"
    jmp error

; --- GDT ---
section .rodata
gdt64:
    dq 0 ; zero entry
.code: equ $ - gdt64
    dq (1<<43) | (1<<44) | (1<<47) | (1<<53) ; code segment
.pointer:
    dw $ - gdt64 - 1
    dq gdt64

; --- Stack and page table ---
section .bss
align 4096
p4_table:
    resb 4096
p3_table:
    resb 4096
p2_table:
    resb 4096
stack_bottom:
    resb 1024 * 124
stack_top:
