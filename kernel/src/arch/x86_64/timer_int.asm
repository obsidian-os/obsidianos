; Copyright (C) 2020-2021 Nicolas Fouquet
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see https://www.gnu.org/licenses.
global timer_interrupt_handler
extern next

section .text
bits 64
timer_interrupt_handler:
  ; Push registers on the stack
  push rax
  push rbx
  push rcx
  push rdx
  push rdi
  push rsi
  push r8
  push r9
  push r10
  push r11
  push r12
  push r13
  push r14
  push r15
  push rbp
  mov r11, cr3
  push r11

  ; Save the start of the registers' stack
  mov rdi, rsp
  sub rsp, 0x88

  ; Switch to the next process
  call next

  ; Sometimes (for example, when a system call is interrupted and that a lock is
  ; held), we need to return
  ret
