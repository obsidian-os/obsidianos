/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Boot informations management
use multiboot2::BootInformation;
use spin::Once;

/// A structure which contains the boot informations
///
/// This structure is just a Sync + Send helper to make BootInformation safe between threads
pub struct BootInfo(pub BootInformation);

impl BootInfo {
    /// Get the inner BootInformation struct
    pub fn get(&'static self) -> &'static BootInformation { &self.0 }

    /// Reload the inner structure with a new address
    pub unsafe fn set(&mut self, addr: usize) { self.0 = multiboot2::load(addr); }
}

unsafe impl Sync for BootInfo {}
unsafe impl Send for BootInfo {}

/// A global BootInformations
///
/// This structure contains all informations passed from the bootloader to the kernel, like:
/// *  The memory areas (used by the frame allocator)
/// *  The sections of the kernel
/// *  The VBE tags and structures
/// *  The command line used to boot the kernel
pub static BOOT_INFO: Once<BootInfo> = Once::new();
