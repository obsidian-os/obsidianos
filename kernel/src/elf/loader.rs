/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! A loader which loads ELF static binaries
use alloc::{
    collections::btree_map::BTreeMap,
    prelude::v1::{String, Vec, Box},
    sync::Arc,
};
use x86_64::structures::paging::page_table::PageTableFlags as Flags;
use goblin::elf::{
    program_header::{PT_LOAD, PT_PHDR, ProgramHeader},
    Elf,
};

use crate::memory::{self, PAGE_SIZE};
use crate::tasking::{
    info::{AuxVecItem, ProcessInfo},
    process::{DATA_SEGMENT_SIZE, SIGNAL_STACK_SIZE, STACK_SIZE, ProcessBuilder},
    memory::{MemoryArea, handlers::FileHandler},
};
use crate::fs::{
    ROOT,
    file::{FileHandle, OpenFlags},
};
use super::ElfError;

trait GetFlags {
    fn flags(&self) -> Flags;
}

impl GetFlags for ProgramHeader {
    fn flags(&self) -> Flags {
        let mut flags = Flags::NO_EXECUTE;

        if self.is_read() {
            flags.insert(Flags::PRESENT);
        }

        if self.is_write() {
            flags.insert(Flags::WRITABLE);
        }

        if self.is_executable() {
            flags.remove(Flags::NO_EXECUTE);
        }

        flags
    }
}

#[derive(PartialEq, Debug, Clone, Copy)]
/// An ELF check error
pub enum ElfCheckError {
    /// The header's magic (must be `0x7f, 'E', 'L', 'F'`) is invalid
    InvalidMagic,

    /// The binary contains section/s address/es which is/are under the limit of `0x40000000`
    AboveLimit,
}

#[derive(Debug)]
/// An ELF loader
pub struct ElfLoader<'a> {
    /// The binary parsed by the `goblin` crate
    binary: Elf<'a>,

    /// The content of the binary
    data: &'a [u8],

    /// The path of the binary
    path: &'a str,

    /// The process which is created during the loading of the binary
    process: Option<ProcessBuilder>,
}

/// The ELF magic
static MAGIC: [u8; 4] = [0x7f, 'E' as u8, 'L' as u8, 'F' as u8];

impl<'a> ElfLoader<'a> {
    /// Create a new ElfLoader with the specified binary
    pub fn new(binary: Elf<'a>, data: &'a [u8], path: &'a str) -> Self {
        Self {
            binary,
            data,
            path,
            process: None,
        }
    }

    /// Check the binary
    pub fn check(&self) -> Result<(), ElfCheckError> {
        // Verify the magic
        if self.binary.header.e_ident[0..4] != MAGIC {
            return Err(ElfCheckError::InvalidMagic);
        }

        // Verify that all segment are under the limit
        for ph in &self.binary.program_headers {
            if ph.p_type == PT_LOAD {
                let end = ph.p_vaddr + ph.p_memsz as u64;

                if end > memory::KERNEL_START_VIRT {
                    return Err(ElfCheckError::AboveLimit);
                }
            }
        }

        Ok(())
    }

    /// Create a new ProcessInfo
    pub fn info(&self, args: Vec<String>, envs: Vec<String>) -> ProcessInfo {
        let mut auxv = BTreeMap::new();

        if let Some(phdr) = self
            .binary
            .program_headers
            .iter()
            .find(|ph| ph.p_type == PT_PHDR)
        {
            auxv.insert(AuxVecItem::PHDR as u8, phdr.p_vaddr as usize);
        } else if let Some(elf_addr) = self
            .binary
            .program_headers
            .iter()
            .find(|ph| ph.p_type == PT_LOAD && ph.p_offset == 0)
        {
            auxv.insert(
                AuxVecItem::PHDR as u8,
                (elf_addr.p_vaddr + self.binary.header.e_phoff) as usize,
            );
        } else {
            println!("No program header found!");
        }
        auxv.insert(
            AuxVecItem::PHENT as u8,
            self.binary.header.e_phentsize as usize,
        );
        auxv.insert(AuxVecItem::PHNUM as u8, self.binary.header.e_phnum as usize);
        auxv.insert(AuxVecItem::PAGESZ as u8, PAGE_SIZE);
        auxv.insert(AuxVecItem::ENTRY as u8, self.binary.header.e_entry as usize);

        ProcessInfo {
            args,
            envs,
            auxv,
            first_arg_addr: 0,
        }
    }

    /// Load the binary in memory
    pub fn load(&mut self, args: Vec<String>, envs: Vec<String>) -> Result<(), ElfError> {
        self.process = Some(ProcessBuilder::new(self.binary.entry as usize, self.info(args, envs)));

        for ph in &self.binary.program_headers {
            if ph.p_type == PT_LOAD {
                let area = MemoryArea::new(
                    ph.p_vaddr,
                    ph.p_vaddr + ph.p_memsz,
                    ph.flags(),
                    Arc::new(FileHandler {
                        file: FileHandle::open(Box::leak(String::from(self.path).into_boxed_str()), OpenFlags::RDONLY, 0o777)?,
                        mem_start: ph.p_vaddr,
                        file_start: ph.p_offset,
                        file_end: ph.p_offset + ph.p_filesz,
                    }),
                );

                // Map the area in the process' page table
                area.map(&mut self.process.as_mut().unwrap().memory()).unwrap();
            }
        }

        // Collect all addresses
        let start_image = self.process.as_ref().unwrap().process.memory.areas.first().unwrap().start;
        let end_image = self.process.as_ref().unwrap().process.memory.areas.last().unwrap().end;

        let start_data_segment = end_image;
        let end_data_segment = start_data_segment + DATA_SEGMENT_SIZE as u64 + PAGE_SIZE as u64;

        let start_stack = end_data_segment;
        let end_stack = start_stack + STACK_SIZE as u64 + PAGE_SIZE as u64;

        let start_signal_stack = end_stack;
        let end_signal_stack = start_signal_stack + SIGNAL_STACK_SIZE as u64;

        // Create the stack and the data segment after the creation of the executable image
        self.process.as_mut().unwrap().create_data_segment(start_data_segment);
        self.process.as_mut().unwrap().create_stack(start_stack);
        self.process.as_mut().unwrap().create_signal_stack(start_signal_stack);

        // Set the addresses in the struct ProcessMemory
        let mut memory = self.process.as_mut().unwrap().memory();
        memory.image = Some((start_image, end_image));
        memory.data_segment = Some((start_data_segment, end_data_segment));
        memory.stack = Some((start_stack, end_stack));
        memory.signal_stack = Some((start_signal_stack, end_signal_stack));

        if let Some(interpreter) = self.binary.interpreter {
            self.load_interpreter(interpreter, end_signal_stack)?;
        }

        println!("Binary {}:\n  - | Executable image | Start: {:#x}, End: {:#x}\n  - | Data segment | Start: {:#x}, End: {:#x}\n  - | Stack | Start: {:#x}, End: {:#x}",
            self.path,
            start_image, end_image,
            start_data_segment, end_data_segment,
            start_stack, end_stack);

        Ok(())
    }

    /// Load the interpreter program
    fn load_interpreter(&mut self, path: &'a str, load_addr: u64) -> Result<(), ElfError> {
        use alloc::prelude::v1::ToString;
        let path = path.to_string();
        let inode = ROOT
            .read()
            .open(path.as_str())
            .unwrap();

        let mut inode = inode.write();
        let size = inode.metadata()?.size;
        let data = inode.read(0, size)?;

        match Elf::parse(data.as_slice()) {
            Ok(binary) => {
                for ph in &binary.program_headers {
                    if ph.p_type == PT_LOAD {
                        let area = MemoryArea::new(
                            load_addr + ph.p_vaddr,
                            load_addr + ph.p_vaddr + ph.p_memsz,
                            ph.flags(),
                            Arc::new(FileHandler {
                                file: FileHandle::open(Box::leak(String::from(path.clone()).into_boxed_str()), OpenFlags::RDONLY, 0o777)?,
                                mem_start: load_addr + ph.p_vaddr,
                                file_start: ph.p_offset,
                                file_end: ph.p_offset + ph.p_filesz,
                            }),
                        );

                        // Map the area in the process' page table
                        area.map(&mut self.process.as_mut().unwrap().memory()).unwrap();
                    }
                }

                // Change the entry point
                self.process.as_mut().unwrap().thread.get_kstack().rip = (load_addr + binary.entry) as usize;

                println!("Interpreter {} successfully loaded at {:#x}!", path, load_addr);
                Ok(())
            },
            Err(e) => {
                Err(e.into())
            }
        }
    }

    /// Get the created process
    pub fn get_process(&self) -> ProcessBuilder {
        if let Some(process) = self.process.clone() {
            process
        } else {
            panic!("You must call `ElfLoader::load` before getting the process!");
        }
    }

    /// Add the created process to the list of processes
    pub fn add(&mut self) {
        self.process.as_ref().unwrap().build();
    }
}
