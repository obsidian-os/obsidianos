/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! ELF manipulation functions: check, load, run

use alloc::prelude::v1::{Vec, String};
use goblin::{
    elf::Elf,
    error::Error,
};

pub mod loader;

use crate::fs::{ROOT, error::FSError};
use self::loader::{ElfLoader, ElfCheckError};

#[derive(Debug)]
/// This structure describes an error happened during the loading of an ELF binary
pub enum ElfError {
    /// A filesystem error happened
    FSError(FSError),

    /// An error happened during the checks of the ELF binary
    CheckError(ElfCheckError),

    /// An error happened during the checks done by the [Goblin](https://github.com/m4b/goblin) library
    GoblinError(Error),
}

impl From<FSError> for ElfError {
    fn from(err: FSError) -> Self {
        Self::FSError(err)
    }
}

impl From<ElfCheckError> for ElfError {
    fn from(err: ElfCheckError) -> Self {
        Self::CheckError(err)
    }
}

impl From<Error> for ElfError {
    fn from(err: Error) -> Self {
        Self::GoblinError(err)
    }
}

/// Check, load and run a static linked binary
pub fn load(path: &'static str, args: Vec<String>, envs: Vec<String>) -> Result<(), ElfError> {
    let inode = ROOT
        .read()
        .open(path)?;

    let mut inode = inode.write();
    let size = inode.metadata()?.size;
    let data = inode.read(0, size)?;

    // TODO: Don't read all the file, just read the ELF header
    // The problem here is that goblin needs to get the dynamic section which is not in the header,
    // so it crashes when we read just the ELF header
    match Elf::parse(data.as_slice()) {
        Ok(binary) => {
            let mut loader = ElfLoader::new(binary, data.as_slice(), path);
            if let Err(e) = loader.check() {
                Err(e.into())
            } else {
                loader.load(args, envs)?;
                loader.add();
                Ok(())
            }
        }
        Err(e) => {
            Err(e.into())
        }
    }
}
