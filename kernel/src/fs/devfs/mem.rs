/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! The `/dev/mem` file represents the physical memory and is used to map virtual to physical
//! memory. Obviously, only the `root` user is allowed to access this file.
use alloc::prelude::v1::Vec;
use x86_64::{
    structures::paging::{Page, PhysFrame, Size4KiB,},
    PhysAddr, VirtAddr,
};

use crate::memory::PAGE_SIZE;
use crate::tasking::memory::{MemoryArea, ProcessMemory};
use crate::fs::{
    error::FSError,
    vfs::{Inode, Metadata, FileType},
};

#[derive(Debug, Default)]
/// An inode in the TtyFS filesystem
pub struct MemInode {}

impl Inode for MemInode {
    fn read(&mut self, _offset: usize, _count: usize) -> Result<Vec<u8>, FSError> {
        // TODO: Read from /dev/mem
        Ok(vec![])
    }

    fn write(&mut self, _offset: usize, _data: &[u8]) -> Result<usize, FSError> {
        // TODO: Write to /dev/mem
        Ok(0)
    }

    fn metadata(&self) -> Result<Metadata, FSError> {
        Ok(Metadata {
            size: 0,
            mode: 0o666,
            type_: FileType::CharDevice,
            inode: 0,
        })
    }

    fn mmap(&self, offset: usize, area: &mut MemoryArea, mem: &mut ProcessMemory) -> Result<(), FSError> {
        // TODO: Check for UID=0

        // As this file represents the wide memory, the offset inside this file is the physical
        // address requested
        let phys_start = offset as u64;
        let virt_start = area.start;
        let virt_end = area.end;

        for (i, page) in Page::<Size4KiB>::range_inclusive(
            Page::containing_address(VirtAddr::new(virt_start)),
            Page::containing_address(VirtAddr::new(virt_end - 1)),
        ).enumerate() {
            let frame = PhysFrame::containing_address(PhysAddr::new(phys_start + i as u64 * PAGE_SIZE as u64));
            mem.table.map(page, frame, area.flags)?;
        }

        mem.areas.push(area.clone());

        Ok(())
    }
}
