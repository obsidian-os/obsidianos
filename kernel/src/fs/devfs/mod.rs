/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! The `/dev` filesystem is a Linux specific filesystem used to manage (block and char) devices as
//! there were just files. It is the origin of the famous `everything is a file`.
use alloc::{
    collections::btree_map::BTreeMap,
    sync::Arc,
    prelude::v1::Vec,
};
use spin::RwLock;

use crate::fs::{
    vfs::{Inode, Filesystem, Metadata, FileType},
    error::FSError,
};

pub mod tty;
pub mod mem;

#[derive(Debug)]
/// The `/dev` filesystem is a Linux specific filesystem used to manage (block and char) devices as
/// there were just files. It is the origin of the famous `everything is a file`.
pub struct DevFS {
    root_inode: Arc<RwLock<DevRoot>>,
}

#[derive(Debug)]
/// The root inode of the DevFS
pub struct DevRoot {
    devices: BTreeMap<&'static str, Arc<RwLock<dyn Inode>>>,
}

impl DevFS {
    /// Create a new DevFS
    pub fn new() -> Self {
        Self {
            root_inode: Arc::new(RwLock::new(DevRoot {
                devices: BTreeMap::new(),
            })),
        }
    }

    /// Add a new device in the DevFS
    pub fn add(&self, name: &'static str, inode: Arc<RwLock<dyn Inode>>) -> Result<(), FSError> {
        if self.root_inode.read().devices.contains_key(name) {
            return Err(FSError::Exists);
        }

        self.root_inode.write().devices.insert(name, inode);
        Ok(())
    }

    /// Remove a device from the DevFS
    pub fn remove(&self, name: &'static str) -> Result<(), FSError> {
        if !self.root_inode.read().devices.contains_key(name) {
            return Err(FSError::NotFound);
        } else {
            self.root_inode.write().devices.remove(name);
            Ok(())
        }
    }
}

impl Filesystem for DevFS {
    fn name(&self) -> &'static str {
        "DevFS"
    }

    fn root_inode(&self) -> Result<Arc<RwLock<dyn Inode>>, FSError> {
        Ok(self.root_inode.clone())
    }
}

impl Inode for DevRoot {
    fn read(&mut self, _offset: usize, _count: usize) -> Result<Vec<u8>, FSError> {
        Err(FSError::IsDir)
    }

    fn write(&mut self, _offset: usize, _data: &[u8]) -> Result<usize, FSError> {
        Err(FSError::IsDir)
    }

    fn metadata(&self) -> Result<Metadata, FSError> {
        Ok(Metadata {
            size: self.devices.len(),
            mode: 0o666,
            type_: FileType::Dir,
            inode: 0,
        })
    }

    fn search(&self, name: &str) -> Result<Arc<RwLock<dyn Inode>>, FSError> {
        match name {
            "." | ".." => Ok(Arc::new(RwLock::new(DevRoot { devices: self.devices.clone() }))),
            _ => {
                self.devices
                    .get(name)
                    .ok_or(FSError::NotFound)
                    .and_then(|t| Ok(t.clone() as Arc<RwLock<dyn Inode>>))
            },
        }
    }
}
