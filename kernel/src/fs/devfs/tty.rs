/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Terminal filesystem (linked with the keyboard)
use alloc::{
    prelude::v1::Vec,
    sync::Arc,
};
use core::str::from_utf8;
use lazy_static::lazy_static;
use spin::RwLock;

use crate::fs::{
    error::FSError,
    vfs::{Inode, Metadata, FileType},
};

lazy_static! {
    /// A global terminal in/output
    pub static ref TTY: Arc<RwLock<TtyInode>> = Arc::new(RwLock::new(TtyInode::default()));
}

#[derive(Debug, Default)]
/// An inode in the TtyFS filesystem
pub struct TtyInode {
    buffer: Vec<u8>,
}

impl TtyInode {
    /// Fill the buffer with a character
    pub fn add(&mut self, ch: u8) {
        self.buffer.push(ch);
    }
}

impl Inode for TtyInode {
    fn read(&mut self, _offset: usize, _count: usize) -> Result<Vec<u8>, FSError> {
        if let Some(ch) = self.buffer.pop() {
            Ok(vec![ch])
        } else {
            // TODO: Poll until EOF
            Ok(vec![])
        }
    }

    fn write(&mut self, _offset: usize, data: &[u8]) -> Result<usize, FSError> {
        // For debugging:
        let content = from_utf8(data).expect("cannot transform to utf8");
        print!("{}", content);
        Ok(data.len())
    }

    fn metadata(&self) -> Result<Metadata, FSError> {
        Ok(Metadata {
            size: 0,
            mode: 0o666,
            type_: FileType::CharDevice,
            inode: 0,
        })
    }
}
