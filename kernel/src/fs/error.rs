/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Defines some error types for the filesystem manipulations
use x86_64::structures::paging::{mapper::MapToError, Size4KiB};

use crate::syscall::error::Error;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
/// A structure describing an error happened in the filesystem
pub enum FSError {
    /// No mount point found for the path
    NoMountPoint,

    /// The file is not found
    NotFound,

    /// This filesystem does not support this action
    NotSupported,

    /// The file or directory already exists
    Exists,

    /// The action cannot be done on a directory
    IsDir,

    /// The action cannot be done on an other thing than a directory
    NotDir,

    /// The name requested contains characters not permitted
    BadName,

    /// The file is not opened to do this action
    BadRights,

    /// The user has not enough rights to do this action
    NotAllowed,

    /// Error during the mapping of the file in memory
    MapError,
}

impl From<FSError> for Error {
    fn from(err: FSError) -> Error {
        match err {
            FSError::NoMountPoint => Error::ENOENT,
            FSError::NotFound => Error::ENOENT,
            FSError::NotSupported => Error::ENOSYS,
            FSError::Exists => Error::EEXIST,
            FSError::IsDir => Error::EISDIR,
            FSError::NotDir => Error::ENOTDIR,
            FSError::BadName => Error::EINVAL,
            FSError::BadRights => Error::EBADF,
            FSError::NotAllowed => Error::EACCES,
            FSError::MapError => Error::ENOMEM,
        }
    }
}

impl From<MapToError<Size4KiB>> for FSError {
    fn from(_: MapToError<Size4KiB>) -> FSError {
        FSError::MapError
    }
}
