/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Filesystem structures
//! *  Handle
//! *  Handle ID
//! *  Permissions
use alloc::{
    sync::Arc,
    prelude::v1::Vec,
};
use spin::RwLock;
use core::fmt;
use bitflags::bitflags;

use crate::tasking::memory::{
    MemoryArea,
    ProcessMemory,
    handlers::FileHandler,
};
use super::{
    ROOT,
    vfs::{Inode, Metadata, FileType},
    error::FSError,
};

bitflags! {
    /// `Mode` field of the StatInfo structure
    pub struct Perms: u32 {
        /// Null
        const NULL  = 0;

        /// Type
        const TYPE_MASK = 0o170000;

        /// FIFO
        const FIFO  = 0o010000;

        /// Character device
        const CHAR  = 0o020000;

        /// Directory
        const DIR   = 0o040000;

        /// Block device
        const BLOCK = 0o060000;

        /// Ordinary regular file
        const FILE  = 0o100000;

        /// Symbolic link
        const LINK  = 0o120000;

        /// Socket
        const SOCKET = 0o140000;

        /// Set-user-ID on execution.
        const SET_UID = 0o4000;

        /// Set-group-ID on execution.
        const SET_GID = 0o2000;

        /// Read, write, execute/search by owner.
        const OWNER_MASK = 0o700;

        /// Read permission, owner.
        const OWNER_READ = 0o400;

        /// Write permission, owner.
        const OWNER_WRITE = 0o200;

        /// Execute/search permission, owner.
        const OWNER_EXEC = 0o100;

        /// Read, write, execute/search by group.
        const GROUP_MASK = 0o70;

        /// Read permission, group.
        const GROUP_READ = 0o40;

        /// Write permission, group.
        const GROUP_WRITE = 0o20;

        /// Execute/search permission, group.
        const GROUP_EXEC = 0o10;

        /// Read, write, execute/search by others.
        const OTHER_MASK = 0o7;

        /// Read permission, others.
        const OTHER_READ = 0o4;

        /// Write permission, others.
        const OTHER_WRITE = 0o2;

        /// Execute/search permission, others.
        const OTHER_EXEC = 0o1;
    }
}

impl From<FileType> for Perms {
    fn from(type_: FileType) -> Self {
        match type_ {
            FileType::File => Perms::FILE,
            FileType::Dir => Perms::DIR,
            FileType::SymLink => Perms::LINK,
            FileType::CharDevice => Perms::CHAR,
            FileType::BlockDevice => Perms::BLOCK,
            FileType::Socket => Perms::SOCKET,
            FileType::NamedPipe => Perms::FIFO,
        }
    }
}

impl Perms {
    /// Split the `Perms` structure into a type of file and a mode
    pub fn split(&self) -> (FileType, u16) {
        // Extract the type
        let type_ = FileType::from(*self);

        // Extract the mode (we need to remove the type)
        let mut mode = self.clone();
        mode.remove(Perms::FILE);
        mode.remove(Perms::DIR);
        mode.remove(Perms::LINK);
        mode.remove(Perms::CHAR);
        mode.remove(Perms::BLOCK);
        mode.remove(Perms::SOCKET);
        mode.remove(Perms::FIFO);

        (type_, mode.bits() as u16)
    }
}

bitflags! {
    /// The flags used during a call to the `open` syscall kept in the `FileHandle` structure
    pub struct OpenFlags: u32 {
        // --- Permissions ---

        /// Read only
        const RDONLY = 0;

        /// Write only
        const WRONLY = 1;

        /// Read and write
        const RDWR = 2;

        // --- Various flags ---

        /// Append instead of rewrite
        const APPEND = 1024;

        /// Create the file if it does not exists
        const CREAT = 64;

        /// TODO: Not supported for now
        const EXCL = 128;

        /// TODO: Not supported for now
        const NOCTTY = 256;

        /// TODO: Not supported for now
        const NONBLOCK = 2048;

        /// TODO: Not supported for now
        const SYNC = 1052672;

        /// TODO: Not supported for now
        const RSYNC = 1052672;

        /// TODO: Not supported for now
        const DSYNC = 4096;

        /// TODO: Not supported for now
        const FSYNC = 0x101000;

        /// TODO: Not supported for now
        const NOATIME = 0o1000000;

        /// TODO: Not supported for now
        const PATH = 0o10000000;

        /// TODO: Not supported for now
        const TMPFILE = 0o20000000 | 0x10000;

        /// TODO: Not supported for now
        const DIRECT = 0x4000;

        /// TODO: Not supported for now
        const DIRECTORY = 0x10000;

        /// TODO: Not supported for now
        const NOFOLLOW = 0x20000;
    }
}

#[derive(Clone)]
/// A handle to a file
pub struct FileHandle {
    /// The node of the handle
    pub inode: Arc<RwLock<dyn Inode>>,

    /// The offset in the file
    pub offset: usize,

    /// Flags of this FileHandle (see OpenFlags)
    pub flags: OpenFlags,
}

impl FileHandle {
    /// Open a new file (or create it)
    pub fn open(path: &'static str, flags: OpenFlags, mode: u16) -> Result<Self, FSError> {
        let root = ROOT.read();
        let inode = match root.open(path) {
            Ok(i) => {
                // TODO: Check that the user have the rights to open this inode with the requested
                // flags (get the current user and group)
                i
            },
            Err(e) => {
                match e {
                    FSError::NotFound => {
                        // The inode is not found but if the flag O_CREAT is set, we need to create
                        // it
                        //
                        // TODO: Don't do this stuff after the call to `root.open`, do it at the
                        // start of this function because if the flag O_CREAT is set, the
                        // `root.open` is useless
                        if flags.contains(OpenFlags::CREAT) {
                            // The flag O_CREAT is set, get the parent directory of the inode
                            let mut path = path.split("/").collect::<Vec<&str>>();
                            if let Some(name) = path.pop() {
                                // Try to open the parent directory
                                match root.open(path.join("/").as_str()) {
                                    Ok(i) => {
                                        // The parent directory exists, create the inode and return it
                                        i.write().create(name, FileType::File, mode)?
                                    },
                                    Err(e) => {
                                        // The parent directory does not exist (or other error), abort
                                        return Err(e);
                                    },
                                }
                            } else {
                                return Err(FSError::NotFound);
                            }
                        } else {
                            // The flag O_CREAT is not set, return NotFound
                            return Err(FSError::NotFound);
                        }
                    },
                    _ => return Err(e),
                }
            },
        };

        Ok(Self {
            inode,
            offset: 0,
            flags,
        })
    }

    /// Read the file
    pub fn read(&mut self, count: usize) -> Result<Vec<u8>, FSError> {
        if !self.flags.contains(OpenFlags::RDONLY) && !self.flags.contains(OpenFlags::RDWR) {
            return Err(FSError::BadRights);
        }
        let result = self.inode.write().read(self.offset, count);

        if let Ok(result) = &result {
            self.offset += result.len();
        }

        result
    }

    /// Move the cursor at `bytes` in the file
    pub fn seek(&mut self, bytes: usize) {
        self.offset = bytes;
    }

    /// Write to the file
    pub fn write(&mut self, data: &[u8]) -> Result<usize, FSError> {
        if !self.flags.contains(OpenFlags::WRONLY) && !self.flags.contains(OpenFlags::RDONLY) {
            return Err(FSError::BadRights);
        }
        let offset = if self.flags.contains(OpenFlags::APPEND) {
            self.inode.read().metadata()?.size
        } else {
            self.offset
        };

        let result = self.inode.write().write(offset, data);

        if let Ok(result) = &result {
            self.offset += result;
        }

        result
    }

    /// Get the metadata of the file (size, modified hour, ...)
    pub fn metadata(&self) -> Result<Metadata, FSError> {
        self.inode.read().metadata()
    }

    /// Map this file in memory
    pub fn mmap(self, offset: usize, area: &mut MemoryArea, mem: &mut ProcessMemory) -> Result<(), FSError> {
        match self.metadata()?.type_ {
            FileType::File => {
                area.handler = Arc::new(FileHandler {
                    file: self,
                    mem_start: area.start,
                    file_start: offset as u64,
                    file_end: offset as u64 + area.size() as u64,
                });

                area.map(mem)?;
                Ok(())
            },
            FileType::CharDevice => self.inode.read().mmap(offset, area, mem),
            _ => Err(FSError::NotSupported),
        }
    }
}

impl fmt::Debug for FileHandle {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "File")
    }
}

/// A file ID
///
/// This ID is used by user programs to open, read, write and close files
pub type Fd = usize;
