/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! A fake initramfs used to read volatile files
//! It uses the `include_bytes` macro to read files during the compilation
use alloc::{
    collections::BTreeMap,
    sync::Arc,
    prelude::v1::Vec,
};
use spin::{RwLock, Once};
use core::{cmp, sync::atomic::{Ordering, AtomicUsize}};

use super::{
    error::FSError,
    vfs::{Inode, Filesystem, Metadata, FileType},
};

/// The next inode ID
pub static NEXT_INODE_NUM: AtomicUsize = AtomicUsize::new(0);

#[derive(Debug)]
/// Filesystem to handle volatile files
pub struct InitFS {
    root_inode: Arc<RwLock<dyn Inode>>,
}

#[derive(Debug)]
/// The inode of the InitFS
pub struct InitFSInode {
    data: Vec<u8>,
    metadata: Metadata,
    children: BTreeMap<&'static str, Arc<RwLock<InitFSInode>>>,
    parent: Once<Arc<RwLock<InitFSInode>>>,
    this: Once<Arc<RwLock<InitFSInode>>>,
}

impl Default for InitFSInode {
    fn default() -> Self {
        Self {
            data: vec![],
            metadata: Metadata {
                size: 0,
                type_: FileType::File,
                mode: 0o755,
                inode: NEXT_INODE_NUM.fetch_add(1, Ordering::Relaxed),
            },
            children: BTreeMap::new(),
            parent: Once::new(),
            this: Once::new(),
        }
    }
}

impl InitFS {
    /// Create a new InitFS filesystem
    pub fn new() -> Self {
        let root_inode = Arc::new(RwLock::new(InitFSInode {
            data: vec![],
            metadata: Metadata {
                size: 0,
                mode: 0o755,
                type_: FileType::Dir,
                inode: NEXT_INODE_NUM.fetch_add(1, Ordering::Relaxed), // Use the inode 0 for the root inode
            },
            children: BTreeMap::new(),
            parent: Once::new(),
            this: Once::new(),
        }));

        root_inode.read().parent.call_once(|| {
            root_inode.clone()
        });

        root_inode.read().this.call_once(|| {
            root_inode.clone()
        });

        print!("Retrieving InitFS files, it can take some time... ");
        Self::create_files(root_inode.clone()).expect("Error during creating InitFS!");
        println!("[ OK ]");

        println!("=== BEGIN InitFS ===\nROOT");
        for child in root_inode.read().children.iter() {
            println!("|--> {}", child.0);
            for child in child.1.read().children.iter() {
                println!("    |--> {}", child.0);
                for child in child.1.read().children.iter() {
                    println!("        |--> {}", child.0);
                }
            }
        }
        println!("=== END InitFS ===");

        Self { root_inode }
    }

    fn create_files(root: Arc<RwLock<InitFSInode>>) -> Result<(), FSError> {
        // For now the InitFS looks like this:
        // ROOT
        // |-> README.md
        // |-> bin
        //      |-> osh
        //      |-> init
        // |-> lib64
        //      |-> ld-linux-x86_64.so.2
        // |-> lib
        //      |-> libc.so.6
        //      |-> libdl.so.2
        //      |-> libgcc_s.so.1
        //      |-> libpthread.so.0

        let mut root = root.write();

        // "/bin"
        let dir = root.create("bin", FileType::Dir, 0o755)?;
        let mut dir = dir.write();

        // "/bin/pci-driver"
        let file = dir.create("pci-driver", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/bin/pci-driver") as &[u8])?;

        // "/bin/osh"
        let file = dir.create("osh", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/bin/osh") as &[u8])?;

        // "/bin/init"
        let file = dir.create("init", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/bin/init") as &[u8])?;

        // "/bin/test-mem"
        let file = dir.create("test-mem", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/bin/test-mem") as &[u8])?;

        // "/bin/lspci"
        let file = dir.create("lspci", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/bin/lspci") as &[u8])?;

        // "/bin/mknod"
        let file = dir.create("mknod", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/bin/mknod") as &[u8])?;

        // "/lib64"
        let dir = root.create("lib64", FileType::Dir, 0o755)?;
        let mut dir = dir.write();

        // "/lib64/ld-linux-x86-64.so.2"
        let file = dir.create("ld-linux-x86-64.so.2", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/lib/ld-linux-x86-64.so.2") as &[u8])?;

        // "/lib"
        let dir = root.create("lib", FileType::Dir, 0o755)?;
        let mut dir = dir.write();

        // "/lib/libc.so.6"
        let file = dir.create("libc.so.6", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/lib/libc.so.6") as &[u8])?;

        // "/lib/libdl.so.2"
        let file = dir.create("libdl.so.2", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/lib/libdl.so.2") as &[u8])?;

        // "/lib/libgcc_s.so.1"
        let file = dir.create("libgcc_s.so.1", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/lib/libgcc_s.so.1") as &[u8])?;

        // "/lib/libpthread.so.0"
        let file = dir.create("libpthread.so.0", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/lib/libpthread.so.0") as &[u8])?;

        // "/lib/libpci.so.3"
        let file = dir.create("libpci.so.3", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/lib/libpci.so.3") as &[u8])?;

        // "/lib/libkmod.so.2"
        let file = dir.create("libkmod.so.2", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/lib/libkmod.so.2") as &[u8])?;

        // "/lib/libz.so.1"
        let file = dir.create("libz.so.1", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/lib/libz.so.1") as &[u8])?;

        // "/lib/libresolv.so.2"
        let file = dir.create("libresolv.so.2", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/lib/libresolv.so.2") as &[u8])?;

        // "/lib/libudev.so.1"
        let file = dir.create("libudev.so.1", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/lib/libudev.so.1") as &[u8])?;

        // "/lib/liblzma.so.5"
        let file = dir.create("liblzma.so.5", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/lib/liblzma.so.5") as &[u8])?;

        // "/lib/libcrypto.so.1.1"
        let file = dir.create("libcrypto.so.1.1", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/lib/libcrypto.so.1.1") as &[u8])?;

        // "/lib/libselinux.so.1"
        let file = dir.create("libselinux.so.1", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/lib/libselinux.so.1") as &[u8])?;

        // "/lib/libpcre2-8.so.0"
        let file = dir.create("libpcre2-8.so.0", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../root/lib/libpcre2-8.so.0") as &[u8])?;

        // "/README.md"
        let file = root.create("README.md", FileType::File, 0o755)?;
        file.write().write(0, include_bytes!("../../../README.md") as &[u8])?;

        // "/tmp"
        root.create("tmp", FileType::Dir, 0o755)?;

        Ok(())
    }
}

impl Filesystem for InitFS {
    fn name(&self) -> &'static str {
        "InitFS"
    }

    fn root_inode(&self) -> Result<Arc<RwLock<dyn Inode>>, FSError> {
        Ok(self.root_inode.clone())
    }
}

impl Inode for InitFSInode {
    fn read(&mut self, offset: usize, count: usize) -> Result<Vec<u8>, FSError> {
        // Check that the current inode is not a directory
        if self.metadata()?.type_ == FileType::Dir {
            return Err(FSError::IsDir);
        }

        let size = self.metadata()?.size;
        let start = cmp::min(size, offset);
        let end = cmp::min(size, offset + count);

        Ok(self.data[start..end].to_vec())
    }

    fn write(&mut self, offset: usize, data: &[u8]) -> Result<usize, FSError> {
        // Check that the current inode is not a directory
        if self.metadata()?.type_ == FileType::Dir {
            return Err(FSError::IsDir);
        }

        if offset + data.len() > self.data.len() {
            self.data.resize(offset + data.len(), 0);
        }
        self.data[offset..].copy_from_slice(data);
        self.metadata.size = data.len();

        Ok(data.len())
    }

    fn metadata(&self) -> Result<Metadata, FSError> {
        Ok(self.metadata)
    }

    fn set_metadata(&mut self, metadata: Metadata) -> Result<(), FSError> {
        self.metadata = metadata;
        Ok(())
    }

    fn create(&mut self, name: &'static str, type_: FileType, mode: u16) -> Result<Arc<RwLock<dyn Inode>>, FSError> {
        // Check that the current inode is a directory
        if self.metadata()?.type_ != FileType::Dir {
            return Err(FSError::NotDir);
        }

        // Check that the child does not already exist
        if self.children.contains_key(name) {
            return Err(FSError::Exists);
        }

        // Check that the name does not contains any slashes
        if name.contains("/") {
            return Err(FSError::BadName);
        }

        let inode = Arc::new(RwLock::new(InitFSInode {
            metadata: Metadata {
                size: 0,
                type_,
                mode,
                inode: NEXT_INODE_NUM.fetch_add(1, Ordering::Relaxed),
            },
            ..Default::default()
        }));

        inode.read().parent.call_once(|| {
            self.this.get().unwrap().clone()
        });
        inode.read().this.call_once(|| {
            inode.clone()
        });

        // Increment the number of children in the directory
        self.metadata.size += 1;

        self.children.insert(name, inode.clone());
        Ok(inode)
    }

    fn search(&self, name: &str) -> Result<Arc<RwLock<dyn Inode>>, FSError> {
        match name {
            "." => Ok(self.this.get().unwrap().clone()),
            ".." => Ok(self.parent.get().unwrap().clone()),
            _ => {
                self.children
                    .get(name)
                    .ok_or(FSError::NotFound)
                    .and_then(|t| Ok(t.clone() as Arc<RwLock<dyn Inode>>))
            },
        }
    }

    fn entries(&self) -> Result<Vec<&'static str>, FSError> {
        Ok(self.children.keys().map(|s| *s).collect::<Vec<&'static str>>())
    }
}
