/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Filesystem management
use lazy_static::lazy_static;
use alloc::sync::Arc;
use spin::RwLock;

pub mod error;
pub mod vfs;
pub mod file;
pub mod root;
pub mod initfs;
pub mod devfs;
pub mod socket;

use root::RootFS;
use initfs::InitFS;
use devfs::{
    tty::TTY,
    mem::MemInode,
    DevFS,
};
use vfs::FileType;

lazy_static! {
    /// The RootFS, an abstraction layer handling inodes of all filesystems
    pub static ref ROOT: Arc<RwLock<RootFS>> = {
        // Create the InitFS
        let initfs = InitFS::new();

        // Add the devices to a DevFS
        let devfs = DevFS::new();
        let mem = Arc::new(RwLock::new(MemInode {}));
        devfs.add("tty", TTY.clone()).expect("Cannot create /dev/tty");
        devfs.add("mem", mem).expect("Cannot create /dev/mem");

        // Create a RootFS and mount the InitFS at /
        let rootfs = RootFS::new(Arc::new(initfs));
        let root = rootfs.read().root_inode().unwrap();

        // Mount the DevFS at /dev
        let dev = root.write().create("dev", FileType::Dir, 0o755).expect("Cannot create /dev");
        dev.write().mount(Arc::new(devfs)).unwrap();

        rootfs
    };
}
