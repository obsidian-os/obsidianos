/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Defines the RootFS, an abstraction layer handling inodes of all filesystems
use alloc::{
    sync::Arc,
    prelude::v1::Vec,
    collections::BTreeMap,
};
use spin::{RwLock, Once};

use crate::tasking::memory::{MemoryArea, ProcessMemory};
use super::{
    error::FSError,
    vfs::{Filesystem, Inode, Metadata, FileType, InodeID},
};

#[derive(Debug)]
/// The RootFS, an abstraction layer handling inodes of all filesystems
pub struct RootFS {
    root_fs: Arc<dyn Filesystem>,
    mountpoints: BTreeMap<InodeID, Arc<dyn Filesystem>>,
    this: Once<Arc<RwLock<RootFS>>>,
}

#[derive(Debug)]
/// An inode in the RootFS filesystem
///
/// It **is not** an inode! This structure just handles methods like read, write, ... and pass
/// them to the inode of the corresponding filesystem.
pub struct RootInode {
    inner: Arc<RwLock<dyn Inode>>,
    rootfs: Arc<RwLock<RootFS>>,
}

impl RootFS {
    /// Create a new RootFS
    pub fn new(root_fs: Arc<dyn Filesystem>) -> Arc<RwLock<Self>> {
        let fs = Arc::new(RwLock::new(Self {
            root_fs,
            mountpoints: BTreeMap::new(),
            this: Once::new(),
        }));

        fs.write().this.call_once(|| fs.clone());
        fs
    }

    /// Open an inode from a path
    pub fn open(&self, path: &str) -> Result<Arc<RwLock<dyn Inode>>, FSError> {
        // Handle the `/` path
        if path != "/" {
            self.root_inode()?.read().open(path)
        } else {
            self.root_inode().and_then(|r| Ok(r as Arc<RwLock<dyn Inode>>))
        }
    }

    /// Get the root inode of the filesystem
    pub fn root_inode(&self) -> Result<Arc<RwLock<RootInode>>, FSError> {
        Ok(Arc::new(RwLock::new(RootInode {
            inner: self.root_fs.root_inode()?,
            rootfs: self.this.get().unwrap().clone(),
        })))
    }
}

impl RootInode {
    /// Create a new inode in the current filesystem
    pub fn create(&mut self, name: &'static str, type_: FileType, mode: u16) -> Result<Arc<RwLock<Self>>, FSError> {
        Ok(Arc::new(RwLock::new(RootInode {
            inner: self.inner.write().create(name, type_, mode)?,
            rootfs: self.rootfs.clone(),
        })))
    }

    /// If a filesystem is mounted here, returns its root inode,
    /// otherwise, returns the inode
    pub fn overlaid_inode(&self) -> Result<Option<Arc<RwLock<RootInode>>>, FSError> {
        let id = self.metadata()?.inode;
        match self.rootfs.read().mountpoints.get(&id) {
            None => {
                Ok(None)
            },
            Some(fs) => {
                Ok(Some(Arc::new(RwLock::new(Self {
                    inner: fs.root_inode()?,
                    rootfs: self.rootfs.clone(),
                }))))
            },
        }
    }

    /// Search a file from here
    pub fn search(&self, name: &str) -> Result<Arc<RwLock<Self>>, FSError> {
        let inode = match self.overlaid_inode()? {
            Some(inode) => {
                Arc::new(RwLock::new(Self {
                    inner: inode.read().inner.read().search(name)?,
                    rootfs: self.rootfs.clone(),
                }))
            },
            None => {
                Arc::new(RwLock::new(Self {
                    inner: self.inner.read().search(name)?,
                    rootfs: self.rootfs.clone(),
                }))
            },
        };

        match inode.clone().read().overlaid_inode()? {
            Some(inode) => Ok(inode),
            None => Ok(inode),
        }
    }

    /// Mount a filesystem here
    pub fn mount(&mut self, fs: Arc<dyn Filesystem>) -> Result<(), FSError> {
        let id = self.metadata()?.inode;
        let mut rootfs = self.rootfs.write();

        match rootfs.mountpoints.contains_key(&id) {
            true => Err(FSError::Exists),
            false => {
                rootfs.mountpoints.insert(id, fs);
                Ok(())
            },
        }
    }
}

impl Inode for RootInode {
    fn create(&mut self, name: &'static str, type_: FileType, mode: u16) -> Result<Arc<RwLock<dyn Inode>>, FSError> {
        Ok(self.create(name, type_, mode)? as Arc<RwLock<dyn Inode>>)
    }

    fn search(&self, name: &str) -> Result<Arc<RwLock<dyn Inode>>, FSError> {
        Ok(self.search(name)? as Arc<RwLock<dyn Inode>>)
    }

    fn read(&mut self, offset: usize, count: usize) -> Result<Vec<u8>, FSError> {
        self.inner.write().read(offset, count)
    }

    fn write(&mut self, offset: usize, data: &[u8]) -> Result<usize, FSError> {
        self.inner.write().write(offset, data)
    }

    fn sync(&mut self) -> Result<(), FSError> {
        self.inner.write().sync()
    }

    fn metadata(&self) -> Result<Metadata, FSError> {
        self.inner.read().metadata()
    }

    fn mmap(&self, offset: usize, area: &mut MemoryArea, mem: &mut ProcessMemory) -> Result<(), FSError> {
        self.inner.read().mmap(offset, area, mem)
    }

    fn entries(&self) -> Result<Vec<&'static str>, FSError> {
        self.inner.read().entries()
    }
}
