/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Virtual File System structures
use alloc::prelude::v1::Vec;
use alloc::sync::Arc;
use spin::RwLock;
use core::fmt::Debug;

use super::{
    error::FSError,
    file::Perms,
};
use crate::{
    tasking::memory::{MemoryArea, ProcessMemory},
    syscall::fs::StatInfo,
};

/// Structure representing a filesystem
/// For each filesystem, we just need to get the root inode
pub trait Filesystem: Send + Sync + Debug {
    /// Get the name of the filesystem
    fn name(&self) -> &'static str;

    /// Get the root inode of the file system
    fn root_inode(&self) -> Result<Arc<RwLock<dyn Inode>>, FSError>;
}

/// Structure representing an inode
/// An inode is the most simple structure in a filesystem, it is used to handle operations like
/// read, write, ...
pub trait Inode: Send + Sync + Debug {
    /// Create a new inode in the current inode
    ///
    /// The current inode must be a directory
    fn create(&mut self, _name: &'static str, _type_: FileType, _mode: u16) -> Result<Arc<RwLock<dyn Inode>>, FSError> {
        Err(FSError::NotSupported)
    }

    /// Read the data of the inode
    fn read(&mut self, offset: usize, count: usize) -> Result<Vec<u8>, FSError>;

    /// Write to the data of the inode
    fn write(&mut self, offset: usize, data: &[u8]) -> Result<usize, FSError>;

    /// Get the metadatas of the inode (size, inode number, ...)
    fn metadata(&self) -> Result<Metadata, FSError> {
        Err(FSError::NotSupported)
    }

    /// Set the metadatas of the inode (size, inode number, ...)
    fn set_metadata(&mut self, _metadata: Metadata) -> Result<(), FSError> {
        Err(FSError::NotSupported)
    }

    /// Search a file in the current inode
    ///
    /// The current inode must be a directory
    fn search(&self, _name: &str) -> Result<Arc<RwLock<dyn Inode>>, FSError> {
        Err(FSError::NotSupported)
    }

    /// Get a file from a path from the current inode
    fn open(&self, path: &str) -> Result<Arc<RwLock<dyn Inode>>, FSError> {
        let mut path = path.split("/");
        path.next();

        let mut inode = self.search(".")?;

        for name in path {
            // Check that the current inode is a directory
            if inode.read().metadata()?.type_ != FileType::Dir {
                return Err(FSError::NotDir);
            }

            let result = inode.write().search(name)?;
            inode = result;
        }

        Ok(inode)
    }

    /// Create a symbolic link to another inode
    ///
    /// Not handled in any filesystem for now
    fn link(&mut self) -> Result<(), FSError> {
        Err(FSError::NotSupported)
    }

    /// Remove a symbolic link
    ///
    /// Not handled in any filesystem for now
    fn unlink(&mut self) -> Result<(), FSError> {
        Err(FSError::NotSupported)
    }

    /// Write the changes to the disk
    ///
    /// As there is not any drivers, this function is not used for now
    fn sync(&mut self) -> Result<(), FSError> {
        Err(FSError::NotSupported)
    }

    /// Create a directory in the current inode
    ///
    /// The current inode must be a directory
    fn mkdir(&mut self) -> Result<(), FSError> {
        Err(FSError::NotSupported)
    }

    /// Remove a directory in the current inode
    ///
    /// The current inode must be a directory
    fn rmdir(&mut self) -> Result<(), FSError> {
        Err(FSError::NotSupported)
    }

    /// Create a block or character file
    ///
    /// The current inode must be a directory
    fn mknod(&mut self) -> Result<(), FSError> {
        Err(FSError::NotSupported)
    }

    /// Rename a file or a directory
    fn rename(&mut self) -> Result<(), FSError> {
        Err(FSError::NotSupported)
    }

    /// Map files or devices into memory
    fn mmap(&self, _offset: usize, _area: &mut MemoryArea, _mem: &mut ProcessMemory) -> Result<(), FSError> {
        Err(FSError::NotSupported)
    }

    /// Get the entries of the current inode
    fn entries(&self) -> Result<Vec<&'static str>, FSError> {
        Err(FSError::NotSupported)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
/// Metadatas of an inode
pub struct Metadata {
    /// Size of the inode
    pub size: usize,

    /// Rights of the inode (UNIX like)
    pub mode: u16,

    /// Type of the inode
    pub type_: FileType,

    /// Inode number
    pub inode: InodeID,

    // TODO: Dev, ...
}

#[repr(u16)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
/// Type of an inode
pub enum FileType {
    /// The inode is a regular file
    File,

    /// The inode is a directory
    Dir,

    /// The inode is a symbolic link
    SymLink,

    /// The inode is a character device
    CharDevice,

    /// The inode is a block device
    BlockDevice,

    /// The inode is a socket
    Socket,

    /// The inode is a pipe
    NamedPipe,
}

impl From<Perms> for FileType {
    fn from(perms: Perms) -> Self {
        if perms.contains(Perms::FILE) { FileType::File }
        else if perms.contains(Perms::DIR) { FileType::Dir }
        else if perms.contains(Perms::LINK) { FileType::SymLink }
        else if perms.contains(Perms::CHAR) { FileType::CharDevice }
        else if perms.contains(Perms::BLOCK) { FileType::BlockDevice }
        else if perms.contains(Perms::SOCKET) { FileType::Socket }
        else if perms.contains(Perms::FIFO) { FileType::NamedPipe }
        else { FileType::File }
    }
}

impl From<Metadata> for StatInfo {
    fn from(meta: Metadata) -> StatInfo {
        let type_ = Perms::from(meta.type_);
        let mode = (type_ | Perms::from_bits_truncate(meta.mode as u32)).bits();

        StatInfo {
            st_dev: 0,
            st_ino: meta.inode as u64,
            st_mode: mode,
            st_size: meta.size as i64,
            .. Default::default()
        }
    }
}

/// ID of an inode
pub type InodeID = usize;
