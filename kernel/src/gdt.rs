/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Global descriptor table segments management and initialization
use core::mem;
use x86::{
    current::task::TaskStateSegment,
    dtables::{lgdt, DescriptorTablePointer},
    segmentation::*,
    task::load_tr,
    Ring::Ring0,
};

/// The index of the Double Fault entry in the Interrupt Stack Table (IST)
pub const DOUBLE_FAULT_IST_INDEX: u16 = 0;

/// The index of the NULL segment in the GDT
pub const GDT_NULL: usize = 0;

/// The index of the KERNEL_CODE segment in the GDT
pub const GDT_KERNEL_CODE: usize = 1;

/// The index of the KERNEL_DATA segment in the GDT
pub const GDT_KERNEL_DATA: usize = 2;

/// The index of the USER_CODE segment in the GDT
pub const GDT_USER_CODE: usize = 3;

/// The index of the USER_DATA segment in the GDT
pub const GDT_USER_DATA: usize = 4;

/// The index of the TSS segment in the GDT
pub const GDT_TSS: usize = 5;

/// The index of the high part of the TSS segment in the GDT
pub const GDT_TSS_HIGH: usize = 6;

/// The flag which indicates that a segment is present
const GDT_A_PRESENT: u8 = 1 << 7;

/// The flag which indicates that a segment used the [Ring 0](https://en.wikipedia.org/wiki/Protection_ring)
const GDT_A_RING_0: u8 = 0 << 5;

/// The flag which indicates that a segment used the [Ring 3](https://en.wikipedia.org/wiki/Protection_ring)
const GDT_A_RING_3: u8 = 3 << 5;

/// The flag which indicates that a segment is a system segment
const GDT_A_SYSTEM: u8 = 1 << 4;

/// The flag which indicates that a segment is an executable segment
const GDT_A_EXECUTABLE: u8 = 1 << 3;

/// The flag which indicates that a segment is a privileged segment
const GDT_A_PRIVILEGE: u8 = 1 << 1;

/// The flag which indicates that a segment is a TSS segment
const GDT_A_TSS_AVAIL: u8 = 0x9;

/// The flag which indicates that a segment is used in long mode
const GDT_F_LONG_MODE: u8 = 1 << 5;

#[derive(Copy, Clone, Debug)]
#[repr(packed)]
/// A struct which [represents an entry](https://wiki.osdev.org/images/f/f3/GDT_Entry.png) in the [GDT](https://en.wikipedia.org/wiki/Global_Descriptor_Table)
pub struct GdtEntry {
    /// The low part of the entry's limit
    pub limitl: u16,

    /// The low part of the entry's offset
    pub offsetl: u16,

    /// The middle part of the entry's offset
    pub offsetm: u8,

    /// The flags which indicates the allowed accesses of a segment
    pub access: u8,

    /// The flags of the entry
    pub flags_limith: u8,

    /// The high part of the entry's offset
    pub offseth: u8,
}

impl GdtEntry {
    /// Create a new GdtEntry
    pub const fn new(access: u8, flags: u8) -> Self {
        GdtEntry {
            limitl: 0,
            offsetl: 0,
            offsetm: 0,
            access: access,
            flags_limith: flags & 0xF0 | ((0 >> 16) as u8) & 0x0F,
            offseth: 0,
        }
    }

    /// Set the offset of the entry
    pub fn set_offset(&mut self, offset: u32) {
        self.offsetl = offset as u16;
        self.offsetm = (offset >> 16) as u8;
        self.offseth = (offset >> 24) as u8;
    }

    /// Set the limit of the entry
    pub fn set_limit(&mut self, limit: u32) {
        self.limitl = limit as u16;
        self.flags_limith = self.flags_limith & 0xF0 | ((limit >> 16) as u8) & 0x0F;
    }
}

/// The pointer to the global GDT
pub static mut GDTR: DescriptorTablePointer<Descriptor> = DescriptorTablePointer {
    limit: 0,
    base: 0 as *const Descriptor,
};

/// The global GDT used by the kernel
pub static mut GDT: [GdtEntry; 7] = [
    // Null segment
    GdtEntry::new(0, 0),
    // Kernel code segment
    GdtEntry::new(
        GDT_A_PRESENT | GDT_A_RING_0 | GDT_A_SYSTEM | GDT_A_EXECUTABLE | GDT_A_PRIVILEGE,
        GDT_F_LONG_MODE,
    ),
    // Kernel data segment
    GdtEntry::new(
        GDT_A_PRESENT | GDT_A_RING_0 | GDT_A_SYSTEM | GDT_A_PRIVILEGE,
        GDT_F_LONG_MODE,
    ),
    // User code segment
    GdtEntry::new(
        GDT_A_PRESENT | GDT_A_RING_3 | GDT_A_SYSTEM | GDT_A_EXECUTABLE | GDT_A_PRIVILEGE,
        GDT_F_LONG_MODE,
    ),
    // User data segment
    GdtEntry::new(
        GDT_A_PRESENT | GDT_A_RING_3 | GDT_A_SYSTEM | GDT_A_PRIVILEGE,
        GDT_F_LONG_MODE,
    ),
    // TSS segment
    GdtEntry::new(GDT_A_PRESENT | GDT_A_RING_3 | GDT_A_TSS_AVAIL, 0),
    // TSS must be 16 bytes long, twice the normal size
    GdtEntry::new(0, 0),
];

/// The [task state segment (TSS)](https://en.wikipedia.org/wiki/Task_state_segment) which handles
/// some informations about the current process
pub static mut TSS: TaskStateSegment = TaskStateSegment {
    reserved: 0,
    rsp: [0; 3],
    reserved2: 0,
    ist: [0; 7],
    reserved3: 0,
    reserved4: 0,
    iomap_base: 0xFFFF,
};

/// Initialize GDT
pub unsafe fn init() {
    // Set the GDT limit and base
    GDTR.limit = (GDT.len() * mem::size_of::<GdtEntry>() - 1) as u16;
    GDTR.base = GDT.as_ptr() as *const Descriptor;

    // Set the TSS entry
    GDT[GDT_TSS].set_offset(&TSS as *const _ as u32);
    GDT[GDT_TSS].set_limit(mem::size_of::<TaskStateSegment>() as u32);

    // Set the stack pointer when coming back from userspace
    set_stack(x86::bits64::registers::rsp());

    // Load the GDT
    lgdt(&GDTR);

    // Load the segment descriptors
    load_cs(SegmentSelector::new(GDT_KERNEL_CODE as u16, Ring0));
    load_ds(SegmentSelector::new(GDT_KERNEL_DATA as u16, Ring0));
    load_es(SegmentSelector::new(GDT_KERNEL_DATA as u16, Ring0));
    load_fs(SegmentSelector::new(GDT_KERNEL_DATA as u16, Ring0));
    load_gs(SegmentSelector::new(GDT_KERNEL_DATA as u16, Ring0));
    load_ss(SegmentSelector::new(GDT_KERNEL_DATA as u16, Ring0));

    // Load the task register
    load_tr(SegmentSelector::new(GDT_TSS as u16, Ring0));
}

/// Set the kernel's stack pointer in the TSS
pub unsafe fn set_stack(addr: u64) { TSS.rsp[0] = addr; }
