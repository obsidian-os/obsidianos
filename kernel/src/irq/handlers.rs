/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Functions which are called when an interrupt or an exception happens
use x86_64::structures::idt::{InterruptStackFrame, PageFaultErrorCode};
use lazy_static::lazy_static;
use spin::Mutex;
use pc_keyboard::{layouts, DecodedKey, HandleControl, Keyboard, ScancodeSet1};

use super::id::IrqId;
use super::idt::PICS;
use crate::{
    memory::{KERNEL_START_VIRT, KERNEL_SIZE},
    tasking::{
        scheduler::{PROCESSES, SCHEDULER, switch},
        signal::{
            info::*,
            Signal,
            send_signal,
        },
        memory::cow::{COW_BIT, Cow},
    },
    fs::devfs::tty::TTY,
    fault_code, fault_code_non_return, fault_without_code, fault_non_return, fault,
};

/// Loop infinitely
pub fn stop() -> ! {
    loop {
        x86_64::instructions::hlt();
    }
}

/// A helper which signals that the current interrupt ended
pub fn end(id: IrqId) {
    unsafe {
        PICS.lock().notify_end_of_interrupt(id as u8);
    }
}

// --- Interrupts ---

extern "x86-interrupt" {
    /// The timer interrupt handler
    ///
    /// This function does several things:
    /// - Save the current context (registers)
    /// - Choose the next thread (with the scheduler)
    /// - Load the new context
    pub fn timer_interrupt_handler(_: &mut InterruptStackFrame);
}

lazy_static! {
    static ref KEYBOARD: Mutex<Keyboard<layouts::Us104Key, ScancodeSet1>> =
        Mutex::new(Keyboard::new(layouts::Us104Key, ScancodeSet1,
            HandleControl::Ignore)
        );
}

/// The keyboard interrupt handler
pub extern "x86-interrupt" fn keyboard_interrupt_handler(_stack_frame: &mut InterruptStackFrame) {
    use x86_64::instructions::port::Port;

    let mut keyboard = KEYBOARD.lock();
    let code: u8 = unsafe { Port::new(0x60).read() };

    if let Ok(Some(key_event)) = keyboard.add_byte(code) {
        if let Some(key) = keyboard.process_keyevent(key_event) {
            match key {
                DecodedKey::Unicode(character) => {
                    TTY.write().add(character as u8);
                },
                DecodedKey::RawKey(key) => {
                    TTY.write().add(key as u8);
                },
            }
        }
    }

    end(IrqId::KEYBOARD);
}

/// The PCI slave interrupt handler
pub extern "x86-interrupt" fn cascade_interrupt_handler(_stack_frame: &mut InterruptStackFrame) {
    end(IrqId::CASCADE);
}

/// The COM1 interrupt handler
pub extern "x86-interrupt" fn com1_interrupt_handler(_stack_frame: &mut InterruptStackFrame) {
    end(IrqId::COM1);
}

/// The COM2 interrupt handler
pub extern "x86-interrupt" fn com2_interrupt_handler(_stack_frame: &mut InterruptStackFrame) {
    end(IrqId::COM2);
}

/// The LPT1 interrupt handler
pub extern "x86-interrupt" fn lpt1_interrupt_handler(_stack_frame: &mut InterruptStackFrame) {
    end(IrqId::LPT1);
}

/// The floppy disk interrupt handler
pub extern "x86-interrupt" fn floppy_interrupt_handler(_stack_frame: &mut InterruptStackFrame) {
    end(IrqId::FLOPPY);
}

/// The LPT2 interrupt handler
pub extern "x86-interrupt" fn lpt2_interrupt_handler(_stack_frame: &mut InterruptStackFrame) {
    end(IrqId::LPT2);
}

/// The RTC interrupt handler
pub extern "x86-interrupt" fn rtc_interrupt_handler(_stack_frame: &mut InterruptStackFrame) {
    end(IrqId::RTC);
}

/// The PCI1 interrupt handler
pub extern "x86-interrupt" fn pci1_interrupt_handler(_stack_frame: &mut InterruptStackFrame) {
    end(IrqId::PCI1);
}

/// The PCI2 interrupt handler
pub extern "x86-interrupt" fn pci2_interrupt_handler(_stack_frame: &mut InterruptStackFrame) {
    end(IrqId::PCI2);
}

/// The PCI3 interrupt handler
pub extern "x86-interrupt" fn pci3_interrupt_handler(_stack_frame: &mut InterruptStackFrame) {
    end(IrqId::PCI3);
}

/// The mouse interrupt handler
pub extern "x86-interrupt" fn mouse_interrupt_handler(_stack_frame: &mut InterruptStackFrame) {
    end(IrqId::MOUSE);
}

/// The FPU interrupt handler
pub extern "x86-interrupt" fn fpu_interrupt_handler(_stack_frame: &mut InterruptStackFrame) {
    end(IrqId::FPU);
}

/// The syscall interrupt handler
pub extern "x86-interrupt" fn syscall_interrupt_handler(_stack_frame: &mut InterruptStackFrame) {
    end(IrqId::SYSCALL);
}

// --- Exceptions ---

fault_without_code!(divide, stack_frame, SIGFPE, {
        println!(
            "KERNEL PANIC @ Fault \"divide error\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_without_code!(debug, stack_frame, SIGTRAP, {
        println!(
            "KERNEL PANIC @ Fault \"debug\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_without_code!(non_maskable, stack_frame, SIGBUS, {
        println!(
            "KERNEL PANIC @ Fault \"non maskable interrupt\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_without_code!(breakpoint, stack_frame, SIGTRAP, {
        println!(
            "KERNEL PANIC @ Fault \"breakpoint\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_without_code!(overflow, stack_frame, SIGFPE, {
        println!(
            "KERNEL PANIC @ Fault \"overflow\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_without_code!(bound_range, stack_frame, SIGSEGV, {
        println!(
            "KERNEL PANIC @ Fault \"bound range exceeded\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_without_code!(invalid_opcode, stack_frame, SIGILL, {
        println!(
            "KERNEL PANIC @ Fault \"invalid opcode\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_without_code!(device, stack_frame, SIGILL, {
        println!(
            "KERNEL PANIC @ Fault \"device not present\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_code_non_return!(double_fault, stack_frame, SIGSEGV, {
        println!(
            "KERNEL PANIC @ Fault \"double fault\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_code!(tss, stack_frame, SIGSEGV, {
        println!(
            "KERNEL PANIC @ Fault \"invalid tss\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_code!(segment_not_present, stack_frame, SIGSEGV, {
        println!(
            "KERNEL PANIC @ Fault \"segment not present\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_code!(stack_segment, stack_frame, SIGSEGV, {
        println!(
            "KERNEL PANIC @ Fault \"stack segment\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_code!(protection, stack_frame, SIGSEGV, {
        println!(
            "KERNEL PANIC @ Fault \"general protection\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_without_code!(fpu, stack_frame, SIGFPE, {
        println!(
            "KERNEL PANIC @ Fault \"x87 floating point\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_code!(alignment, stack_frame, SIGBUS, {
        println!(
            "KERNEL PANIC @ Fault \"alignment check\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_non_return!(machine, stack_frame, SIGBUS, {
        println!(
            "KERNEL PANIC @ Fault \"alignment check\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_without_code!(simd, stack_frame, SIGFPE, {
        println!(
            "KERNEL PANIC @ Fault \"simd floating point\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_without_code!(virtualization, stack_frame, SIGBUS, {
        println!(
            "KERNEL PANIC @ Fault \"virtualization\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

fault_code!(security, stack_frame, SIGBUS, {
        println!(
            "KERNEL PANIC @ Fault \"security\" at {:#x}",
            stack_frame.instruction_pointer.as_u64()
        );
});

/// A page fault handler calling the right handler for the requested area, sending the SIGSEGV
/// signal or trigger the kernel panic
///
/// Handler(s):
/// * Copy-on-write (also called COW)
/// * Zero (zeroing the area)
/// * File (writing the content of a file into the area)
pub extern "x86-interrupt" fn page_fault_handler(
    stack_frame: &mut InterruptStackFrame,
    error_code: PageFaultErrorCode,
) {
    use x86_64::registers::control::Cr2;

    let mut handled = false;
    let addr = Cr2::read();

    if let Some(current_thread) = SCHEDULER.get().current() {
        // Use the kernel's page table
        crate::memory::switch_to_kernel_page_table();

        let addr = addr.as_u64();

        let mut processes = PROCESSES.write();
        let current_process = processes
            .get_mut(&current_thread.process)
            .expect("Could not find the current process");

        if let Some(index) = current_process
            .memory
            .get_area(addr, addr)
        {
            let area = current_process
                .memory
                .areas[index]
                .clone();
            let entry = current_process.memory.table.get_entry(addr).unwrap();

            if entry.flags().contains(COW_BIT)
                && error_code.contains(PageFaultErrorCode::CAUSED_BY_WRITE) {
                Cow::handle_page_fault(addr, entry, current_process);
                handled = true;
            }

            if !handled {
                handled = area.handler.handle_page_fault(addr, &current_process.memory.table);
            }

            if handled {
                current_process
                    .memory
                    .switch_page_table();
                return;
            }
        }
    }

    if !handled {
        if stack_frame.instruction_pointer.as_u64() >= KERNEL_START_VIRT
            && stack_frame.instruction_pointer.as_u64() <= KERNEL_START_VIRT + KERNEL_SIZE {
            // The page fault is generated by the kernel
            //
            // KERNEL PANIC!
            println!(
                "KERNEL PANIC @ Fault at {:#x} | {:?} | -> {:#x}",
                stack_frame.instruction_pointer.as_u64(),
                error_code,
                addr.as_u64()
            );
            stop();
        } else {
            // The page fault is generated by a userspace program, send the SIGSEGV signal
            send_signal(
                Signal::SIGSEGV,
                SigInfo {
                    si_signo: Signal::SIGSEGV as usize,
                    si_errno: 0, // FIXME ???
                    si_code: SEGV_MAPERR,
                    si_addr: stack_frame.instruction_pointer.as_u64() as usize,
                }
            ).expect("Signal sending failed");

            switch();
        }
    }
}
