/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! The interrupt's numbers

/// Interrupt positions in the Interrupt Descriptor Table (IDT)
/// See [OSDEV](https://wiki.osdev.org/IVT)
#[derive(Debug)]
pub enum IrqId {
    // --- Master ---
    /// Timer interruption (the PIT) used to schedule processes
    TIMER = 32,

    /// Keyboard interruption
    KEYBOARD = 33,

    /// Slave interruption
    CASCADE = 34,

    /// COM2 interruption
    COM2 = 35,

    /// COM1 interruption
    COM1 = 36,

    /// LPT2 interruption
    LPT2 = 37,

    /// Floppy disk interruption
    FLOPPY = 38,

    /// LPT1 interruption
    LPT1 = 39,

    // --- Slave ---
    /// Real Time Clock (RTC) interruption
    RTC = 40,

    /// PCI1 interruption
    PCI1 = 41,

    /// PCI2 interruption
    PCI2 = 42,

    /// PCI3 interruption
    PCI3 = 43,

    /// Mouse interruption
    MOUSE = 44,

    /// FPU interruption
    FPU = 45,

    /// Syscall interruption used in userspace processes to call some kernel's functions
    SYSCALL = 0x80,
}
