/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! The Interrupt Descriptor Table (IDT) which describes which handler must be called when an
//! interrupt happens (see [OSDEV](https://wiki.osdev.org/IDT))
use core::mem;
use lazy_static::lazy_static;
use pic8259_simple::ChainedPics;
use spin::Mutex;
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame};
use x86_64::PrivilegeLevel::Ring3;

use super::handlers::*;
use super::id::IrqId;

/// The position of the PCI master in the IVT
pub const PIC_1_OFFSET: u8 = 32;

/// The position of the PCI slave in the IVT
pub const PIC_2_OFFSET: u8 = PIC_1_OFFSET + 8;

/// The PICs master and slave
pub static PICS: spin::Mutex<ChainedPics> =
    Mutex::new(unsafe { ChainedPics::new(PIC_1_OFFSET, PIC_2_OFFSET) });

lazy_static! {
    static ref IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();

        // Exceptions
        idt.divide_error.set_handler_fn(divide);
        idt.debug.set_handler_fn(debug);
        idt.non_maskable_interrupt.set_handler_fn(non_maskable);
        idt.breakpoint.set_handler_fn(breakpoint);
        idt.overflow.set_handler_fn(overflow);
        idt.bound_range_exceeded.set_handler_fn(bound_range);
        idt.invalid_opcode.set_handler_fn(invalid_opcode);
        idt.device_not_available.set_handler_fn(device);
        unsafe {
            idt.double_fault.set_handler_fn(double_fault)
                .set_stack_index(crate::gdt::DOUBLE_FAULT_IST_INDEX);
        }
        idt.invalid_tss.set_handler_fn(tss);
        idt.segment_not_present.set_handler_fn(segment_not_present);
        idt.stack_segment_fault.set_handler_fn(stack_segment);
        idt.general_protection_fault.set_handler_fn(protection);
        idt.page_fault.set_handler_fn(page_fault_handler);
        idt.x87_floating_point.set_handler_fn(fpu);
        idt.alignment_check.set_handler_fn(alignment);
        idt.machine_check.set_handler_fn(machine);
        idt.simd_floating_point.set_handler_fn(simd);
        idt.virtualization.set_handler_fn(virtualization);
        idt.security_exception.set_handler_fn(security);

        // Interrupts
        idt[IrqId::TIMER as usize].set_handler_fn(
            unsafe { mem::transmute::<
                unsafe extern "x86-interrupt" fn(_: &mut InterruptStackFrame),
                extern "x86-interrupt" fn(_: &mut InterruptStackFrame)
            >(timer_interrupt_handler) });
        idt[IrqId::KEYBOARD as usize].set_handler_fn(keyboard_interrupt_handler);
        idt[IrqId::CASCADE as usize].set_handler_fn(cascade_interrupt_handler);
        idt[IrqId::COM1 as usize].set_handler_fn(com1_interrupt_handler);
        idt[IrqId::COM2 as usize].set_handler_fn(com2_interrupt_handler);
        idt[IrqId::LPT2 as usize].set_handler_fn(lpt2_interrupt_handler);
        idt[IrqId::FLOPPY as usize].set_handler_fn(floppy_interrupt_handler);
        idt[IrqId::LPT1 as usize].set_handler_fn(lpt1_interrupt_handler);
        idt[IrqId::RTC as usize].set_handler_fn(rtc_interrupt_handler);
        idt[IrqId::PCI1 as usize].set_handler_fn(pci1_interrupt_handler);
        idt[IrqId::PCI2 as usize].set_handler_fn(pci2_interrupt_handler);
        idt[IrqId::PCI3 as usize].set_handler_fn(pci3_interrupt_handler);
        idt[IrqId::MOUSE as usize].set_handler_fn(mouse_interrupt_handler);
        idt[IrqId::FPU as usize].set_handler_fn(fpu_interrupt_handler);
        idt[IrqId::SYSCALL as usize].set_handler_fn(syscall_interrupt_handler)
            .set_privilege_level(Ring3);

        idt
    };
}

/// Init the IDT by using the `lidt` instruction
pub fn init() { IDT.load(); }
