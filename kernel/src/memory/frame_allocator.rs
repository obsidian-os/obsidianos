/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Physical memory's frames allocator
use alloc::prelude::v1::Vec;
use multiboot2::{MemoryArea, MemoryAreaIter, MemoryAreaType};
use x86_64::{
    structures::paging::{FrameAllocator, FrameDeallocator, PhysFrame, Size4KiB},
    PhysAddr,
};

use super::heap::{HEAP_SIZE, HEAP_START_PHYS};
use crate::boot_info::BOOT_INFO;

/// An allocator which gives frames of physical memory
pub struct NextFrameAllocator {
    /// The areas of physical memory
    ///
    /// See [OSDEV](https://wiki.osdev.org/Memory_Map_(x86))
    areas: MemoryAreaIter<'static>,

    /// The current area for new frames
    current_area: Option<&'static MemoryArea>,

    /// An area of deallocated frames
    free: Vec<PhysFrame>,

    /// The next frame of physical memory
    next: PhysFrame,
}

impl NextFrameAllocator {
    /// Create a new FrameAllocator with the given areas
    pub fn new() -> Self {
        let mut result = Self {
            areas: BOOT_INFO
                .get()
                .unwrap()
                .get()
                .memory_map_tag()
                .expect("Memory map tag required")
                .all_memory_areas(),
            current_area: None,
            free: vec![],
            next: PhysFrame::containing_address(PhysAddr::new(HEAP_START_PHYS + HEAP_SIZE as u64)),
        };

        result.choose_next_area();
        result
    }

    /// Try to get the current area
    ///
    /// This function returns the current area or panics
    fn try_current(&self) -> &MemoryArea {
        self.current_area
            .as_ref()
            .expect("Allocator not initialized or it is an Out Of Memory!")
    }

    /// Choose the next area
    fn choose_next_area(&mut self) {
        // Switch to the next area
        if let Some(area) = self.areas.next() {
            self.current_area = Some(area);
        } else {
            panic!("KERNEL PANIC: Out of memory!");
        }

        match self.current_area.as_ref().unwrap().typ() {
            MemoryAreaType::Available => {
                let start_frame = addr_to_frame(self.try_current().start_address());
                if self.next < start_frame {
                    self.next = start_frame;
                }
            }
            _ => {
                // This memory area is not available, try another
                self.choose_next_area();
            }
        }
    }

    /// Get a new frame of physical memory
    fn get_frame(&mut self) -> PhysFrame {
        let current_area = self.try_current();
        let current_area_last_frame =
            PhysFrame::containing_address(PhysAddr::new(current_area.end_address()));

        if !self.free.is_empty() {
            // The free frames are priority
            self.free.pop().unwrap()
        } else if self.next > current_area_last_frame {
            // It remains zero frames in this area, switch to another
            self.choose_next_area();
            self.get_frame()
        } else {
            // This frame is unused, use it
            self.next = self.next + 1;
            self.next
        }
    }
}

unsafe impl FrameAllocator<Size4KiB> for NextFrameAllocator {
    fn allocate_frame(&mut self) -> Option<PhysFrame> { Some(self.get_frame()) }
}

impl FrameDeallocator<Size4KiB> for NextFrameAllocator {
    unsafe fn deallocate_frame(&mut self, frame: PhysFrame) { self.free.push(frame); }
}

/// Translate an address to a frame
pub fn addr_to_frame(addr: u64) -> PhysFrame {
    PhysFrame::from_start_address(PhysAddr::new(addr)).expect("Not start address")
}
