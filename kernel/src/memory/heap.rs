/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Heap allocator
use linked_list_allocator::LockedHeap;

use super::{MULTIBOOT_SIZE, MULTIBOOT_START_PHYS, MULTIBOOT_START_VIRT};

/// The start of the kernel's heap (Virtual)
pub const HEAP_START_VIRT: u64 = MULTIBOOT_START_VIRT + MULTIBOOT_SIZE;

/// The start of the kernel's heap (Physical)
pub const HEAP_START_PHYS: u64 = MULTIBOOT_START_PHYS + MULTIBOOT_SIZE;

/// The size of the kernel's heap
pub const HEAP_SIZE: usize = 40 * 1024 * 1024;

#[global_allocator]
/// A global heap allocator to allocate all things of the `alloc` crate (Vec, String, Box, ...)
pub static HEAP_ALLOCATOR: LockedHeap = LockedHeap::empty();

/// Initialize the heap allocator
pub fn init() {
    unsafe {
        HEAP_ALLOCATOR
            .lock()
            .init(HEAP_START_VIRT as usize, HEAP_SIZE);
    }
}
