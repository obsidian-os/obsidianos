/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Memory management functions which include:
//!   * Kernel heap management
//!   * Virtual memory management, such as mappings
//!   * A frame allocator which allocate frames of physical memory memory which will be mapped
//!   later to a page of virtual memory
//!
//!                  Virtual memory                                       Physical memory
//!
//!   0xc140a000 |-------------------|                       0x140a000 |-------------------|
//!              |                   |                                 |                   |
//!              |    Kernel heap    |                                 |    Kernel heap    |
//!              |                   |                                 |                   |
//!   0xc100a000 |-------------------|                       0x100a000 |-------------------|
//!              |                   |  ---------------->              |                   |
//!              |     Multiboot     |  ---------------->              |     Multiboot     |
//!              |                   |                                 |                   |
//!   0xc1000000 |-------------------|                       0x1000000 |-------------------|
//!              |                   |                                 |                   |
//!              |      Kernel       |                                 |      Kernel       |
//!              |                   |                                 |                   |
//!   0xc0000000 ---------------------                             0x0 ---------------------
pub mod frame_allocator;
pub mod heap;
pub mod table;

use lazy_static::lazy_static;
use spin::{Mutex, Once};
use x86_64::{
    instructions::tlb,
    registers::control::{Cr3, Cr3Flags},
    structures::paging::{
        frame::PhysFrame,
        mapper::{Mapper, RecursivePageTable},
        page_table::PageTableFlags as Flags,
        FrameAllocator, Page, PageTable, PageTableIndex, Size4KiB,
    },
    PhysAddr, VirtAddr,
};

use self::heap::{HEAP_SIZE, HEAP_START_PHYS, HEAP_START_VIRT};
use self::table::ProcessPageTable;
use frame_allocator::NextFrameAllocator;

/// The address of the temporary page (to map the kernel page table)
pub const TMP_PAGE_ADDR: u64 = HEAP_START_VIRT as u64 + HEAP_SIZE as u64 + PAGE_SIZE as u64;

/// The address of the second temporary page (various uses)
pub const TMP_PAGE_ADDR_2: u64 = TMP_PAGE_ADDR + PAGE_SIZE as u64;

/// The start of the kernel in the Higher Half part (Virtual)
pub const KERNEL_START_VIRT: u64 = 0xc0000000;

/// The start of the kernel in the Higher Half part (Physical)
pub const KERNEL_START_PHYS: u64 = 0;

/// The size of the kernel
pub const KERNEL_SIZE: u64 = 0x1e00000;

/// The start of the multiboot information structure (Virtual)
pub const MULTIBOOT_START_VIRT: u64 = KERNEL_START_VIRT + KERNEL_SIZE;

/// The start of the multiboot information structure (Physical)
pub const MULTIBOOT_START_PHYS: u64 = KERNEL_START_PHYS + KERNEL_SIZE;

/// The size of the multiboot information structure
pub const MULTIBOOT_SIZE: u64 = 0xa000;

/// The size of a page of virtual memory
pub const PAGE_SIZE: usize = 4096;

/// The index of the recursive page
pub const RECURSIVE_PAGE_INDEX: u16 = 511;

lazy_static! {
    /// A global kernel memory mapper
    pub static ref KERNEL_MAPPER: Once<Mutex<RecursivePageTable<'static>>> = Once::new();
}

lazy_static! {
    /// A global frame allocator
    pub static ref FRAME_ALLOCATOR: Once<Mutex<NextFrameAllocator>> = Once::new();
}

lazy_static! {
    /// The frame which represents the kernel's page table
    pub static ref KERNEL_PAGE_TABLE: Once<PhysFrame> = Once::new();
}

/// Remap the kernel, initialize the kernel memory mapper and the allocator
pub fn init() {
    init_allocator();
    init_mapper();

    let frame = PhysFrame::from_start_address(PhysAddr::new(MULTIBOOT_START_PHYS)).unwrap();

    let mapper = remap_kernel(frame);

    KERNEL_PAGE_TABLE.call_once(|| frame);

    // Use the new page table
    *KERNEL_MAPPER.get().unwrap().lock() = mapper;
}

/// Initialize the mapper
fn init_mapper() {
    KERNEL_MAPPER.call_once(|| {
        Mutex::new(RecursivePageTable::new(active_table()).expect("cannot create kernel mapper"))
    });
}

/// Initialize the frame allocator
fn init_allocator() { FRAME_ALLOCATOR.call_once(|| Mutex::new(NextFrameAllocator::new())); }

/// Remap the kernel and switch to a new page table
fn remap_kernel(frame: PhysFrame) -> RecursivePageTable<'static> {
    // Create a new table
    let table = ProcessPageTable::new(frame);

    // Change the 511th entry of the current page table and put instead the frame of the page table
    active_table()[RECURSIVE_PAGE_INDEX as usize].set_frame(table.frame(), Flags::PRESENT | Flags::WRITABLE);

    let mut mapper =
        unsafe { RecursivePageTable::new_unchecked(table.table(), PageTableIndex::new(RECURSIVE_PAGE_INDEX)) };

    // Flush the TLB
    tlb::flush_all();

    // Map the kernel
    // TODO: Use the multiboot2 elf_section_tags function to map the kernel segments with the right
    // flags
    for (i, page) in Page::<Size4KiB>::range_inclusive(
        Page::containing_address(VirtAddr::new(KERNEL_START_VIRT)),
        Page::containing_address(VirtAddr::new(KERNEL_START_VIRT + KERNEL_SIZE)),
    )
    .enumerate()
    {
        let frame = PhysFrame::containing_address(PhysAddr::new(
            KERNEL_START_PHYS + i as u64 * PAGE_SIZE as u64,
        ));
        unsafe {
            mapper
                .map_to(
                    page,
                    frame,
                    Flags::PRESENT | Flags::WRITABLE,
                    &mut *FRAME_ALLOCATOR.get().unwrap().lock(),
                )
                .unwrap()
                .ignore();
        }
    }

    // Map the kernel heap
    for (i, page) in Page::<Size4KiB>::range_inclusive(
        Page::containing_address(VirtAddr::new(HEAP_START_VIRT)),
        Page::containing_address(VirtAddr::new(HEAP_START_VIRT + HEAP_SIZE as u64)),
    )
    .enumerate()
    {
        let frame = PhysFrame::containing_address(PhysAddr::new(
            HEAP_START_PHYS + i as u64 * PAGE_SIZE as u64,
        ));
        unsafe {
            mapper
                .map_to(
                    page,
                    frame,
                    Flags::PRESENT | Flags::WRITABLE | Flags::NO_EXECUTE,
                    &mut *FRAME_ALLOCATOR.get().unwrap().lock(),
                )
                .unwrap()
                .ignore();
        }
    }

    // Map the page table itself
    unsafe {
        mapper
            .identity_map(
                table.frame(),
                Flags::PRESENT | Flags::WRITABLE | Flags::NO_EXECUTE,
                &mut *FRAME_ALLOCATOR.get().unwrap().lock(),
            )
            .unwrap()
            .ignore();
    }

    // Use the new kernel page table
    unsafe {
        table.switch();
    }

    mapper
}

/// Map a range of virtual pages to a range of physical frames
pub fn map(start: u64, end: u64, flags: Flags) {
    let page_range = {
        let start_addr = VirtAddr::new(start);
        let end_addr = VirtAddr::new(end);
        let start_page = Page::containing_address(start_addr);
        let end_page = Page::containing_address(end_addr);
        Page::range_inclusive(start_page, end_page)
    };

    for page in page_range {
        let frame = FRAME_ALLOCATOR
            .get()
            .unwrap()
            .lock()
            .allocate_frame()
            .expect("cannot allocate frame");

        unsafe {
            KERNEL_MAPPER.get().unwrap().lock().map_to(
                page,
                frame,
                flags,
                &mut *FRAME_ALLOCATOR.get().unwrap().lock(),
            )
        }
        .expect("Mapping error")
        .ignore(); // Ignore mapping because we will flush all mappings after
    }
}

/// Get the active table at the start of the kernel
fn active_table() -> &'static mut PageTable { unsafe { to_page_table(0xffffffff_fffff000) } }

/// Translate an address into a page table
/// Very unsafe
pub unsafe fn to_page_table(addr: u64) -> &'static mut PageTable {
    &mut *((addr) as *mut PageTable)
}

/// Switch to the kernel's page table
pub fn switch_to_kernel_page_table() {
    unsafe { Cr3::write(*KERNEL_PAGE_TABLE.get().unwrap(), Cr3Flags::empty()) }
}
