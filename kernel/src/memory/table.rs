/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Process' page tables management
use x86_64::{
    instructions::tlb,
    registers::control::{Cr3, Cr3Flags},
    structures::paging::{
        frame::PhysFrame,
        mapper::{Mapper, MapToError, UnmapError, TranslateError, RecursivePageTable},
        page_table::{PageTableFlags as Flags, PageTableEntry},
        page::NotGiantPageSize,
        Page, PageTable,
        PageTableIndex, Size4KiB, PageSize,
    },
    VirtAddr,
};

use super::{
    TMP_PAGE_ADDR,
    RECURSIVE_PAGE_INDEX,
    FRAME_ALLOCATOR,
    KERNEL_MAPPER,
    to_page_table,
};

#[derive(PartialEq, Debug, Clone)]
/// A process' page table which contains the mappings between a frame and a page
pub struct ProcessPageTable {
    p4_frame: PhysFrame,
}

impl ProcessPageTable {
    /// Create and initialize a new ProcessPageTable
    pub fn new(frame: PhysFrame) -> Self {
        let mut temporary_page =
            TemporaryPage::new(Page::containing_address(VirtAddr::new(TMP_PAGE_ADDR)));
        {
            let table = temporary_page.map_table_frame(
                frame,
                Flags::PRESENT | Flags::WRITABLE | Flags::NO_EXECUTE,
            );
            // now we are able to zero the table
            table.zero();
            // set up recursive mapping for the table
            table[RECURSIVE_PAGE_INDEX as usize].set_frame(
                frame,
                Flags::PRESENT | Flags::WRITABLE | Flags::NO_EXECUTE,
            );
        }
        temporary_page.unmap();

        Self { p4_frame: frame }
    }

    /// Get a PageTableEntry from an address
    ///
    /// This code is directly imported from the `x86_64` crate
    pub fn get_entry(&self, addr: u64) -> Result<PageTableEntry, TranslateError> {
        let page = Page::containing_address(VirtAddr::new(addr));
        let mut temporary_page =
            TemporaryPage::new(Page::containing_address(VirtAddr::new(TMP_PAGE_ADDR)));
        let p4 = temporary_page.map_table_frame(
            self.frame(),
            Flags::PRESENT | Flags::WRITABLE | Flags::NO_EXECUTE,
        );

        // Create a backup of the current page table
        let backup = Cr3::read().0;

        // Change the 511th entry of the current page table and put instead the frame of the page table
        unsafe {
            to_page_table(backup.start_address().as_u64())[511]
                .set_frame(self.frame(), Flags::PRESENT | Flags::WRITABLE)
        };

        // Flush the TLB
        tlb::flush_all();

        if p4[page.p4_index()].is_unused() {
            return Err(TranslateError::PageNotMapped);
        }

        let p3 = unsafe { &*(p3_ptr(page,  PageTableIndex::new(RECURSIVE_PAGE_INDEX as u16))) };
        let p3_entry = &p3[page.p3_index()];

        if p3_entry.is_unused() {
            return Err(TranslateError::PageNotMapped);
        }

        let p2 = unsafe { &*(p2_ptr(page,  PageTableIndex::new(RECURSIVE_PAGE_INDEX as u16))) };
        let p2_entry = &p2[page.p2_index()];

        if p2_entry.is_unused() {
            return Err(TranslateError::PageNotMapped);
        }

        let p1 = unsafe { &*(p1_ptr(page,  PageTableIndex::new(RECURSIVE_PAGE_INDEX as u16))) };
        let p1_entry = p1[page.p1_index()].clone();

        if p1_entry.is_unused() {
            return Err(TranslateError::PageNotMapped);
        }

        // Restore the 511th entry
        unsafe {
            to_page_table(backup.start_address().as_u64())[511]
                .set_frame(backup, Flags::PRESENT | Flags::WRITABLE);
        }

        // Flush the TLB
        tlb::flush_all();

        // Unmap the temporary page
        temporary_page.unmap();

        Ok(p1_entry)
    }

    /// Set a PageTableEntry from an address
    ///
    /// This code is directly imported from the `x86_64` crate
    pub fn set_entry(&self, addr: u64, entry: PageTableEntry) -> Result<(), TranslateError> {
        let page = Page::containing_address(VirtAddr::new(addr));
        let mut temporary_page =
            TemporaryPage::new(Page::containing_address(VirtAddr::new(TMP_PAGE_ADDR)));
        let p4 = temporary_page.map_table_frame(
            self.frame(),
            Flags::PRESENT | Flags::WRITABLE | Flags::NO_EXECUTE,
        );

        // Create a backup of the current page table
        let backup = Cr3::read().0;

        // Change the 511th entry of the current page table and put instead the frame of the page table
        unsafe {
            to_page_table(backup.start_address().as_u64())[511]
                .set_frame(self.frame(), Flags::PRESENT | Flags::WRITABLE)
        };

        // Flush the TLB
        tlb::flush_all();

        if p4[page.p4_index()].is_unused() {
            return Err(TranslateError::PageNotMapped);
        }

        let p3 = unsafe { &*(p3_ptr(page,  PageTableIndex::new(RECURSIVE_PAGE_INDEX as u16))) };
        let p3_entry = &p3[page.p3_index()];

        if p3_entry.is_unused() {
            return Err(TranslateError::PageNotMapped);
        }

        let p2 = unsafe { &*(p2_ptr(page,  PageTableIndex::new(RECURSIVE_PAGE_INDEX as u16))) };
        let p2_entry = &p2[page.p2_index()];

        if p2_entry.is_unused() {
            return Err(TranslateError::PageNotMapped);
        }

        let p1 = unsafe { &mut *(p1_ptr(page,  PageTableIndex::new(RECURSIVE_PAGE_INDEX as u16))) };
        let p1_entry = &mut p1[page.p1_index()];

        if p1_entry.is_unused() {
            return Err(TranslateError::PageNotMapped);
        }

        *p1_entry = entry;

        // Restore the 511th entry
        unsafe {
            to_page_table(backup.start_address().as_u64())[511]
                .set_frame(backup, Flags::PRESENT | Flags::WRITABLE);
        }

        // Flush the TLB
        tlb::flush_all();

        // Unmap the temporary page
        temporary_page.unmap();

        Ok(())
    }

    /// Get the frame containing the process' page table
    pub fn frame(&self) -> PhysFrame { self.p4_frame }

    /// Prepare and create a new mapper of the process' page table
    pub fn mapper<F: FnOnce(&mut RecursivePageTable)>(&self, f: F) {
        // Create a new temporary page and map the page table
        let mut temporary_page =
            TemporaryPage::new(Page::containing_address(VirtAddr::new(TMP_PAGE_ADDR)));
        let table = temporary_page.map_table_frame(
            self.frame(),
            Flags::PRESENT | Flags::WRITABLE | Flags::NO_EXECUTE,
        );

        // Create a backup of the current page table
        let backup = Cr3::read().0;

        // Change the 511th entry of the current page table and put instead the frame of the page table
        unsafe {
            to_page_table(backup.start_address().as_u64())[511]
                .set_frame(self.frame(), Flags::PRESENT | Flags::WRITABLE)
        };

        // Flush the TLB
        tlb::flush_all();

        // Create a new mapper of the page table
        let mut mapper =
            unsafe { RecursivePageTable::new_unchecked(table, PageTableIndex::new(511)) };

        // Call the function with the new mapper
        f(&mut mapper);

        // Restore the 511th entry
        unsafe {
            to_page_table(backup.start_address().as_u64())[511]
                .set_frame(backup, Flags::PRESENT | Flags::WRITABLE);
        }

        // Flush the TLB
        tlb::flush_all();

        // Unmap the temporary page
        temporary_page.unmap();
    }

    /// Map all pages between two addresses in the process' page table
    pub fn map(
        &self,
        page: Page,
        frame: PhysFrame,
        mut flags: Flags,
    ) -> Result<(), MapToError<Size4KiB>> {
        // Add the USER_ACCESSIBLE flag to make sure that the content can be accessed in
        // userspace
        flags.set(Flags::USER_ACCESSIBLE, true);

        let mut ret = Ok(());
        self.mapper(|mapper| {
            unsafe {
                if let Err(e) = mapper
                    .map_to_with_table_flags(
                        page,
                        frame,
                        flags,
                        Flags::PRESENT | Flags::USER_ACCESSIBLE,
                        &mut *FRAME_ALLOCATOR.get().unwrap().lock(),
                    ) {
                    ret = Err(e);
                }
            }
        });

        ret
    }

    /// Unmap a page
    pub fn unmap(&self, page: Page) -> Result<(), UnmapError> {
        let mut ret = Ok(());
        self.mapper(|mapper| {
            ret = mapper.unmap(page).and(Ok(()));
        });

        ret
    }

    /// Get the process' page table
    pub unsafe fn table(&self) -> &'static mut PageTable {
        to_page_table(self.p4_frame.start_address().as_u64())
    }

    /// Switch to the process' page table
    pub unsafe fn switch(&self) { Cr3::write(self.p4_frame, Cr3Flags::empty()); }

    /// Use process' page table in a closure
    pub fn with<F: FnOnce()>(&self, f: F) {
        // Create a backup of the current page table
        let backup = Cr3::read().0;

        unsafe {
            // Change the current page table
            Cr3::write(self.p4_frame, Cr3Flags::empty());

            f();

            // Switch to the previous page table
            Cr3::write(backup, Cr3Flags::empty());
        }
    }
}

#[derive(PartialEq, Debug, Clone)]
/// A temporary page
pub struct TemporaryPage {
    page: Page,
}

impl TemporaryPage {
    /// Create a new temporary page
    pub fn new(page: Page) -> TemporaryPage { TemporaryPage { page: page } }

    /// Maps the temporary page to the given frame in the active table.
    /// Returns the start address of the temporary page.
    pub fn map(&mut self, frame: PhysFrame, flags: Flags) -> u64 {
        unsafe {
            KERNEL_MAPPER
                .get()
                .unwrap()
                .lock()
                .map_to(
                    self.page,
                    frame,
                    flags,
                    &mut *FRAME_ALLOCATOR.get().unwrap().lock(),
                ).ok();
        }
        self.page.start_address().as_u64()
    }

    /// Maps the temporary page to the given page table frame in the active
    /// table. Returns a reference to the now mapped table.
    pub fn map_table_frame(&mut self, frame: PhysFrame, flags: Flags) -> &mut PageTable {
        unsafe { &mut *(self.map(frame, flags) as *mut PageTable) }
    }

    /// Unmaps the temporary page in the active table.
    pub fn unmap(&mut self) {
        KERNEL_MAPPER
            .get()
            .unwrap()
            .lock()
            .unmap(self.page)
            .unwrap()
            .1
            .flush();
    }
}

#[inline]
fn p3_ptr<S: PageSize>(page: Page<S>, recursive_index: PageTableIndex) -> *mut PageTable {
    p3_page(page, recursive_index).start_address().as_mut_ptr()
}

#[inline]
fn p3_page<S: PageSize>(page: Page<S>, recursive_index: PageTableIndex) -> Page {
    Page::from_page_table_indices(
        recursive_index,
        recursive_index,
        recursive_index,
        page.p4_index(),
    )
}

#[inline]
fn p2_ptr<S: NotGiantPageSize>(page: Page<S>, recursive_index: PageTableIndex) -> *mut PageTable {
    p2_page(page, recursive_index).start_address().as_mut_ptr()
}

#[inline]
fn p2_page<S: NotGiantPageSize>(page: Page<S>, recursive_index: PageTableIndex) -> Page {
    Page::from_page_table_indices(
        recursive_index,
        recursive_index,
        page.p4_index(),
        page.p3_index(),
    )
}

#[inline]
fn p1_ptr(page: Page<Size4KiB>, recursive_index: PageTableIndex) -> *mut PageTable {
    p1_page(page, recursive_index).start_address().as_mut_ptr()
}

#[inline]
fn p1_page(page: Page<Size4KiB>, recursive_index: PageTableIndex) -> Page {
    Page::from_page_table_indices(
        recursive_index,
        page.p4_index(),
        page.p3_index(),
        page.p2_index(),
    )
}
