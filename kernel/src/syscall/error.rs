/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! This module defines the errors which can be used in the syscalls

/// An error happens during a syscall
#[derive(Debug, PartialOrd, PartialEq, Clone, Copy)]
pub enum Error {
    /// No such file or directory
    ENOENT = 2,

    /// Bad file descriptor
    EBADF = 9,

    /// Try again!
    EAGAIN = 11,

    /// Out of memory
    ENOMEM,

    /// Permission denied
    EACCES,

    /// File or directory exists
    EEXIST,

    /// Not a directory
    ENOTDIR = 20,

    /// Is a directory
    EISDIR,

    /// Invalid value
    EINVAL,

    /// Function not implemented
    ENOSYS = 38,

    /// File descriptor in bad state
    EBADFD = 77,
}

/// A return type for all syscalls
pub type Result = core::result::Result<usize, Error>;
