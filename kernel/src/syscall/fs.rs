/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Syscalls used to manage the filesystem (open, read, write, close, ...)
use alloc::prelude::v1::Vec;
use core::{
    slice::from_raw_parts,
    str::from_utf8,
    cmp,
};

use crate::fs::{
    file::{FileHandle, OpenFlags, Perms},
    error::FSError,
    ROOT,
};
use crate::tasking::scheduler::{SCHEDULER, PROCESSES};
use super::{
    error::{Error, Result},
    Syscall, get_string,
};

const STDIN: usize = 0;
const STDOUT: usize = 1;
const STDERR: usize = 2;

const AT_FDCWD: usize            = (-100 as isize) as usize;

#[allow(dead_code)]
const AT_SYMLINK_NOFOLLOW: usize = 0x100;

#[allow(dead_code)]
const AT_REMOVEDIR: usize        = 0x200;

#[allow(dead_code)]
const AT_SYMLINK_FOLLOW: usize   = 0x400;

#[allow(dead_code)]
const AT_NO_AUTOMOUNT: usize     = 0x800;

#[allow(dead_code)]
const AT_EMPTY_PATH: usize       = 0x1000;

#[derive(Debug, Default)]
#[allow(missing_docs)]
pub struct StatInfo {
    pub st_dev: u64,
    pub st_ino: u64,
    pub st_nlink: u64,
    pub st_mode: u32,
    pub st_uid: u32,
    pub st_gid: u32,
    pub __pad0: i32,
    pub st_rdev: u64,
    pub st_size: i64,
    pub st_blksize: i64,
    pub st_blocks: i64,
    pub st_atime: i64,
    pub st_atime_nsec: i64,
    pub st_mtime: i64,
    pub st_mtime_nsec: i64,
    pub st_ctime: i64,
    pub st_ctime_nsec: i64,
    pub __unused: [i64; 3],
}

#[derive(Debug)]
struct PipePair {
    fd: [u32; 2],
}

impl Syscall {
    /// Open a file
    pub fn open(&self, path: usize, flags: usize, mode: usize) -> Result { self.openat(AT_FDCWD, path, flags, mode) }

    /// Open a file
    ///
    /// If the pathname given in pathname is relative, then it is interpreted relative to the
    /// directory referred to by the file descriptor `dirfd`.
    pub fn openat(&self, _dirfd: usize, path: usize, flags: usize, mode: usize) -> Result {
        // TODO: `dirfd` is ignored for now
        let path = unsafe { get_string(path).unwrap() };
        let flags = OpenFlags::from_bits_truncate(flags as u32);

        print!("openat(AT_FDCWD, {:?}, {:?}, {}) = ", path, flags, mode);

        let mut processes = PROCESSES.write();
        let current = SCHEDULER.get().current().unwrap().process;
        let process = processes.get_mut(&current).unwrap();

        let handle = match FileHandle::open(path, flags, mode as u16) {
            Ok(h) => h,
            Err(e) => {
                println!("{:?}", Error::from(e));
                return Err(e.into());
            },
        };
        let id = usize::from(process.add_file(handle));

        println!("{}", id);
        Ok(id)
    }

    /// Read a file descriptor
    pub fn read(&self, fd: usize, buf: usize, count: usize) -> Result {
        // print!("read({}, {:#x}, {}) = ", fd, buf, count);
        let mut processes = PROCESSES.write();
        let current = SCHEDULER.get().current().unwrap().process;
        let process = processes.get_mut(&current).unwrap();

        match process.get_file_mut(fd) {
            Some(file) => {
                let dest = buf as *mut u8;
                match file.read(count) {
                    Ok(content) => {
                        let src = content.as_ptr();
                        let bytes_copied = cmp::min(content.len(), count);
                        unsafe { src.copy_to(dest, bytes_copied); }
                        file.offset += bytes_copied;

                        // println!("{}", bytes_copied);
                        Ok(bytes_copied)
                    },
                    Err(err) => {
                        let err = Error::from(err);

                        // println!("{:?}", err);
                        Err(err)
                    },
                }
            },
            None => {
                // println!("EBADF");
                Err(Error::EBADFD)
            },
        }
    }

    /// Write to the corresponding file descriptor
    pub fn write(&mut self, fd: usize, buf: usize, count: usize) -> Result {
        if fd != STDIN && fd != STDOUT {
            println!("write({}, {:#x}, {}) = ?", fd, buf, count);
        }
        let mut processes = PROCESSES.write();
        let current = SCHEDULER.get().current().unwrap().process;
        let process = processes.get_mut(&current).unwrap();

        match process.get_file_mut(fd) {
            Some(file) => {
                match file.write(unsafe { from_raw_parts(buf as *const u8, count) }) {
                    Ok(bytes_written) => {
                        Ok(bytes_written)
                    },
                    Err(err) => {
                        let err = Error::from(err);

                        if fd != STDIN && fd != STDOUT {
                            println!("{:?}", err);
                        }
                        Err(err)
                    },
                }
            },
            None => {
                if fd != STDIN && fd != STDOUT {
                    println!("EBADF");
                }
                Err(Error::EBADFD)
            },
        }
    }

    /// Write to the corresponding file descriptor
    pub fn writev(&mut self, fd: usize, iov: usize, iovcnt: usize) -> Result {
        #[derive(Debug, Clone, Copy)]
        struct IoVec {
            iov_base: u64,
            iov_len: usize,
        }

        let iov = unsafe { *(iov as *const IoVec) };

        match fd {
            STDIN | STDOUT | STDERR => {
                let content = unsafe { from_utf8(from_raw_parts(iov.iov_base as *const u8, iov.iov_len)) }
                    .expect("cannot transform to utf8");

                print!("{}", content);
                Ok(iov.iov_len)
            }
            _ => {
                println!("writev({}, {:#?}, {})", fd, iov, iovcnt);
                println!("We can't write in files for now. Sorry!");
                Err(Error::ENOSYS)
            }
        }
    }

    /// Close a file descriptor
    pub fn close(&self, fd: usize) -> Result {
        println!("close({})", fd);
        let mut processes = PROCESSES.write();
        let current = SCHEDULER.get().current().unwrap().process;
        let process = processes.get_mut(&current).unwrap();

        process.files.remove(&fd);
        Ok(0)
    }

    /// Do a file operation on a file descriptor
    pub fn fcntl(&self, fd: usize, cmd: usize, arg: usize) -> Result {
        // TODO: Fcntl
        println!("fcntl({:#x}, {:#x}, {:#x}) = 0", fd, cmd, arg);
        Ok(0)
    }

    /// Wait for an event on a file
    pub fn poll(&self, fds: usize, nfds: usize, timeout: usize) -> Result {
        // TODO: poll
        println!("poll({:#x}, {:#x}, {}) = 0", fds, nfds, timeout);
        Ok(0)
    }

    /// Create a pipe between two processes
    pub fn pipe2(&self, pipefd: usize, flags: usize) -> Result {
        /* TODO: Pipe2 syscall */
        let pair;
        unsafe {
            pair = &mut *(pipefd as *mut PipePair);
        }

        pair.fd[0] = 3;
        pair.fd[1] = 4;

        println!("pipe2({:?}, {}) = 0", pair, flags);

        Ok(0)
    }

    /// Get file status with a path
    pub fn stat(&self, path: usize, buf: usize) -> Result {
        let path = unsafe { get_string(path).unwrap() };
        let stat = unsafe { &mut *(buf as *mut StatInfo) };

        print!("stat({:?}, {:#x}) = ", path, buf);

        match FileHandle::open(path, OpenFlags::empty(), 0o777) {
            Ok(file) => {
                let metadata = file.metadata()?;
                *stat = StatInfo::from(metadata);

                println!("{}", 0);
                Ok(0)
            },
            Err(err) => {
                let err = Error::from(err);
                println!("{:?}", err);
                Err(err)
            },
        }
    }

    /// Get file status with a file descriptor
    pub fn fstat(&self, fd: usize, buf: usize) -> Result {
        let mut processes = PROCESSES.write();
        let current = SCHEDULER.get().current().unwrap().process;
        let process = processes.get_mut(&current).unwrap();
        let stat = unsafe { &mut *(buf as *mut StatInfo) };

        print!("fstat({}, {:#x}) = ", fd, buf);

        match process.get_file(fd) {
            Some(file) => {
                let metadata = file.metadata()?;
                *stat = StatInfo::from(metadata);

                println!("{}", 0);
                Ok(0)
            },
            None => {
                println!("ENOENT");
                Err(Error::ENOENT)
            },
        }
    }

    /// Check if the current process is allowed to access a file
    pub fn access(&self, path: usize, mode: usize) -> Result {
        // TODO: Access syscall
        let path = unsafe { get_string(path).unwrap() };
        print!("access({:?}, {}) = ", path, mode);

        println!("ENOENT");
        Err(Error::ENOENT)
    }

    /// Duplicate a file descriptor
    pub fn dup(&self, oldfd: usize) -> Result {
        print!("dup({}) = ", oldfd);

        let mut processes = PROCESSES.write();
        let current = SCHEDULER.get().current().unwrap().process;
        let process = processes.get_mut(&current).unwrap();
        let old_handle = process.get_file(oldfd).ok_or(Error::EBADF)?.clone();
        let newfd = process.add_file(old_handle);
        // TODO: Share the offset (Arc?) and reset CLOEXEC

        println!("{:#?}", newfd);
        Ok(newfd)
    }

    /// Create a special or ordinary file
    ///
    /// This syscall is used to create either a simple empty file or a special file like a socket,
    /// a FIFO (also called named pipe), a block device or a char device
    pub fn mknod(&self, path: usize, mode: usize, dev: usize) -> Result {
        let path = unsafe { get_string(path).unwrap() };
        let mode = Perms::from_bits_truncate(mode as u32);

        print!("mknod({:?}, {:?}, {}) = ", path, mode, dev);

        crate::memory::switch_to_kernel_page_table();
        let root = ROOT.read();
        let mut path = path.split("/").collect::<Vec<&'static str>>();
        if let Some(name) = path.pop() {
            // Try to open the parent directory
            match root.open(path.join("/").as_str()) {
                Ok(i) => {
                    // The parent directory exists, create the inode and return it
                    let (type_, mode) = mode.split();
                    i.write().create(name, type_, mode)?;
                },
                Err(e) => {
                    // The parent directory does not exist (or other error), abort
                    let error = e.into();
                    println!("{:?}", error);

                    return Err(error);
                },
            }
        } else {
            println!("ENOENT");
            return Err(FSError::NotFound.into());
        }

        println!("0");
        Ok(0)
    }
}
