/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Syscalls used to manage the memory
use alloc::{
    prelude::v1::Vec,
    sync::Arc,
};
use bitflags::bitflags;
use x86_64::{
    structures::paging::page_table::PageTableFlags as Flags,
};

use super::{
    error::{Error, Result},
    Syscall,
};
use crate::tasking::{
    memory::{MemoryArea, handlers::ZeroHandler},
    scheduler::{PROCESSES, SCHEDULER},
};
use crate::{
    memory::PAGE_SIZE,
    common::align_up,
};

bitflags! {
    struct MmapFlags: u32 {
        const SHARED = 1 << 0;
        const PRIVATE = 1 << 1;
        const FIXED = 1 << 4;
        const ANONYMOUS = 1 << 5;
        const MAP_FIXED_NOREPLACE = 1 << 20;
    }
}

bitflags! {
    struct MmapProt: u32 {
        const NONE = 0;
        const READ = 1 << 0;
        const WRITE = 1 << 1;
        const EXEC = 1 << 2;
    }
}

impl From<MmapProt> for Flags {
    fn from(prot: MmapProt) -> Flags {
        let mut flags = Flags::NO_EXECUTE;

        if prot.contains(MmapProt::READ) {
            flags.insert(Flags::PRESENT);
        }

        if prot.contains(MmapProt::WRITE) {
            flags.insert(Flags::WRITABLE);
        }

        if prot.contains(MmapProt::EXEC) {
            flags.remove(Flags::NO_EXECUTE);
        }

        flags.insert(Flags::USER_ACCESSIBLE);

        flags
    }
}

impl Syscall {
    /// Map files or devices into memory
    pub fn mmap(
        &self,
        addr: usize,
        len: usize,
        prot: usize,
        flags: usize,
        fd: usize,
        offset: usize,
    ) -> Result {
        // Use the kernel's page table
        crate::memory::switch_to_kernel_page_table();

        let prot = MmapProt::from_bits_truncate(prot as u32);
        let flags = MmapFlags::from_bits_truncate(flags as u32);
        let addr = addr as u64;
        let len = len as u64;

        // Return EINVAL if length is null
        if len == 0 {
            println!("EINVAL");
            return Err(Error::EINVAL);
        }

        print!(
            "mmap({:#x}, {}, {:?}, {:?}, {:#x}, {}) = ",
            addr, len, prot, flags, fd, offset
        );

        let mut processes = PROCESSES.write();

        // Get the current thread's process
        let current_process = SCHEDULER.get().current().unwrap().process;
        let current_process = processes
            .get_mut(&current_process)
            .unwrap();

        let mut area = MemoryArea::new(
            addr as u64,
            addr as u64 + len as u64,
            Flags::from(prot),
            Arc::new(ZeroHandler {}),
        );

        if flags.contains(MmapFlags::FIXED) {
            if addr % PAGE_SIZE as u64 == 0 {
                if let Some(overlapping_area) = current_process.memory.get_area(addr as u64, (addr + len) as u64) {
                    // If the flag MAP_FIXED_NOREPLACE is set and the area is overlapping with
                    // another, return EEXIST
                    if flags.contains(MmapFlags::MAP_FIXED_NOREPLACE) {
                        println!("EEXIST");
                        return Err(Error::EEXIST);
                    }

                    let start_addr = addr;
                    let end_addr = align_up(addr + len, PAGE_SIZE);

                    let mut added_areas: Vec<MemoryArea> = vec![];
                    let overlapping_area = current_process.memory.areas.remove(overlapping_area);

                    if overlapping_area.start >= start_addr && overlapping_area.end <= end_addr {
                        // The overlapping area is in the middle of the new area
                        //
                        // $$$$|--|$$$$
                        // $   |  |   $
                        // $$$$|--|$$$$

                        // Unmap the area
                        overlapping_area.unmap(&mut current_process.memory).unwrap(); // TODO: Check errors
                    } else if overlapping_area.start >= start_addr && overlapping_area.start < end_addr {
                        // The overlapping area is overlapping only with the end of the new area
                        //
                        // |---$$|$$$$$
                        // |   $ |    $
                        // |---$$|$$$$$

                        // Unmap the area
                        MemoryArea::new(
                            overlapping_area.start,
                            end_addr,
                            overlapping_area.flags,
                            overlapping_area.handler.clone(),
                        ).unmap(&mut current_process.memory).unwrap(); // TODO: Check errors

                        // Create a new area
                        let new_area = MemoryArea::new(
                            end_addr,
                            overlapping_area.end,
                            overlapping_area.flags,
                            overlapping_area.handler,
                        );
                        added_areas.push(new_area);
                    } else if overlapping_area.end <= end_addr && overlapping_area.end > start_addr {
                        // The overlapping area is overlapping only with the start of the new area
                        //
                        // $$$$|$$----|
                        // $   | $    |
                        // $$$$|$$----|

                        // Unmap the area
                        MemoryArea::new(
                            start_addr,
                            overlapping_area.end,
                            overlapping_area.flags,
                            overlapping_area.handler.clone(),
                        ).unmap(&mut current_process.memory).unwrap(); // TODO: Check errors

                        // Create a new area
                        let new_area = MemoryArea::new(
                            overlapping_area.start,
                            start_addr,
                            overlapping_area.flags,
                            overlapping_area.handler,
                        );
                        added_areas.push(new_area);
                    } else {
                        // The overlapping area is divided in two by the new area
                        //
                        // |---$$$$---|
                        // |   $  $   |
                        // |---$$$$---|

                        // Unmap the area
                        MemoryArea::new(
                            start_addr,
                            end_addr,
                            overlapping_area.flags,
                            overlapping_area.handler.clone(),
                        ).unmap(&mut current_process.memory).unwrap(); // TODO: Check errors

                        // Create two new areas
                        let new_area = MemoryArea::new(
                            overlapping_area.start,
                            start_addr,
                            overlapping_area.flags,
                            overlapping_area.handler.clone(),
                        );
                        added_areas.push(new_area);

                        let new_area = MemoryArea::new(
                            end_addr,
                            overlapping_area.end,
                            overlapping_area.flags,
                            overlapping_area.handler,
                        );
                        added_areas.push(new_area);
                    }

                    current_process.memory.areas.extend(added_areas);
                }
            } else {
                println!("EINVAL");
                return Err(Error::EINVAL);
            }
        } else {
            let free_area_start = current_process
                .memory
                .find_free_area(addr as u64, len as usize);

            area.start = free_area_start;
            area.end = free_area_start + len;
        }

        if flags.contains(MmapFlags::ANONYMOUS) {
            if flags.contains(MmapFlags::SHARED) {
                // TODO: Shared memory + preserve during cow
                area.map(&mut current_process.memory).unwrap();

                current_process
                    .memory
                    .switch_page_table();

                println!("{:#x}", area.start);
                Ok(area.start as usize)
            } else {
                area.map(&mut current_process.memory).unwrap();

                current_process
                    .memory
                    .switch_page_table();

                println!("{:#x}", area.start);
                Ok(area.start as usize)
            }
        } else {
            match current_process.get_file(fd) {
                Some(file) => {
                    match file.clone().mmap(offset, &mut area, &mut current_process.memory) {
                        Ok(_) => (),
                        Err(_) => {
                            println!("ENOMEM");
                            return Err(Error::ENOMEM);
                        },
                    }
                },
                None => {
                    println!("ENOENT");
                    return Err(Error::ENOENT);
                },
            }

            current_process
                .memory
                .switch_page_table();

            println!("{:#x}", area.start);
            Ok(area.start as usize)
        }
    }

    /// Set protection on a region of memory
    pub fn mprotect(&self, addr: usize, len: usize, prot: usize) -> Result {
        // TODO: mprotect
        println!("mprotect({:#x}, {}, {}) = 0", addr, len, prot);
        Ok(0)
    }

    /// Unmap a memory area
    pub fn munmap(&self, addr: usize, len: usize) -> Result {
        println!("munmap({:#x}, {})", addr, len);

        // Return EINVAL if the length is null
        if len == 0 {
            return Err(Error::EINVAL);
        }

        // TODO: munmap

        Ok(0)
    }

    /// Change data segment size
    pub fn brk(&self, addr: usize) -> Result {
        print!("brk({:#x}) = ", addr);

        if addr == 0 {
            let processes = PROCESSES.read();

            // Get the current thread's process
            let current = SCHEDULER.get().current().unwrap().process;

            if let Some(data_segment) = processes.get(&current).unwrap().memory.data_segment {
                println!("{:#x}", data_segment.0);
                Ok(data_segment.0 as usize)
            } else {
                println!("ENOMEM");
                Err(Error::ENOMEM)
            }
        } else {
            // TODO: Change program break
            println!("{:#x}", addr);
            Ok(addr)
        }
    }
}
