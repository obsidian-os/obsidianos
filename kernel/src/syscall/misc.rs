/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Other syscalls
use x86_64::{registers::model_specific::FsBase, VirtAddr};

use super::{error::Result, Syscall};

impl Syscall {
    /// Set or get some architecture specific registers
    pub fn arch_prctl(&mut self, code: usize, addr: usize) -> Result {
        println!("arch_prctl({:#x}, {:#x}) = 0", code, addr);
        const ARCH_SET_FS: usize = 0x1002;

        match code {
            ARCH_SET_FS => {
                FsBase::write(VirtAddr::new(addr as u64));
            }
            _ => (),
        }
        Ok(0)
    }

    /// Get some informations about the Operating System
    pub fn uname(&self, buf: usize) -> Result {
        println!("uname({:#x}) = 0", buf);
        let uname = [
            "ObsidianOS",
            "user",
            "5.7.0", // HACK: Cheat to get ld.so working! Should be option_env!("CARGO_PKG_VERSION").unwrap_or("unknown version")
            "5.7.0", // HACK: Cheat to get ld.so working! Should be option_env!("CARGO_PKG_VERSION").unwrap_or("unknown version")
            "machine",
            "domain"
        ];
        let offset = 65;

        uname.iter().enumerate().for_each(|(i, s)| {
            let ptr = (buf + i * offset) as *mut u8;
            let addr = s.as_ptr();
            let len = s.len();

            unsafe {
                ptr.copy_from(addr, len);
                ptr.add(len).write(0);
            }
        });

        Ok(0)
    }

    /// Fast userspace locking
    pub fn futex(&self, _: usize, _: usize, _: usize, _: usize) -> Result { Ok(0) }
}
