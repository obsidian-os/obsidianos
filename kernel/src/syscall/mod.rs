/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Syscall management. There are organized in some subcategories:
//! *  **Process** for syscalls used for tasking (exit, ...)
//! *  **Filesystem** for syscalls used to manage the filesystem (open, read, write, close, ...)
//! *  **Memory** for syscalls used to manage the memory (mmap, brk, ...)
//! *  **Misc** for the others
use alloc::prelude::v1::{Vec, Box, String, ToString};
use core::slice::from_raw_parts;
use x86::msr;

use self::error::Error;
use crate::tasking::context::Registers;

pub mod error;
pub mod fs;
pub mod memory;
pub mod misc;
pub mod process;

/// Init the syscalls by setting some [model specific registers](https://en.wikipedia.org/wiki/Model-specific_register).
pub unsafe fn init() {
    use crate::gdt;

    msr::wrmsr(msr::IA32_STAR, ((gdt::GDT_KERNEL_CODE as u64) << 3) << 32);
    msr::wrmsr(msr::IA32_LSTAR, syscall as u64);
    msr::wrmsr(msr::IA32_FMASK, 1 << 9);
    msr::wrmsr(msr::IA32_KERNEL_GSBASE, &gdt::TSS as *const _ as u64);

    let efer = msr::rdmsr(msr::IA32_EFER);
    msr::wrmsr(msr::IA32_EFER, efer | 1);
}

#[naked]
/// The entry used when a syscall happened.
/// This function:
/// *  Saves the register's values
/// *  Gets the syscall's arguments
/// *  Calls the desired function
/// *  Restores the register's values
pub unsafe extern "C" fn syscall() {
    #[inline(never)]
    #[no_mangle]
    unsafe fn syscall_inner(stack: &'static mut Registers) -> isize {
        Syscall::new(stack).syscall()
    }

    asm!("swapgs",                // GS is now pointing to the TSS
        "mov gs:[28], rsp",       // Store the current stack pointer (to access TSS.rsp[2], we need to read the 4+8*3 = 28th byte)
        "mov rsp, gs:[4]",        // Use the kernel's stack pointer saved in the TSS (to access TSS.rsp[0], we need to read the 4th byte)
        "push 0x23",              // Data segment (fourth in the GDT, so (4 << 3) + 3 = 0x23)
        "push qword ptr gs:[28]", // Use the stack pointer saved in the TSS
        "push r11",               // Rflags register
        "push 0x1b",              // Code segment (third in the GDT, so (3 << 3) + 3 = 0x1b)
        "push rcx",               // Instruction pointer
        "swapgs",                 // Switch to the normal GS

        // Push scratch registers
        "push rax",
        "push rbx",
        "push rcx",
        "push rdx",
        "push rdi",
        "push rsi",
        "push r8",
        "push r9",
        "push r10",
        "push r11",
        "push r12",
        "push r13",
        "push r14",
        "push r15",
        "push rbp",
        "mov r11, cr3",
        "push r11",

        "mov rdi, rsp",
        "call syscall_inner",

        // The return value of the syscall (RAX) is already set because the return value
        // of a function is stored in RAX, so nothing to do!
        "pop r11",
        "mov cr3, r11",
        "pop rbp",
        "pop r15",
        "pop r14",
        "pop r13",
        "pop r12",
        "pop r11",
        "pop r10",
        "pop r9",
        "pop r8",
        "pop rsi",
        "pop rdi",
        "pop rdx",
        "pop rcx",
        "pop rbx",
        "add rsp, 8", // Don't `pop rax` but ignore it
        "iretq",
        options(noreturn),
    );
}

// --- Syscall numbers ---
const READ: usize = 0;
const WRITE: usize = 1;
const OPEN: usize = 2;
const CLOSE: usize = 3;
const STAT: usize = 4;
const FSTAT: usize = 5;
const POLL: usize = 7;
const MMAP: usize = 9;
const MPROTECT: usize = 10;
const MUNMAP: usize = 11;
const BRK: usize = 12;
const SIGACTION: usize = 13;
const SIGPROCMASK: usize = 14;
const SIGRETURN: usize = 15;
const WRITEV: usize = 20;
const ACCESS: usize = 21;
const DUP: usize = 32;
const CLONE: usize = 56;
const FORK: usize = 57;
const EXECVE: usize = 59;
const EXIT: usize = 60;
const WAIT4: usize = 61;
const KILL: usize = 62;
const UNAME: usize = 63;
const FCNTL: usize = 72;
const GETCWD: usize = 79;
const SIGALTSTACK: usize = 131;
const MKNOD: usize = 133;
const ARCH_PRCTL: usize = 158;
const IOPL: usize = 172;
const GETTID: usize = 186;
const TKILL: usize = 200;
const SET_TID_ADDR: usize = 218;
const FUTEX: usize = 202;
const EXIT_GROUP: usize = 231;
const OPENAT: usize = 257;
const PIPE2: usize = 293;

/// A structure used to call the right handler for each syscall
pub struct Syscall {
    stack: &'static mut Registers,
}

impl Syscall {
    /// Create a new Syscall
    pub fn new(stack: &'static mut Registers) -> Self { Self { stack } }

    /// Call the handler which matchs the syscall's number
    pub fn syscall(&mut self) -> isize {
        let num = self.stack.rax;
        let args = [
            self.stack.rdi,
            self.stack.rsi,
            self.stack.rdx,
            self.stack.r10,
            self.stack.r8,
            self.stack.r9,
        ];

        let result = match num {
            READ => self.read(args[0], args[1], args[2]),
            WRITE => self.write(args[0], args[1], args[2]),
            OPEN => self.open(args[0], args[1], args[2]),
            CLOSE => self.close(args[0]),
            STAT => self.stat(args[0], args[1]),
            FSTAT => self.fstat(args[0], args[1]),
            POLL => self.poll(args[0], args[1], args[2]),
            MMAP => self.mmap(args[0], args[1], args[2], args[3], args[4], args[5]),
            MPROTECT => self.mprotect(args[0], args[1], args[2]),
            MUNMAP => self.munmap(args[0], args[1]),
            BRK => self.brk(args[0]),
            SIGACTION => self.sigaction(args[0], args[1], args[2]),
            SIGPROCMASK => self.sigprocmask(args[0], args[1], args[2], args[3]),
            SIGRETURN => self.sigreturn(),
            WRITEV => self.writev(args[0], args[1], args[2]),
            ACCESS => self.access(args[0], args[1]),
            DUP => self.dup(args[0]),
            CLONE => self.clone(args[0], args[1], args[2], args[3], args[4]),
            FORK => self.fork(),
            EXECVE => self.execve(args[0], args[1], args[2]),
            EXIT => self.exit(args[0]),
            WAIT4 => self.wait4(args[0], args[1], args[2], args[3]),
            KILL => self.kill(args[0], args[1]),
            UNAME => self.uname(args[0]),
            FCNTL => self.fcntl(args[0], args[1], args[2]),
            GETCWD => self.getcwd(args[0], args[1]),
            SIGALTSTACK => self.sigaltstack(args[0], args[1]),
            MKNOD => self.mknod(args[0], args[1], args[2]),
            ARCH_PRCTL => self.arch_prctl(args[0], args[1]),
            IOPL => self.iopl(args[0]),
            GETTID => self.gettid(),
            TKILL => self.tkill(args[0], args[1]),
            SET_TID_ADDR => self.set_tid_addr(args[0]),
            FUTEX => self.futex(args[0], args[1], args[2], args[3]),
            EXIT_GROUP => self.exit_group(args[0]),
            OPENAT => self.openat(args[0], args[1], args[2], args[3]),
            PIPE2 => self.pipe2(args[0], args[1]),
            _ => {
                println!("Unknown syscall: {}", num);
                Err(Error::ENOSYS)
            }
        };

        match result {
            Ok(code) => code as isize,
            Err(code) => -(code as isize),
        }
    }
}

/// Get a string from an address
///
/// Safety:
/// *  The caller must give a right address
pub unsafe fn get_string(addr: usize) -> Option<&'static str> {
    // Get the count of bytes by iterating the memory and by breaking when '\0' is found
    let pointer = addr as *const u8;
    let mut count = 0;

    // Send a timeout at 10_000_000
    for _ in 0..10_000_000 {
        let data = *pointer.offset(count);
        if data != 0 {
            count += 1;
        } else {
            break;
        }
    }

    if count != 0 {
        // We need to store the string in the heap because otherwise, it is stored in the
        // userspace's address space
        Some(Box::leak(String::from(core::str::from_utf8(from_raw_parts(pointer, count as usize)).ok()?).into_boxed_str()))
    } else {
        None
    }
}

/// Get an array of strings from an address
///
/// Safety:
/// *  The caller must give a right address
pub unsafe fn get_array_of_strings(addr: usize) -> Option<Vec<String>> {
    // The array is composed of pointers to a string
    // So, we need to check that the pointer is not null (else, it is the end of the array), to get
    // the address of the string and to read the string until '\0'
    let mut strings = vec![];
    let mut addr = addr as *const usize;

    // Send a timeout at 10_000_000
    for _ in 0..10_000_000 {
        let string_addr = *addr;
        if string_addr != 0 {
            if let Some(string) = get_string(string_addr) {
                strings.push(string.to_string());
                addr = addr.add(1);
            }
        } else {
            break;
        }
    }

    if strings.len() != 0 {
        Some(strings)
    } else {
        None
    }
}
