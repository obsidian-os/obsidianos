/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Syscalls used for tasking (exit, ...)
use alloc::{
    prelude::v1::ToString,
    alloc::{GlobalAlloc, Layout},
};
use x86_64::registers::model_specific::FsBase;
use goblin::elf::Elf;
use bitflags::bitflags;

use super::{
    error::{Error, Result},
    get_string, get_array_of_strings, Syscall,
};
use crate::{
    memory::heap::HEAP_ALLOCATOR,
    elf::loader::ElfLoader,
    fs::ROOT,
    tasking::{
        process::Pid,
        scheduler::{exit, switch_to, PROCESSES, SCHEDULER},
        thread::{Thread, FX_SIZE},
        memory::cow::Cow,
        signal::{
            Signal,
            SignalStack,
            send_signal_to,
            action::{
                SigAction,
                SigActionFlags,
            },
            mask::{
                SigMask,
                SIG_BLOCK,
                SIG_UNBLOCK,
                SIG_SETMASK,
            },
            info::{
                SI_TKILL,
                SigInfo,
            },
        },
        context::Fx,
    },
};

bitflags! {
    /// The flags used during the `clone` syscall describing things to clone
    pub struct CloneFlags: usize {
        /// Clone the virtual memory
        const VM =              0x00000100;

        /// Clone the filesystem
        const FS =              0x00000200;

        /// Clone the files
        const FILES =           0x00000400;

        /// Reset signal dispositions
        const SIGHAND =         0x00000800;

        /// Trace also the child
        const PTRACE =          0x00002000;

        #[allow(missing_docs)]
        const VFORK =           0x00004000;

        #[allow(missing_docs)]
        const PARENT =          0x00008000;

        #[allow(missing_docs)]
        const THREAD =          0x00010000;

        #[allow(missing_docs)]
        const NEWNS	 =          0x00020000;

        #[allow(missing_docs)]
        const SYSVSEM =         0x00040000;

        /// Set the tls
        const SETTLS =          0x00080000;

        #[allow(missing_docs)]
        const PARENT_SETTID =   0x00100000;

        #[allow(missing_docs)]
        const CHILD_CLEARTID =  0x00200000;

        #[allow(missing_docs)]
        const DETACHED =        0x00400000;

        #[allow(missing_docs)]
        const UNTRACED =        0x00800000;

        #[allow(missing_docs)]
        const CHILD_SETTID =    0x01000000;

        #[allow(missing_docs)]
        const NEWCGROUP =       0x02000000;

        #[allow(missing_docs)]
        const NEWUTS =          0x04000000;

        #[allow(missing_docs)]
        const NEWIPC =          0x08000000;

        #[allow(missing_docs)]
        const NEWUSER =         0x10000000;

        #[allow(missing_docs)]
        const NEWPID =          0x20000000;

        #[allow(missing_docs)]
        const NEWNET =          0x40000000;

        #[allow(missing_docs)]
        const IO =              0x80000000;
    }
}

impl Syscall {
    /// Exit from the current thread by switching to the next thread
    pub fn exit(&self, code: usize) -> Result {
        match code {
            0 => println!("Exit success"),
            _ => println!("Error"),
        }

        exit(false);
        unreachable!();
    }

    /// Exit from the current thread by switching to the next thread
    pub fn exit_group(&self, code: usize) -> Result {
        match code {
            0 => println!("EXIT_SUCCESS"),
            _ => println!("EXIT_ERROR"),
        }

        exit(true);
        unreachable!();
    }

    /// Send a signal to a thread
    ///
    /// FIXME: For now, the signal is sent to the process. However Linux do the same
    /// (in linux/kernel/signal.c:3839, the PID (**Process** ID) is used)
    pub fn tkill(&self, tid: usize, sig: usize) -> Result {
        self.kill(tid, sig)
    }

    /// Send a signal to a process
    pub fn kill(&self, pid: usize, sig: usize) -> Result {
        let sig = Signal::from(sig);
        println!("kill({:#x}, {:?})", pid, sig);

        match send_signal_to(
            Pid(pid),
            sig,
            SigInfo {
                si_signo: sig as usize,
                si_code: SI_TKILL,
                si_errno: 0, // FIXME: ???
                si_addr: 0, // FIXME: What to do?
            })
        {
            Ok(_) => {
                Ok(0)
            },
            Err(_) => Err(Error::EINVAL),
        }
    }

    /// Get the TID of the current thread
    pub fn gettid(&self) -> Result {
        print!("gettid() = ");

        // Use the kernel's page table
        crate::memory::switch_to_kernel_page_table();

        let tid = SCHEDULER.get().current().unwrap().tid.0;
        println!("{}", tid);
        Ok(tid)
    }

    /// Set the function which will be executed when the signal `num` will be handled
    pub fn sigaction(&self, num: usize, action: usize, oldaction: usize) -> Result {
        let sig = Signal::from(num);
        let mut processes = PROCESSES.write();
        let thread = SCHEDULER.get().current().unwrap();
        let process = processes.get_mut(&thread.process).unwrap();

        let action = if action > 0 {
            let mut val = unsafe { &mut *(action as *mut SigAction) };

            // Dereference the `mask` pointer
            val.sa_mask = unsafe { *(&val.sa_mask as *const SigMask) };

            Some(val)
        } else {
            None
        };
        let oldaction = if oldaction > 0 {
            let mut val = unsafe { &mut *(oldaction as *mut SigAction) };

            // Dereference the `mask` pointer
            val.sa_mask = unsafe { *(&val.sa_mask as *const SigMask) };

            Some(val)
        } else {
            None
        };

        println!("sigaction({}, {:?}, {:?})", num, action, oldaction);

        if oldaction.is_some() {
            // Get action
            match process.actions.get(&sig) {
                Some(sigact) => *oldaction.unwrap() = *sigact,
                None => return Err(Error::EINVAL),
            }
        }

        if action.is_some() {
            // Set action
            if sig != Signal::SIGKILL && sig != Signal::SIGSTOP {
                match process.actions.get_mut(&sig) {
                    Some(sigact) => *sigact = *action.unwrap(),
                    None => return Err(Error::EINVAL),
                }
            } else {
                // SIGKILL and SIGSTOP handlers cannot be set
                return Err(Error::EINVAL);
            }
        }

        Ok(0)
    }

    /// Change the mask of blocked signals
    pub fn sigprocmask(&self, how: usize, set: usize, oldset: usize, size: usize) -> Result {
        let set = if set > 0 {
            Some(unsafe { *(set as *const SigMask) })
        } else {
            None
        };

        let oldset = if oldset > 0 {
            Some(unsafe { &mut *(oldset as *mut SigMask) })
        } else {
            None
        };

        let mut processes = PROCESSES.write();
        let thread = SCHEDULER.get().current().unwrap();
        let process = processes.get_mut(&thread.process).unwrap();

        println!(
            "sigprocmask({}, {}{:?}, {:?}, {}) = 0",
            match how {
                SIG_BLOCK => "SIG_BLOCK",
                SIG_UNBLOCK => "SIG_UNBLOCK",
                SIG_SETMASK => "SIG_SETMASK",
                _ => "",
            },
            match how {
                SIG_BLOCK => "~ ",
                SIG_UNBLOCK => "&! ",
                SIG_SETMASK => "= ",
                _ => "",
            },
            match how {
                // Display the opposite for SIG_BLOCK
                SIG_BLOCK => if let Some(set) = set { Some(!set) } else { None },
                _ => set,
            }, oldset, size
        );

        if oldset.is_some() {
            *oldset.unwrap() = process.signal_mask;
        }

        if let Some(set) = set {
            // Make sure that neither SIGKILL nor SIGSTOP are present
            let mut set = set.clone();
            set.remove(SigMask::SIGKILL);
            set.remove(SigMask::SIGSTOP);

            match how {
                SIG_BLOCK => process.signal_mask.insert(set),
                SIG_UNBLOCK => process.signal_mask.remove(set),
                SIG_SETMASK => process.signal_mask = set,
                _ => return Err(Error::EINVAL),
            }
        }

        Ok(0)
    }

    /// Restore the context before the signal handler
    pub fn sigreturn(&mut self) -> Result {
        let mut processes = PROCESSES.write();
        let current = SCHEDULER.get().current().unwrap().process;
        let process = processes.get_mut(&current).unwrap();

        assert!(process.signal_saved.is_some());
        let saved = process.signal_saved.take().unwrap();

        println!("sigreturn() = 0");

        // Unblock the current signal if SA_NODEFER is not set
        if !saved.2.sa_flags.contains(SigActionFlags::SA_NODEFER) {
            process.signal_mask.remove(SigMask::from(saved.1));
        }

        // Restore the context
        *self.stack = saved.0;

        // Restore the SSE state
        Fx::restore(saved.3);

        // Deallocate the SSE area
        unsafe {
            HEAP_ALLOCATOR.dealloc(saved.3 as *mut u8, Layout::from_size_align_unchecked(FX_SIZE, 16));
        }

        Ok(saved.0.rax)
    }

    /// Wait for a process to change state
    pub fn wait4(&self, pid: usize, status: usize, opts: usize, usage: usize) -> Result {
        /* TODO: Wait4 */
        println!("wait4({}, {}, {:#x}, {}) = 0", pid, status, opts, usage);
        Ok(0)
    }

    /// Set the stack used during a signal handler
    pub fn sigaltstack(&self, ss: usize, oldss: usize) -> Result {
        let mut processes = PROCESSES.write();
        let current = SCHEDULER.get().current().unwrap().process;
        let process = processes.get_mut(&current).unwrap();

        let ss = if ss > 0 {
            Some(unsafe { *(ss as *const SignalStack) })
        } else {
            None
        };

        let oldss = if oldss > 0 {
            Some(unsafe { &mut *(oldss as *mut SignalStack) })
        } else {
            None
        };

        println!("sigaltstack({:?}, {:?})", ss, oldss);

        if oldss.is_some() {
            *oldss.unwrap() = process.signal_stack.unwrap();
        }

        if ss.is_some() {
            process.signal_stack = ss;
        }
        Ok(0)
    }

    /// Wait for a process to change state
    pub fn set_tid_addr(&self, tidptr: usize) -> Result {
        /* TODO: Set tid addr */
        println!("set_tid_addr({:#x}) = ?", tidptr);

        Ok(SCHEDULER.get().current().unwrap().tid.0)
    }

    /// Get the working directory
    pub fn getcwd(&self, buf: usize, size: usize) -> Result {
        println!("getcwd({:#x}, {}) = {:#x}", buf, size, buf);
        let processes = PROCESSES.read();
        let current = SCHEDULER.get().current().unwrap().process;
        let cwd = &processes.get(&current).unwrap().cwd;

        let ptr = buf as *mut u8;
        let addr = cwd.as_ptr();
        let len = cwd.len();

        unsafe {
            ptr.copy_from(addr, len);
            ptr.add(len).write(0);
        }
        Ok(buf)
    }

    /// Create a child process
    pub fn clone(&self, flags: usize, stack_pointer: usize, parent_tid: usize, child_tid: usize, newtls: usize) -> Result {
        // TODO: Clone with args

        let flags = CloneFlags::from_bits_truncate(flags);
        print!("clone({:#x}, {:#?}, {:#x}, {:#x}, {:#x}) = ", stack_pointer, flags, parent_tid, child_tid, newtls);

        // Use the kernel's page table
        crate::memory::switch_to_kernel_page_table();

        // Get the PID of the new process
        let pid = usize::from(Pid::get_new());

        // List of processes
        let mut processes = PROCESSES.write();

        // Get the current process
        let parent_process = SCHEDULER.get().current().unwrap().process;

        // Clone the current process (thank you Rust's traits for this awesome implementation ;-))
        let mut child_process = processes.get(&parent_process).unwrap().clone();

        // Set the PID of the new process
        child_process.pid = Pid(pid);

        // Set the parent of this process
        child_process.parent = Some(parent_process);

        // Remove all threads in the new process
        child_process.threads.clear();

        // Set the memory as Copy-on-write
        let (parent_memory, child_memory) =
            Cow::make_for(processes.get(&parent_process).unwrap().memory.clone());
        processes.get_mut(&parent_process).unwrap().memory = parent_memory;
        child_process.memory = child_memory;

        // Choose a good TLS
        let newtls = if flags.contains(CloneFlags::SETTLS) { newtls as u64 } else { FsBase::read().as_u64() };

        // Create a new thread for the new process
        let thread = Thread::clone_(child_process.pid, self.stack.clone(), newtls, stack_pointer);
        child_process.threads.push(thread.tid);

        // Add the new process in the list of children
        processes
            .get_mut(&parent_process)
            .unwrap()
            .children
            .push(child_process.pid);

        // Push the process in the list of processes
        processes.insert(child_process.pid, child_process.clone());

        // Push the thread for the process
        SCHEDULER.get().add(thread);

        // Drop Vecs because after the switch of page tables, we cannot use the kernel's heap
        drop(parent_process);
        drop(child_process);

        println!("{}", pid);

        // Use the process' page table
        processes
            .get(&parent_process)
            .unwrap()
            .memory
            .switch_page_table();

        Ok(pid as usize)
    }

    /// Create a new child process from the current
    pub fn fork(&self) -> Result {
        print!("fork() = ");

        // Use the kernel's page table
        crate::memory::switch_to_kernel_page_table();

        // Get the PID of the new process
        let pid = usize::from(Pid::get_new());

        // List of processes
        let mut processes = PROCESSES.write();

        // Get the current process
        let parent_process = SCHEDULER.get().current().unwrap().process;

        // Clone the current process (thank you Rust's traits for this awesome implementation ;-))
        let mut child_process = processes.get(&parent_process).unwrap().clone();

        // Set the PID of the new process
        child_process.pid = Pid(pid);

        // Set the parent of this process
        child_process.parent = Some(parent_process);

        // Remove all threads in the new process
        child_process.threads.clear();

        // Set the memory as Copy-on-write
        let (parent_memory, child_memory) =
            Cow::make_for(processes.get(&parent_process).unwrap().memory.clone());
        processes.get_mut(&parent_process).unwrap().memory = parent_memory;
        child_process.memory = child_memory;

        // Create a new thread for the new process
        let thread = Thread::fork(child_process.pid, self.stack.clone(), FsBase::read().as_u64());
        child_process.threads.push(thread.tid);

        // Add the new process in the list of children
        processes
            .get_mut(&parent_process)
            .unwrap()
            .children
            .push(child_process.pid);

        // Push the process in the list of processes
        processes.insert(child_process.pid, child_process.clone());

        // Push the thread for the process
        SCHEDULER.get().add(thread);

        // Drop Vecs because after the switch of page tables, we cannot use the kernel's heap
        drop(parent_process);
        drop(child_process);

        println!("{}", pid);

        // Use the process' page table
        processes
            .get(&parent_process)
            .unwrap()
            .memory
            .switch_page_table();

        Ok(pid as usize)
    }

    /// Replace the current process with a new process from a path
    pub fn execve(&mut self, path: usize, args: usize, envs: usize) -> Result {
        let path = unsafe { get_string(path).unwrap() };
        let args = unsafe { get_array_of_strings(args).unwrap() };
        let envs = unsafe { get_array_of_strings(envs).unwrap() };
        print!("execve(\"{}\", {:?}, {:?}) = ", path, args, envs);

        if let Ok(inode) = ROOT
            .read()
            .open(path) {
            let mut inode = inode.write();
            let size = inode.metadata()?.size;
            let data = inode.read(0, size)?;

            // Load the binary
            match Elf::parse(data.as_slice()) {
                Ok(binary) => {
                    let mut loader = ElfLoader::new(binary, data.as_slice(), path);
                    if let Err(e) = loader.check() {
                        println!(
                            "EINVAL\nError during the check of the ELF binary: {:?}. The binary is not loaded.",
                            e
                        );
                        Err(Error::EINVAL)
                    } else {
                        // TODO: CLOEXEC

                        crate::memory::switch_to_kernel_page_table();
                        if let Err(e) = loader.load(args.clone(), envs) {
                            // TODO: Return a better value than EINVAL
                            println!(
                                "EINVAL\nError during the load of the ELF binary: {:?}. The binary is not loaded.",
                                e
                            );
                            return Err(Error::EINVAL);
                        }

                        let mut processes = PROCESSES.write();
                        let current = SCHEDULER.get().current().unwrap().process;

                        // Kill all threads
                        for thread in processes.get(&current).unwrap().threads.clone() {
                            SCHEDULER.get().remove(thread);
                        }

                        // Replace the process
                        let mut new_process = loader.get_process();
                        new_process.process.pid = current;
                        new_process.thread.process = current;
                        new_process.process.cwd = "/".to_string();
                        new_process.process.name = args[0].clone();

                        *processes.get_mut(&current).unwrap() = new_process.process.clone();

                        // Insert the new thread
                        SCHEDULER.get().add(new_process.thread.clone());

                        // Add the new thread in the process' threads list
                        processes
                            .get_mut(&new_process.process.pid)
                            .unwrap()
                            .threads
                            .push(new_process.thread.tid);

                        // Drop the lock
                        drop(processes);

                        println!("0");

                        // Switch to the new process
                        switch_to(new_process.thread.tid);
                        unreachable!();
                    }
                }
                Err(e) => {
                    println!("EINVAL\nELF Error: {}", e);
                    Err(Error::EINVAL)
                }
            }
        } else {
            println!("ENOENT");
            Err(Error::ENOENT)
        }
    }

    /// Change the I/O privilege level of the current process
    pub fn iopl(&mut self, level: usize) -> Result {
        // TODO: Check for UID=0
        if level > 3 {
            return Err(Error::EINVAL);
        }

        // Change the IOPL flag in the [RFLAGS register](https://en.wikipedia.org/wiki/FLAGS_register)
        self.stack.rflags = (self.stack.rflags & !(3 << 12)) | ((level & 3) << 12);

        Ok(0)
    }
}
