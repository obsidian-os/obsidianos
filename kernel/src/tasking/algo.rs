/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! A trait which make the addition of other scheduler algorithms easier
use alloc::{
    prelude::v1::Box,
    alloc::{GlobalAlloc, Layout},
};
use core::mem::{transmute, size_of};
use x86_64::{registers::model_specific::FsBase, VirtAddr};

use crate::memory::heap::HEAP_ALLOCATOR;
use super::{
    thread::{Thread, Tid, FX_SIZE},
    context::Fx,
    scheduler::PROCESSES,
    signal::{
        action::{
            DEFAULT_ACTIONS,
            SIGDFL, SIGIGN,
            SigActionFlags
        },
        info::SigInfo,
        mask::SigMask,
    },
    info::StackWriter,
};

/// A trait which defines the common functions used in a scheduler algorithm
pub trait SchedulerAlgo {
    /// Add a new thread in the list of threads
    fn add(&mut self, thread: Thread);

    /// Remove the thread with tid `tid` in the list of threads
    fn remove(&mut self, tid: Tid);

    /// Choose the next thread which will be executed
    fn choose(&mut self) -> Option<Thread>;

    /// Indicates if a thread ID is used or not
    fn contains(&self, tid: &Tid) -> bool;

    /// Get the current thread
    fn current(&self) -> Option<&Thread>;

    /// Get the current mutable thread
    fn current_mut(&mut self) -> Option<&mut Thread>;

    /// Set the current process
    fn set_current(&mut self, current: Tid);

    /// Get the thread with the TID `tid`
    fn get(&self, tid: Tid) -> Option<&Thread>;

    /// Get the mutable thread with the TID `tid`
    fn get_mut(&mut self, tid: Tid) -> Option<&mut Thread>;

    /// Idle function when there is no process
    // FIXME: Extract this function to prevent stack mangling due to the SCHEDULER's lock being
    // held
    fn idle(&self) {
        loop {}
    }
}
