/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! The context of a process (its registers, its stack, the SSE state, ...).
//! See [OSDEV](https://wiki.osdev.org/Context_Switching)
#[allow(dead_code)]
#[repr(packed)]
#[derive(Debug, Clone, Copy)]
/// A structure which contains all registers of the CPU
pub struct Registers {
    /// The CR3 register (which contains the page table)
    pub cr3: usize,

    /// The RBP register (base pointer)
    pub rbp: usize,

    /// The R15 register
    pub r15: usize,

    /// The R14 register
    pub r14: usize,

    /// The R13 register
    pub r13: usize,

    /// The R12 register
    pub r12: usize,

    /// The R11 register
    pub r11: usize,

    /// The R10 register
    pub r10: usize,

    /// The R9 register
    pub r9: usize,

    /// The R8 register
    pub r8: usize,

    /// The RSI register
    pub rsi: usize,

    /// The RDI register
    pub rdi: usize,

    /// The RDX register
    pub rdx: usize,

    /// The RCX register
    pub rcx: usize,

    /// The RBX register
    pub rbx: usize,

    /// The RAX register
    pub rax: usize,

    /// The RIP register (instruction pointer)
    pub rip: usize,

    /// The CS register (code segment)
    pub cs: usize,

    /// The RFLAGS register (which contains [various flags](https://en.wikipedia.org/wiki/FLAGS_register))
    pub rflags: usize,

    /// The RSP register (the stack pointer)
    pub rsp: usize,

    /// The DS register (the data segment)
    pub ds: usize,
}

impl core::default::Default for Registers {
    fn default() -> Self {
        Self {
            cr3: 0x0,
            rbp: 0x0,
            r15: 0x0,
            r14: 0x0,
            r13: 0x0,
            r12: 0x0,
            r11: 0x0,
            r10: 0x0,
            r9: 0x0,
            r8: 0x0,
            rsi: 0x0,
            rdi: 0x0,
            rdx: 0x0,
            rcx: 0x0,
            rbx: 0x0,
            rax: 0x0,
            rip: 0x0,
            cs: 0x0,
            rflags: 0x0,
            rsp: 0x0,
            ds: 0x0,
        }
    }
}

/// The FX register which saves the SSE state
pub struct Fx;

impl Fx {
    /// Init the SSE state
    pub fn init() {
        unsafe { llvm_asm!("fninit" : : : : "intel", "volatile"); }
    }

    /// Save the SSE state
    pub fn save(addr: u64) {
        unsafe { llvm_asm!("fxsave [$0]" : : "r"(addr) : : "intel", "volatile"); }
    }

    /// Restore the SSE state
    pub fn restore(addr: u64) {
        unsafe { llvm_asm!("fxrstor [$0]" : : "r"(addr) : : "intel", "volatile"); }
    }
}
