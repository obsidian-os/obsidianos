/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Management of the informations about a process
use alloc::{
    collections::btree_map::BTreeMap,
    prelude::v1::{String, Vec},
};
use core::ptr::null;

/// An item in the [auxiliary vector](https://www.gnu.org/software/libc/manual/html_node/Auxiliary-Vector.html)
#[derive(PartialEq, Debug, Clone)]
pub enum AuxVecItem {
    /// Pointer to the program headers of the program
    PHDR = 3,

    /// Size of a program header's entry
    PHENT = 4,

    /// Number of program headers
    PHNUM = 5,

    /// Size of a page
    PAGESZ = 6,

    /// Entry point of the program
    ENTRY = 9,
}

/// The informations about a process (arguments, environment, auxiliary vector, ...)
#[derive(PartialEq, Debug, Clone)]
pub struct ProcessInfo {
    /// The arguments of the program
    pub args: Vec<String>,

    /// The environment variables of the program
    pub envs: Vec<String>,

    /// The auxiliary vector of the program
    pub auxv: BTreeMap<u8, usize>,

    /// The address of the first argument
    pub first_arg_addr: u64,
}

impl ProcessInfo {
    /// Write the infos at the address `stack_top`
    pub fn push_at(&mut self, stack_top: usize) -> usize {
        let mut writer = StackWriter { sp: stack_top };

        // Program name
        writer.push_str(&self.args[0]);

        // Environment variables
        let envs: Vec<_> = self
            .envs
            .iter()
            .map(|arg| {
                writer.push_str(arg.as_str());
                writer.sp
            })
            .collect();

        // Arguments
        let argv: Vec<_> = self
            .args
            .iter()
            .map(|arg| {
                writer.push_str(arg.as_str());
                writer.sp
            })
            .collect();

        // Set the first argument address
        self.first_arg_addr = argv[0] as u64;

        // Auxiliary vector
        writer.push_slice(&[null::<u8>(), null::<u8>()]);
        for (&type_, &value) in self.auxv.iter() {
            writer.push_slice(&[type_ as usize, value]);
        }

        // Environment pointers
        writer.push_slice(&[null::<u8>()]);
        writer.push_slice(envs.as_slice());

        // Arguments pointers
        writer.push_slice(&[null::<u8>()]);
        writer.push_slice(argv.as_slice());
        writer.push_slice(&[argv.len()]);
        writer.sp
    }
}

/// Helper struct to write on the stack
pub struct StackWriter {
    /// The stack pointer
    pub sp: usize,
}

impl StackWriter {
    /// Write a slice on the stack
    pub fn push_slice<T: Copy>(&mut self, vs: &[T]) {
        use core::{
            mem::{align_of, size_of},
            slice,
        };
        self.sp -= vs.len() * size_of::<T>();
        self.sp -= self.sp % align_of::<T>();
        unsafe { slice::from_raw_parts_mut(self.sp as *mut T, vs.len()) }.copy_from_slice(vs);
    }

    /// Write a string on the stack
    pub fn push_str(&mut self, s: &str) {
        self.push_slice(&[b'\0']);
        self.push_slice(s.as_bytes());
    }
}
