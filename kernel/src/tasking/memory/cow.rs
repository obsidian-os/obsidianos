/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! The copy-on-write (COW) process let us speed up the system by copying the data of the page only
//! when it is written
use core::ptr::copy_nonoverlapping;
use x86_64::{
    VirtAddr, PhysAddr,
    structures::paging::{
        page_table::{PageTableFlags as Flags, PageTableEntry},
        Page, FrameAllocator, Size4KiB, PhysFrame,
    },
};

use crate::tasking::process::Process;
use crate::memory::{
    TMP_PAGE_ADDR_2,
    PAGE_SIZE,
    table::ProcessPageTable,
};
use crate::common::align_down;
use super::ProcessMemory;

/// The bit indicating that a page is a Copy-on-write (COW) page
pub const COW_BIT: Flags = Flags::BIT_9;

/// The copy-on-write (COW) process let us speed up the system by copying the data of the page only
/// when it is written
pub struct Cow;

impl Cow {
    /// Set all pages of a `ProcessMemory` structure as Copy-on-write and create a new `ProcessMemory`
    /// structure having all pages set as Copy-on-write
    ///
    /// Return two `ProcessMemory`s: one for the parent and the other for the child
    pub fn make_for(old: ProcessMemory) -> (ProcessMemory, ProcessMemory) {
        // Create a new page table for the new process
        let mut new = ProcessMemory::new();
        new.areas = old.areas.clone();

        for area in old.areas.iter() {
            // Change the flags of the pages of the area
            for page in Page::<Size4KiB>::range(
                Page::containing_address(VirtAddr::new(area.start)),
                Page::containing_address(VirtAddr::new(area.end)),
            ) {
                Self::set_page(page, &old.table, &new.table);
            }
        }

        (old, new)
    }

    /// Set a page as copy-on-write (COW)
    fn set_page(page: Page, pt: &ProcessPageTable, pt_new: &ProcessPageTable) {
        // Get the entry of the page in the page table
        let addr = page.start_address().as_u64();

        let mut entry = pt.get_entry(addr).unwrap();

        // If the entry is not PRESENT, we have nothing to do because no frame was allocated
        if let Ok(frame) = entry.frame() {
            // --- Old page table ---
            // Remove the WRITABLE bit and add the COW_BIT
            let mut flags = entry.flags();
            flags.remove(Flags::WRITABLE);
            flags.insert(COW_BIT);
            entry.set_flags(flags);

            // Set the entry of the page in the page table
            pt.set_entry(addr, entry).unwrap();

            // --- New page table ---
            pt_new.map(page, frame, flags).unwrap();
        } else {
            // --- New page table ---
            pt_new.map(page, PhysFrame::containing_address(PhysAddr::new(0)), entry.flags()).unwrap();
        }
    }

    /// Handle a page fault by copying the data on in a new page
    ///
    /// Step 0 (original):
    /// Original page -> Frame
    ///
    /// Step 1 (create new frame):
    /// Original page -> Frame
    /// New temporary page -> New frame (empty)
    ///
    /// Step 2 (copy data):
    /// Original page -> Frame
    /// New temporary page -> New frame
    ///
    /// Step 3 (unmap original page):
    /// New temporary page -> New frame
    ///
    /// Step 4 (use the original page):
    /// Original page -> New frame
    pub fn handle_page_fault(addr: u64, mut entry: PageTableEntry, current_process: &mut Process) {
        // --- Step 1 ---
        // Allocate a new frame which will be the clone of the frame
        let frame = crate::memory::FRAME_ALLOCATOR
            .get()
            .unwrap()
            .lock()
            .allocate_frame()
            .unwrap();

        // Create a temporary page to copy data between the two frames
        let tmp_page = Page::containing_address(VirtAddr::new(TMP_PAGE_ADDR_2));

        // Map the temporary page
        current_process.memory.table.map(
            tmp_page,
            frame,
            Flags::PRESENT | Flags::WRITABLE | Flags::NO_EXECUTE,
        ).unwrap();

        // --- Step 2 ---
        // Copy the data
        current_process.memory.with(|| {
            let src = align_down(addr, PAGE_SIZE) as *const u8;
            let dest = tmp_page.start_address().as_u64() as *mut u8;

            unsafe { copy_nonoverlapping(src, dest, PAGE_SIZE) };
        });

        // --- Step 3 ---
        // Unmap the temporary page
        current_process
            .memory
            .table
            .unmap(tmp_page).unwrap();

        // --- Step 4 ---
        // Map the new frame
        let mut flags = entry.flags();
        let area = &current_process.memory.areas[current_process.memory.get_area(addr, addr).unwrap()];

        // If the WRITABLE flag is not requested, don't add it
        if area.flags.contains(Flags::WRITABLE) {
            flags.insert(Flags::WRITABLE);
        }
        flags.remove(COW_BIT);
        entry.set_frame(frame, flags);

        current_process.memory.table.set_entry(addr, entry).unwrap();
    }
}
