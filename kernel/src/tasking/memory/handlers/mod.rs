/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Handlers defining what to do when a page fault happens in a specific area
use x86_64::structures::paging::{
    mapper::{MapToError, UnmapError},
    page_table::PageTableFlags as Flags,
    Size4KiB, Page,
};

use crate::memory::table::ProcessPageTable;

pub mod zero;
pub mod file;
pub mod now;

pub use zero::ZeroHandler;
pub use file::FileHandler;
pub use now::NowHandler;

/// Defines what happens during the mapping of an area and during a page fault
pub trait Handler: Send + Sync  {
    /// Map the area in the page table
    fn map(&self, page: Page, flags: Flags, pt: &ProcessPageTable) -> Result<(), MapToError<Size4KiB>>;

    /// Unmap the area in the page table
    fn unmap(&self, page: Page, pt: &ProcessPageTable) -> Result<(), UnmapError>;

    /// Handle a page fault
    ///
    /// Returns if the page fault is successfully handled or not
    fn handle_page_fault(&self, addr: u64, pt: &ProcessPageTable) -> bool;
}
