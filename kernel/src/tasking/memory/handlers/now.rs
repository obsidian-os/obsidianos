/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! A handler filling the area with zeros
use x86_64::structures::paging::{
    mapper::{MapToError, UnmapError},
    page_table::{PageTableFlags as Flags},
    Size4KiB, FrameAllocator, FrameDeallocator, Page,
};

use crate::common::memset;
use crate::memory::{
    FRAME_ALLOCATOR,
    PAGE_SIZE,
    table::ProcessPageTable,
};
use super::Handler;

/// A handler filling the area with zeros
pub struct NowHandler {}

impl Handler for NowHandler {
    fn map(&self, page: Page, mut flags: Flags, pt: &ProcessPageTable) -> Result<(), MapToError<Size4KiB>> {
        let original_flags = flags.clone();

        // We will need to write zeros, so force adding the WRITABLE flag
        flags.insert(Flags::WRITABLE);

        // Map the area to a dummy frame
        let frame = FRAME_ALLOCATOR
            .get()
            .unwrap()
            .lock()
            .allocate_frame()
            .expect("cannot allocate frame");
        pt.map(page, frame, flags)?;

        // Fill the area with zeros
        pt.with(|| {
            unsafe { memset(page.start_address().as_u64() as *mut u8, 0, PAGE_SIZE); }
        });

        // If the WRITABLE flag is not requested, remove it
        if !original_flags.contains(Flags::WRITABLE) {
            pt.get_entry(page.start_address().as_u64()).unwrap().set_flags(original_flags);
        }

        Ok(())
    }

    fn unmap(&self, page: Page, pt: &ProcessPageTable) -> Result<(), UnmapError> {
        // Force the PRESENT flag to be set
        let mut entry = pt.get_entry(page.start_address().as_u64()).unwrap();

        // If the entry is PRESENT, deallocate the frame
        if entry.flags().contains(Flags::PRESENT) {
            unsafe { FRAME_ALLOCATOR.get().unwrap().lock().deallocate_frame(entry.frame().unwrap()); }
        }

        // The frame must be PRESENT during a call to `unmap`
        entry.set_flags(entry.flags() | Flags::PRESENT);

        // Change the entry in the page table
        pt.set_entry(page.start_address().as_u64(), entry).unwrap();

        pt.unmap(page)
    }

    fn handle_page_fault(&self, _: u64, _: &ProcessPageTable) -> bool {
        false
    }
 }
