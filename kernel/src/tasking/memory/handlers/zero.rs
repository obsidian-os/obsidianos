/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! A handler filling the area with zeros
use x86_64::{
    PhysAddr,
    structures::paging::{
        mapper::{MapToError, UnmapError},
        page_table::{PageTableFlags as Flags},
        Size4KiB, FrameAllocator, FrameDeallocator, PhysFrame, Page,
    }
};

use crate::memory::{
    FRAME_ALLOCATOR,
    PAGE_SIZE,
    table::ProcessPageTable,
};
use crate::common::{memset, align_down};
use super::Handler;

/// A handler filling the area with zeros
pub struct ZeroHandler {}

impl Handler for ZeroHandler {
    fn map(&self, page: Page, mut flags: Flags, pt: &ProcessPageTable) -> Result<(), MapToError<Size4KiB>> {
        // Remove the PRESENT flag to trigger a Page Fault when read
        flags.remove(Flags::PRESENT);

        // Map the area to a dummy frame
        pt.map(page, PhysFrame::containing_address(PhysAddr::new(0)), flags)?;

        Ok(())
    }

    fn unmap(&self, page: Page, pt: &ProcessPageTable) -> Result<(), UnmapError> {
        // Force the PRESENT flag to be set
        let mut entry = pt.get_entry(page.start_address().as_u64()).unwrap();

        // If the entry is PRESENT, deallocate the frame
        if entry.flags().contains(Flags::PRESENT) {
            unsafe { FRAME_ALLOCATOR.get().unwrap().lock().deallocate_frame(entry.frame().unwrap()); }
        }

        // The frame must be PRESENT during a call to `unmap`
        entry.set_flags(entry.flags() | Flags::PRESENT);

        // Change the entry in the page table
        pt.set_entry(page.start_address().as_u64(), entry).unwrap();

        pt.unmap(page)
    }

    fn handle_page_fault(&self, addr: u64, pt: &ProcessPageTable) -> bool {
        let mut entry = pt.get_entry(addr).unwrap();
        let start = align_down(addr, PAGE_SIZE);
        let original_flags = entry.flags();

        // If the PRESENT flag is already set, this handler has nothing to do!
        if original_flags.contains(Flags::PRESENT) {
            return false;
        }

        // Map the page to a frame
        let frame = FRAME_ALLOCATOR
            .get()
            .unwrap()
            .lock()
            .allocate_frame()
            .expect("cannot allocate frame");
        entry.set_frame(frame, original_flags);

        // Add the PRESENT flag
        // We will need to write zeros, so force adding the WRITABLE flag
        let mut flags = entry.flags();
        flags.insert(Flags::PRESENT);
        flags.insert(Flags::WRITABLE);

        entry.set_flags(entry.flags() | Flags::PRESENT | Flags::WRITABLE);

        // Change the entry in the page table
        pt.set_entry(addr, entry.clone()).unwrap();

        // Zero the page
        pt.with(|| {
            unsafe { memset(start as *mut u8, 0, PAGE_SIZE); }
        });

        // If the WRITABLE flag is not requested, remove it
        if !original_flags.contains(Flags::WRITABLE) {
            let mut flags = entry.flags();
            flags.remove(Flags::WRITABLE);

            entry.set_flags(flags);

            // Change the entry in the page table
            pt.set_entry(addr, entry).unwrap();
        }

        true
    }
 }
