/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Memory used by a process management
use alloc::{
    prelude::v1::Vec,
    sync::Arc,
};
use core::{cmp::Ordering, fmt};
use x86_64::{
    structures::paging::{
        mapper::{Mapper, MapToError, UnmapError},
        page_table::PageTableFlags as Flags,
        FrameAllocator, Page, PhysFrame, Size4KiB,
    },
    registers::control::Cr3,
    PhysAddr, VirtAddr,
};

use crate::memory::{
    self,
    PAGE_SIZE,
    table::ProcessPageTable,
};
use crate::common::{align_down, align_up};
use super::memory::handlers::Handler;

pub mod handlers;
pub mod cow;

#[derive(PartialEq, Debug, Clone)]
/// A structure which represents the memory used by a process.
///
/// It keeps track of all areas used.
/// WARNING: Don't confuse this structure with the struct ProcessPageTable, it is a high level
/// structure which is based on it and which keeps track of mappings.
pub struct ProcessMemory {
    /// The frame which contains the PageTable
    pub table: ProcessPageTable,

    /// The areas used by this process
    pub areas: Vec<MemoryArea>,

    /// The start and end addresses of the executable image
    pub image: Option<(u64, u64)>,

    /// The start and end addresses of the data segment
    pub data_segment: Option<(u64, u64)>,

    /// The start and end addresses of the stack
    pub stack: Option<(u64, u64)>,

    /// The start and end addresses of the signal stack
    pub signal_stack: Option<(u64, u64)>,
}

impl ProcessMemory {
    /// Create a new ProcessMemory
    pub fn new() -> Self {
        let page_table_frame = memory::FRAME_ALLOCATOR
            .get()
            .unwrap()
            .lock()
            .allocate_frame()
            .unwrap();

        let table = ProcessPageTable::new(page_table_frame);

        table.mapper(|mapper| {
            // Map the kernel
            for (i, page) in Page::<Size4KiB>::range_inclusive(
                Page::containing_address(VirtAddr::new(memory::KERNEL_START_VIRT)),
                Page::containing_address(VirtAddr::new(
                    memory::KERNEL_START_VIRT + memory::KERNEL_SIZE,
                )),
            )
            .enumerate()
            {
                let frame = PhysFrame::containing_address(PhysAddr::new(
                    memory::KERNEL_START_PHYS + i as u64 * PAGE_SIZE as u64,
                ));
                unsafe {
                    mapper
                        .map_to(
                            page,
                            frame,
                            Flags::PRESENT | Flags::WRITABLE,
                            &mut *memory::FRAME_ALLOCATOR.get().unwrap().lock(),
                        )
                        .unwrap()
                        .ignore();
                }
            }

            // Map the kernel heap
            for (i, page) in Page::<Size4KiB>::range_inclusive(
                Page::containing_address(VirtAddr::new(memory::heap::HEAP_START_VIRT as u64)),
                Page::containing_address(VirtAddr::new(
                    memory::heap::HEAP_START_VIRT as u64 + memory::heap::HEAP_SIZE as u64,
                )),
            )
            .enumerate()
            {
                let frame = PhysFrame::containing_address(PhysAddr::new(
                    memory::heap::HEAP_START_PHYS + i as u64 * PAGE_SIZE as u64,
                ));
                unsafe {
                    mapper
                        .map_to(
                            page,
                            frame,
                            Flags::PRESENT | Flags::WRITABLE | Flags::NO_EXECUTE,
                            &mut *memory::FRAME_ALLOCATOR.get().unwrap().lock(),
                        )
                        .unwrap()
                        .ignore();
                }
            }
        });

        Self {
            table,
            areas: vec![],
            image: None,
            data_segment: None,
            stack: None,
            signal_stack: None,
        }
    }

    /// Find a free memory area
    pub fn find_free_area(&mut self, addr_hint: u64, len: usize) -> u64 {
        // This awesome algorithm is from the rCore OS (https://github.com/rcore-os/rCore/blob/43f6d9d69519f40cf152b29199e066429107fda5/crate/memory/src/memory_set/mod.rs#L183)
        core::iter::once(addr_hint)
            .chain(self.areas.iter().map(|area| align_up(area.end, PAGE_SIZE)))
            .find(|&addr| self.get_area(addr + 1, addr + len as u64).is_none())
            .expect("failed to find free area")
    }

    /// Get the area with `addr` inside
    ///
    /// Return the area and the index of the area in the list
    pub fn get_area(&self, start: u64, end: u64) -> Option<usize> {
        self.areas
            .iter()
            .enumerate()
            .find(|(_, area)| {
                if end < area.start {
                    return false;
                } else if start > area.end - 1 {
                    return false;
                } else {
                    return true;
                }
            })
            .and_then(|a| Some(a.0))
    }

    /// Switch to the process' page table
    pub fn switch_page_table(&self) {
        unsafe {
            self.table.switch();
        }
    }

    /// Execute the function `f` with the page table
    pub fn with<F: FnOnce()>(&self, f: F) {
        // Create a backup of the current page table
        let cr3 = Cr3::read();

        // Switch to this page table
        self.switch_page_table();

        // Execute the function
        f();

        // Restore the old table
        unsafe { Cr3::write(cr3.0, cr3.1); }
    }
}

#[derive(Clone)]
/// The representation of an area of a process' memory
///
/// It is used to keep track of memory allocations for each process
pub struct MemoryArea {
    /// The start of this area
    pub start: u64,

    /// The end of this area
    pub end: u64,

    /// The flags (rights) of this area
    pub flags: Flags,

    /// A handler defining what happens during the creation of the area and during a page fault on
    /// this area
    pub handler: Arc<dyn Handler>,
}

impl fmt::Debug for MemoryArea {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Area {{\n    start: {:#x}\n    end: {:#x}\n    flags: {:?}\n}}",
            self.start, self.end, self.flags
        )
    }
}

impl PartialEq for MemoryArea {
    fn eq(&self, other: &Self) -> bool {
        self.start == other.start
    }
}

impl Eq for MemoryArea {}

impl PartialOrd for MemoryArea {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> { self.start.partial_cmp(&other.start) }
}

impl Ord for MemoryArea {
    fn cmp(&self, other: &Self) -> Ordering { self.start.cmp(&other.start) }
}

impl MemoryArea {
    /// Create a new Area of memory which can be used with `ProcessMemory::map` for map a new area of
    /// memory
    pub fn new(start: u64, end: u64, mut flags: Flags, handler: Arc<dyn Handler>) -> Self {
        // Force adding the USER_ACCESSIBLE field
        flags.set(Flags::USER_ACCESSIBLE, true);

        Self {
            start,
            end,
            flags,
            handler,
        }
    }

    /// Get the size of the area
    pub fn size(&self) -> usize { (self.end - self.start) as usize }

    /// Map this area in the `mem` memory by calling the handler of the area
    pub fn map(&self, mem: &mut ProcessMemory) -> Result<(), MapToError<Size4KiB>> {
        let mut area = self.clone();

        for page in Page::<Size4KiB>::range_inclusive(
            Page::containing_address(VirtAddr::new(self.start)),
            Page::containing_address(VirtAddr::new(self.end - 1)),
        ) {
            self.handler.map(page, self.flags, &mem.table)?;
        }

        // Round the start and end addresses of the self by 4096 (the size of a page)
        area.start = align_down(area.start, PAGE_SIZE);
        area.end = align_up(area.end, PAGE_SIZE);

        // Add the self in the list of existing areas
        mem.areas.push(area);

        Ok(())
    }

    /// Unmap this area in the `mem` memory by calling the handler of the area
    pub fn unmap(&self, mem: &mut ProcessMemory) -> Result<(), UnmapError> {
        for page in Page::<Size4KiB>::range(
            Page::containing_address(VirtAddr::new(self.start)),
            Page::containing_address(VirtAddr::new(self.end)),
        ) {
            self.handler.unmap(page, &mem.table)?;
        }

        // Remove the area from the list of existing areas
        //
        // (Use Vec::retain because if the area is not in the list, no error will be emitted)
        mem.areas.retain(|a| {
            !(a.start == self.start && a.end == self.end)
        });

        Ok(())
    }
}
