/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Tasking and processes management functions which include:
//!   * A [Round Robin scheduler](https://en.wikipedia.org/wiki/Round-robin_scheduling) to choose
//!   the next process and to jump to it
//!   * A struct Context which contains all saved registers of a process
//!   * A struct ProcessMemory which handles all mappings of the process
//!   * A struct Arguments which copies arguments at the right place and which provides argc
//!   (argument count) and argv (arguments)
//!   * A struct ProcessInfo which contains the arguments, the environment variables and the
//!   auxiliary vector of the process
pub mod algo;
pub mod context;
pub mod info;
pub mod memory;
pub mod process;
pub mod scheduler;
pub mod thread;
pub mod signal;
