/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Definition of a Process which regroups a set of resources (memory, ...)
use alloc::{
    prelude::v1::{Vec, String},
    collections::btree_map::BTreeMap,
    sync::Arc,
};
use x86_64::{
    VirtAddr,
    structures::paging::{
        page_table::PageTableFlags as Flags,
        FrameDeallocator, Size4KiB, Page,
    },
};

use super::{
    info::ProcessInfo,
    memory::{cow::COW_BIT, MemoryArea, ProcessMemory, handlers::{ZeroHandler, NowHandler}},
    scheduler::{PROCESSES, SCHEDULER},
    thread::{Thread, Tid},
    signal::{
        SignalQueue,
        Signal,
        SignalStack,
        action::{
            SigAction,
            SigActions,
        },
        mask::SigMask,
    },
    context::Registers,
};
use crate::memory;
use crate::fs::{
    file::{Fd, FileHandle, OpenFlags},
    devfs::tty::TTY,
};

/// The size of the process' stack
pub const STACK_SIZE: usize = 65536;

/// The size of the process' data segment
pub const DATA_SEGMENT_SIZE: usize = 65536;

/// The size of the process' signal stack
pub const SIGNAL_STACK_SIZE: usize = 65536;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
/// A process ID
pub struct Pid(pub usize);

impl Pid {
    /// Get a new PID
    pub fn get_new() -> Self {
        let processes = PROCESSES.read();
        Self((0..)
            .find(|i| !processes.contains_key(&Pid(*i)))
            .unwrap())
    }
}

impl From<Pid> for usize {
    fn from(pid: Pid) -> Self {
        pid.0
    }
}

#[derive(PartialEq, Debug, Clone, Copy)]
/// The state of the process (killed or alive)
pub enum ProcessState {
    /// The process is alive
    Alive,

    /// The process is killed and will be replaced when there will be a new process
    Killed,
}

#[derive(Debug, Clone)]
/// A process which regroups resources and more
pub struct Process {
    // --- Hierarchy ---

    /// The process' name
    pub name: String,

    /// The process ID
    pub pid: Pid,

    /// Threads of the process
    pub threads: Vec<Tid>,

    /// Children of this process
    pub children: Vec<Pid>,

    /// Parent of this process (if there is one)
    pub parent: Option<Pid>,

    // --- Resources ---

    /// The state of this process
    pub state: ProcessState,

    /// The memory used by this process
    pub memory: ProcessMemory,

    /// Info about the process (arguments, environment, ...)
    pub info: ProcessInfo,

    /// The working directory
    pub cwd: String,

    /// The files used by this process
    pub files: BTreeMap<Fd, FileHandle>,

    // --- Signals ---

    /// The queue of signals
    pub signals: SignalQueue,

    /// The actions defining what to do when a signal is raised
    pub actions: SigActions,

    /// The stack used when a signal happens
    pub signal_stack: Option<SignalStack>,

    /// The saved things before the execution of the signal handler
    /// *  The context
    /// *  The signal's action
    /// *  The SSE state
    pub signal_saved: Option<(Registers, Signal, SigAction, u64)>,

    /// The signal mask for blocked signals
    pub signal_mask: SigMask,
}

impl Process {
    /// Create a new process
    pub fn new(info: ProcessInfo) -> Self {
        let mut process = Self {
            name: info.args[0].clone(),
            pid: Pid::get_new(),
            state: ProcessState::Alive,
            memory: ProcessMemory::new(),
            info: info.clone(),
            cwd: String::from("/"),
            files: BTreeMap::new(),
            threads: vec![],
            children: vec![],
            parent: None,
            signals: SignalQueue::new(),
            actions: SigActions::default(),
            signal_stack: None,
            signal_saved: None,
            signal_mask: SigMask::empty(),
        };

        // STDIN, read only
        process.add_file(FileHandle {
            offset: 0,
            inode: TTY.clone(),
            flags: OpenFlags::RDONLY,
        });

        // STDOUT, write only
        process.add_file(FileHandle {
            offset: 0,
            inode: TTY.clone(),
            flags: OpenFlags::WRONLY,
        });

        // STDERR, write only
        process.add_file(FileHandle {
            offset: 0,
            inode: TTY.clone(),
            flags: OpenFlags::WRONLY,
        });

        process
    }

    /// Create the user stack
    ///
    /// This function is called after the creation of the binary in memory, so the stack address
    /// can be flexible with the end of the binary
    pub fn create_stack(&mut self, addr: u64) -> (usize, u64) {
        let mut rsp = 0;

        MemoryArea::new(
            addr,
            addr + STACK_SIZE as u64,
            Flags::PRESENT | Flags::WRITABLE | Flags::NO_EXECUTE,
            Arc::new(NowHandler {}),
        )
            .map(&mut self.memory)
            .unwrap();

        let mut info = self.info.clone();
        self.memory.with(|| {
            rsp = info.push_at(addr as usize + STACK_SIZE);
        });
        self.info.first_arg_addr = info.first_arg_addr;

        (rsp, info.first_arg_addr)
    }

    /// Create the data segment
    ///
    /// This function is called after the creation of the binary in memory, so the data segment address
    /// can be flexible with the end of the binary
    pub fn create_data_segment(&mut self, addr: u64) {
        MemoryArea::new(
            addr,
            addr + DATA_SEGMENT_SIZE as u64,
            Flags::PRESENT | Flags::WRITABLE | Flags::NO_EXECUTE,
            Arc::new(ZeroHandler {}),
        ).map(&mut self.memory).unwrap();
    }

    /// Create the signal stack
    ///
    /// This function is called after the creation of the binary in memory, so the signal stack
    /// can be flexible with the end of the binary
    pub fn create_signal_stack(&mut self, addr: u64) {
        MemoryArea::new(
            addr,
            addr + SIGNAL_STACK_SIZE as u64,
            Flags::PRESENT | Flags::WRITABLE | Flags::NO_EXECUTE,
            Arc::new(ZeroHandler {}),
        ).map(&mut self.memory).unwrap();

        self.signal_stack = Some(SignalStack {
            ss_sp: addr + SIGNAL_STACK_SIZE as u64,
            ss_flags: 0,
            ss_size: SIGNAL_STACK_SIZE,
        });
    }

    /// Add a file handle in the list of files
    pub fn add_file(&mut self, fd: FileHandle) -> Fd {
        let id = (0..)
            .find(|i| !self.files.contains_key(i))
            .unwrap();

        self.files.insert(id, fd);
        id
    }

    /// Get a FileHandle from an ID
    pub fn get_file(&self, fd: Fd) -> Option<&FileHandle> {
        self.files.get(&fd)
    }

    /// Get a mutable FileHandle from an ID
    pub fn get_file_mut(&mut self, fd: Fd) -> Option<&mut FileHandle> {
        self.files.get_mut(&fd)
    }

    /// Exit handler called when the process exits
    pub fn exit(&self) {
        // Switch to the kernel's page table
        crate::memory::switch_to_kernel_page_table();

        // Deallocate all used frames
        for area in &self.memory.areas {
            // If the memory is used by the kernel, don't deallocate this area!
            if !area.flags.contains(Flags::USER_ACCESSIBLE) {
                continue;
            }

            // If the memory is shared with the parent, don't deallocate this area!
            if area.flags.contains(COW_BIT) && self.parent.is_some() {
                continue;
            }

            for page in Page::<Size4KiB>::range_inclusive(
                Page::containing_address(VirtAddr::new(area.start)),
                Page::containing_address(VirtAddr::new(area.end - 1)),
            ) {
                // We are using directly the FRAME_ALLOCATOR here because the process will exit and
                // we don't need to unmap the pages in the page table
                if let Ok(frame) = self.memory.table.get_entry(page.start_address().as_u64())
                    .unwrap()
                    .frame() {
                    unsafe {
                        memory::FRAME_ALLOCATOR
                            .get()
                            .unwrap()
                            .lock()
                            .deallocate_frame(frame);
                    }
                }
            }
        }
    }
}

#[derive(Debug, Clone)]
/// This builder helps us building a new process and a new thread
pub struct ProcessBuilder {
    /// The new process
    pub process: Process,

    /// The new thread
    pub thread: Thread,
}

impl ProcessBuilder {
    /// Create a new ProcessBuilder
    pub fn new(rip: usize, info: ProcessInfo) -> Self {
        let mut process = Process::new(info.clone());
        let cr3 = process.memory.table.frame().start_address().as_u64() as usize;
        let thread = Thread::new(process.pid, rip, cr3);
        process.threads.push(thread.tid);

        Self { thread, process }
    }

    /// Create the user stack in the underlying process
    pub fn create_stack(&mut self, addr: u64) {
        let (rsp, first_arg_addr) = self.process.create_stack(addr);
        let mut regs = self.thread.get_kstack();

        // Set the stack pointer
        regs.rsp = rsp;

        // Set the arguments
        regs.rdi = self.process.info.args.len();
        regs.rsi = first_arg_addr as usize;
    }

    /// Create the data segment
    pub fn create_data_segment(&mut self, addr: u64) {
        self.process.create_data_segment(addr);
    }

    /// Create the signal stack
    pub fn create_signal_stack(&mut self, addr: u64) {
        self.process.create_signal_stack(addr);
    }

    /// Get the memory of the process
    pub fn memory(&mut self) -> &mut ProcessMemory { &mut self.process.memory }

    /// Build the ProcessBuilder by adding the process and the thread to their lists
    pub fn build(&self) {
        let mut processes = PROCESSES.write();

        // Insert the new process
        processes.insert(self.process.pid, self.process.clone());

        // Insert the new thread
        SCHEDULER.get().add(self.thread.clone());
    }
}
