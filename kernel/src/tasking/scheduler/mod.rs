/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! This directory regroups some alogithms for scheduler
use alloc::{
    collections::btree_map::BTreeMap,
    alloc::{GlobalAlloc, Layout},
    prelude::v1::Box,
};
use core::{
    cell::UnsafeCell,
    sync::atomic::{AtomicBool, Ordering},
    mem::{transmute, size_of},
};
use lazy_static::lazy_static;
use spin::RwLock;
use x86_64::{registers::model_specific::FsBase, VirtAddr};

use crate::memory::heap::HEAP_ALLOCATOR;
use crate::irq::{handlers, id};
use super::{
    algo::SchedulerAlgo,
    process::{Pid, Process},
    scheduler::round_robin::RoundRobinScheduler,
    context::{Fx, Registers},
    thread::{Tid, FX_SIZE},
    signal::{
        action::{
            DEFAULT_ACTIONS,
            SIGDFL, SIGIGN,
            SigActionFlags
        },
        info::SigInfo,
        mask::SigMask,
    },
    info::StackWriter,
};

pub mod round_robin;

lazy_static! {
    /// A global scheduler
    pub static ref SCHEDULER: SchedulerCurrentAlgo = SchedulerCurrentAlgo(UnsafeCell::new(Box::new(RoundRobinScheduler::new())));
}

lazy_static! {
    /// A global array which regroups all processes and their PIDs
    pub static ref PROCESSES: RwLock<BTreeMap<Pid, Process>> = RwLock::new(BTreeMap::new());
}

/// Whether or not it is the first time that the processor is running
pub static FIRST_TIME: AtomicBool = AtomicBool::new(true);

/// The inner of the Processor
pub struct SchedulerCurrentAlgo(pub UnsafeCell<Box<dyn SchedulerAlgo>>);

impl SchedulerCurrentAlgo {
    /// Get a mutable reference of the scheduler
    pub fn get(&self) -> &mut Box<dyn SchedulerAlgo> { unsafe { &mut *self.0.get() } }
}

unsafe impl Sync for SchedulerCurrentAlgo {}
unsafe impl Send for SchedulerCurrentAlgo {}

/// If `all_threads` is false, remove the current thread and switch to the next thread
/// (does the job of the `exit` syscall)
///
/// Else, remove all threads of the process of the current thread and switch to the next thread
/// (does the job of the `exit_group` syscall)
pub fn exit(all_threads: bool) {
    let mut processes = PROCESSES.write();
    let pid = SCHEDULER
        .get()
        .current()
        .expect("No current process")
        .process;

    if all_threads {
        // Remove all threads of the process
        processes.get_mut(&pid).unwrap().threads.iter().for_each(|t| {
            if let Some(thread) = SCHEDULER.get().get(*t) {
                thread.exit();
                SCHEDULER.get().remove(*t);
            }
        });
        processes.get_mut(&pid).unwrap().threads.clear();
    } else {
        // Remove the current thread
        let tid = SCHEDULER.get().current().expect("No current process").tid;

        SCHEDULER.get().remove(tid);

        processes
            .get_mut(&pid)
            .unwrap()
            .threads
            .retain(|thread| thread != &tid);
    }

    // If there is no other threads, remove the process
    if processes.get(&pid).unwrap().threads.is_empty() {
        processes.get(&pid).unwrap().exit();
        processes.remove(&pid);
    }

    // Drop the lock because this function will never return
    drop(processes);

    // Switch to the next thread
    switch();
}

#[no_mangle]
#[inline(never)]
/// Switch to the next process
pub fn next(regs: Registers) {
    // Signals the end of the interrupt
    handlers::end(id::IrqId::TIMER);

    // If this is the first time, don't save registers because otherwise they will be saved in the
    // first process which is not already running!
    if !FIRST_TIME.load(Ordering::Relaxed) {
        match SCHEDULER.get().current_mut() {
            Some(thread) => {
                // Save the registers
                *thread.get_kstack() = regs;

                // Save fsbase
                thread.fsbase = FsBase::read().as_u64();

                // Save FX (we can save it now because the kernel does not mangle SSE registers)
                Fx::save(SCHEDULER.get().current().unwrap().fx);

                // Switch to the next process
                switch();
            },
            None => {
                // Switch to IDLE
                SCHEDULER.get().idle();
            },
        }
    } else {
        FIRST_TIME.store(false, Ordering::Relaxed);

        // Switch to the next process
        switch();
    }
}

pub fn switch() -> ! {
    // Choose the next thread
    let thread = SCHEDULER.get().choose();

    if let Some(mut thread) = thread {
        {
            let mut processes = PROCESSES.write();
            let process = processes.get_mut(&thread.process).unwrap();

            if !process.signals.is_empty() {
                // --- Handles signals ---
                let signal = process.signals.pop_front().unwrap();
                let action = process.actions.get(&signal.0).expect("cannot find the requested signal");

                println!("[ {} ] {:#x} {:?} {}", process.pid.0, signal.1.si_addr, signal.0, process.name);

                if action.sa_handler == SIGIGN {
                    // Ignore this signal, so nothing to do!
                } else if action.sa_handler == SIGDFL {
                    // If the action is a default action, do it in kernel mode, so here
                    let handler = DEFAULT_ACTIONS.get(&signal.0).unwrap().sa_handler;

                    // Block the current signal if SA_NODEFER is not set
                    if !action.sa_flags.contains(SigActionFlags::SA_NODEFER) {
                        process.signal_mask.insert(SigMask::from(signal.0));
                    }

                    drop(processes);
                    unsafe { transmute::<*const (), fn()>(handler as *const ())(); }

                    unreachable!();
                } else {
                    // Custom handler defined by the process

                    let kstack = thread.get_kstack();

                    // Save the context, the SSE state, the signal number and the action
                    let fx =
                        Box::into_raw(unsafe { Box::from_raw(HEAP_ALLOCATOR.alloc(Layout::from_size_align_unchecked(FX_SIZE, 16)) as *mut [u8; FX_SIZE]) }) as u64;
                    Fx::save(fx);

                    process.signal_saved = Some((*kstack, signal.0, *action, fx));

                    // Change the instruction pointer
                    kstack.rip = action.sa_handler;

                    // Stack management
                    if action.sa_flags.contains(SigActionFlags::SA_ONSTACK) {
                        // Use a specially defined stack
                        // TODO: Signal alternate stack with if
                        kstack.rsp = process.signal_stack.expect("the signal stack must be initialized").ss_sp as usize;
                    }

                    // Block the current signal if SA_NODEFER is not set
                    if !action.sa_flags.contains(SigActionFlags::SA_NODEFER) {
                        process.signal_mask.insert(SigMask::from(signal.0));
                    }

                    // Save utilities before the drop of the lock
                    let mem = process.memory.clone();
                    let action = action.clone();

                    // Drop the lock because if the stack is a Copy-on-write stack, the page
                    // fault handler will be executed and it uses this lock
                    // TODO: Faster workaround (do not clone memory and action)
                    drop(processes);

                    // Switch to the process' memory space to copy the values on the stack
                    mem.with(|| {
                        let mut writer = StackWriter { sp: kstack.rsp };

                        if action.sa_flags.contains(SigActionFlags::SA_SIGINFO) {
                            writer.push_slice(unsafe { &transmute::<SigInfo, [u8; size_of::<SigInfo>()]>(signal.clone().1) });
                            kstack.rsi = writer.sp;

                            // TODO: We are ignoring ucontext for now.
                            // kstack.rdx = ucontext;
                        } else {
                            kstack.rsi = 0;
                        }

                        // Restorer
                        if action.sa_flags.contains(SigActionFlags::SA_RESTORER) {
                            writer.push_slice(&[action.sa_restorer]);
                        } else {
                            // TODO: Sigreturn even if SA_RESTORER is not set (create a function
                            // doing the job)
                        }

                        kstack.rsp = writer.sp;
                    });

                    // Registers
                    kstack.rdi = signal.0 as usize;
                    kstack.rax = 0;
                    kstack.rip = action.sa_handler;

                    // Use a new SSE state
                    Fx::init();
                }
            }
        }

        if thread.first_time {
            // Init the SSE state
            Fx::init();
            SCHEDULER.get().current_mut().unwrap().first_time = false;
        } else {
            Fx::restore(thread.fx);
        }

        let fsbase = thread.fsbase.clone();
        let kstack = thread.kstack.clone();
        drop(thread);

        unsafe {
            // --- Executes the thread ---

            // Change fsbase
            FsBase::write(VirtAddr::new(fsbase));

            // Change the stack to use the new context
            llvm_asm!("mov rsp, rax" : : "{rax}"(kstack) : : "intel", "volatile");

            // Load the new context
            llvm_asm!("pop r11
                mov cr3, r11
                pop rbp
                pop r15
                pop r14
                pop r13
                pop r12
                pop r11
                pop r10
                pop r9
                pop r8
                pop rsi
                pop rdi
                pop rdx
                pop rcx
                pop rbx
                pop rax
                iretq"
                : : : : "intel", "volatile");
        }
    } else {
        loop {
            unsafe {
                llvm_asm!("sti; hlt; cli");
            }
        }
    }
    unreachable!();
}

/// Quickly jump to a process (used in the `execve` syscall)
pub fn switch_to(new: Tid) -> ! {
    SCHEDULER.get().set_current(new);
    let thread = SCHEDULER.get().current().unwrap();

    unsafe {
        // Change the stack to use the new context
        llvm_asm!("mov rsp, rax" : : "{rax}"(thread.kstack) : : "intel", "volatile");

        // Load the new context
        llvm_asm!("pop r11
            mov cr3, r11
            pop rbp
            pop r15
            pop r14
            pop r13
            pop r12
            pop r11
            pop r10
            pop r9
            pop r8
            pop rsi
            pop rdi
            pop rdx
            pop rcx
            pop rbx
            pop rax
            iretq"
            : : : : "intel", "volatile");
    }
    unreachable!();
}
