/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! The [Round Robin](https://en.wikipedia.org/wiki/Round-robin_scheduling) scheduling algorithm
use alloc::{
    collections::vec_deque::VecDeque,
    prelude::v1::Vec,
};

use crate::tasking::{
    algo::SchedulerAlgo,
    thread::{Thread, Tid},
};

/// A [Round Robin](https://en.wikipedia.org/wiki/Round-robin_scheduling) sheduler
pub struct RoundRobinScheduler {
    current: Tid,
    threads: VecDeque<Thread>,
}

impl RoundRobinScheduler {
    /// Create a new Round Robin scheduler
    pub fn new() -> Self {
        Self {
            current: Tid(0),
            threads: VecDeque::new(),
        }
    }
}

impl SchedulerAlgo for RoundRobinScheduler {
    fn add(&mut self, thread: Thread) { self.threads.push_back(thread); }

    fn choose(&mut self) -> Option<Thread> {
        // Get the next thread
        let thread = self.threads.pop_front();

        // Add the thread at the end of the queue
        if let Some(thread) = thread.clone() {
            self.current = thread.tid;
            self.threads.push_back(thread);
        }

        // Return the thread popped
        thread
    }

    fn contains(&self, tid: &Tid) -> bool {
        self.threads.iter().map(|t| t.tid).collect::<Vec<Tid>>().contains(tid)
    }

    fn remove(&mut self, tid: Tid) {
        assert!(!self.threads.is_empty());
        let mut index = 0;

        for (i, thread) in self.threads.iter().enumerate() {
            if thread.tid == tid {
                index = i;
                break;
            }
        }
        self.threads.remove(index);
    }

    fn current(&self) -> Option<&Thread> { self.get(self.current) }

    fn current_mut(&mut self) -> Option<&mut Thread> { self.get_mut(self.current) }

    fn set_current(&mut self, current: Tid) { self.current = current; }

    fn get(&self, tid: Tid) -> Option<&Thread> {
        self.threads.iter().find(|thread| thread.tid == tid)
    }

    fn get_mut(&mut self, tid: Tid) -> Option<&mut Thread> {
        self.threads.iter_mut().find(|thread| thread.tid == tid)
    }

    fn idle(&self) {
        loop {
            unsafe { llvm_asm!("sti; hlt; cli"); }
        }
    }
}
