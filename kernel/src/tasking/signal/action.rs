/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Signal actions
//! > A signal action is a handler function executed when a signal occurs.
use alloc::collections::btree_map::BTreeMap;
use lazy_static::lazy_static;
use bitflags::bitflags;
use core::fmt;

use crate::tasking::scheduler::exit;
use super::{
    Signal,
    mask::SigMask,
};

/// Use the default signal handler (see DEFAULT_ACTIONS)
pub const SIGDFL: SigHandler = 0;

/// Ignore the signal handler
pub const SIGIGN: SigHandler = 1;

/// The handler of a signal
pub type SigHandler = usize;

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone)]
/// The actions defining what to do when a signal is raised
pub struct SigActions (BTreeMap<Signal, SigAction>);

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
/// This struct defines a specific action for a signal
pub struct SigAction {
    /// Handler of the signal
    pub sa_handler: SigHandler,

    /// Flags of the signal
    pub sa_flags: SigActionFlags,

    /// Handler when the signal handler is done
    pub sa_restorer: usize,

    /// Mask of blocked signals during this signal
    pub sa_mask: SigMask,
}

bitflags! {
    /// The flags for sigaction
    pub struct SigActionFlags: usize {
        /// TODO
        const SA_NOCLDSTOP = 1 << 0;

        /// TODO
        const SA_NOCLDWAIT = 1 << 1;

        /// Toggle the use of a struct SigInfo as second argument in the signal handler
        const SA_SIGINFO   = 1 << 2;

        /// Toggle the use of a restorer function (a function called after the execution of the
        /// signal handler)
        const SA_RESTORER  = 1 << 26;

        /// Use an alternate stack during the signal handler
        const SA_ONSTACK   = 1 << 27;

        /// Is an interrupted syscall need to be restarted
        const SA_RESTART   = 1 << 28;

        /// Prevent the signal from being masked in the handler
        const SA_NODEFER   = 1 << 30;

        /// TODO
        const SA_RESETHAND = 1 << 31;
    }
}

lazy_static! {
    /// The default actions
    ///
    /// See [Wikipedia](https://en.wikipedia.org/wiki/Signal_(IPC)#Default_action)
    pub static ref DEFAULT_ACTIONS: SigActions = {
        let mut actions = BTreeMap::new();
        let terminate_action = SigAction {
            sa_handler: terminate as *const () as SigHandler,
            sa_flags: SigActionFlags::empty(),
            sa_restorer: 0,
            sa_mask: SigMask::empty(),
        };
        let continue_action = SigAction {
            sa_handler: cont as *const () as SigHandler,
            sa_flags: SigActionFlags::empty(),
            sa_restorer: 0,
            sa_mask: SigMask::empty(),
        };
        let stop_action = SigAction {
            sa_handler: stop as *const () as SigHandler,
            sa_flags: SigActionFlags::empty(),
            sa_restorer: 0,
            sa_mask: SigMask::empty(),
        };
        let ignore_action = SigAction {
            sa_handler: SIGIGN,
            sa_flags: SigActionFlags::empty(),
            sa_restorer: 0,
            sa_mask: SigMask::empty(),
        };

        actions.insert(Signal::SIGHUP, terminate_action);
        actions.insert(Signal::SIGINT, terminate_action);
        actions.insert(Signal::SIGQUIT, terminate_action);
        actions.insert(Signal::SIGILL, terminate_action);
        actions.insert(Signal::SIGTRAP, terminate_action);
        actions.insert(Signal::SIGABRT, terminate_action);
        actions.insert(Signal::SIGBUS, terminate_action);
        actions.insert(Signal::SIGFPE, terminate_action);
        actions.insert(Signal::SIGKILL, terminate_action);
        actions.insert(Signal::SIGUSR1, terminate_action);
        actions.insert(Signal::SIGSEGV, terminate_action);
        actions.insert(Signal::SIGUSR2, terminate_action);
        actions.insert(Signal::SIGPIPE, terminate_action);
        actions.insert(Signal::SIGALRM, terminate_action);
        actions.insert(Signal::SIGTERM, terminate_action);
        actions.insert(Signal::SIGCHLD, ignore_action);
        actions.insert(Signal::SIGCONT, continue_action);
        actions.insert(Signal::SIGSTOP, stop_action);
        actions.insert(Signal::SIGTSTP, stop_action);
        actions.insert(Signal::SIGTTIN, stop_action);
        actions.insert(Signal::SIGTTOUT, stop_action);
        actions.insert(Signal::SIGURG, ignore_action);
        actions.insert(Signal::SIGXCPU, terminate_action);
        actions.insert(Signal::SIGXFSZ, terminate_action);
        actions.insert(Signal::SIGVTALRM, terminate_action);
        actions.insert(Signal::SIGPROF, terminate_action);
        actions.insert(Signal::SIGWINCH, ignore_action);
        actions.insert(Signal::SIGIO, terminate_action);
        actions.insert(Signal::SIGPWR, terminate_action);
        actions.insert(Signal::SIGSYS, terminate_action);

        SigActions (actions)
    };
}

impl fmt::Debug for SigAction {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{{sa_handler={:#x}, sa_mask={:?}, sa_flags={:?}, sa_restorer={:#x}}}",
            self.sa_handler,
            self.sa_mask,
            self.sa_flags,
            self.sa_restorer)
    }
}

impl SigActions {
    /// Get the signal handler for a signal
    pub fn get(&self, sig: &Signal) -> Option<&SigAction> {
        self.0.get(sig)
    }

    /// Get a mutable reference of the signal handler for a signal
    pub fn get_mut(&mut self, sig: &Signal) -> Option<&mut SigAction> {
        self.0.get_mut(sig)
    }
}

impl Default for SigActions {
    fn default() -> Self {
        let mut actions = DEFAULT_ACTIONS.clone();

        for action in actions.0.iter_mut() {
            action.1.sa_handler = SIGDFL;
        }

        actions
    }
}

// --- Default functions ---

/// Terminates the current process
pub fn terminate() {
    println!("Terminate!");
    exit(true);
}

/// Resumes a stopped process
pub fn cont() {
    // TODO: Continue
}

/// Stops a process
pub fn stop() {
    // TODO: Stop
}
