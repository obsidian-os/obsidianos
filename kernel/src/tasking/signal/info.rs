/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Signal info
//! > If the SA_SIGINFO flag is set in a signal's action, the kernel should give informations about
//! the signal to the handler function.

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone)]
/// The structure used in signal handlers when the SA_SIGINFO flag is set
pub struct SigInfo {
    /// The number of the signal
    pub si_signo: usize,

    /// The error code
    pub si_errno: usize,

    /// The code of the signal
    pub si_code: usize,

    /// The address causing the fault
    pub si_addr: usize,

    // TODO:
    // *  Fill all fields in this structure
    // *  Implement all values of si_code
}

// --- SigInfo si_code values ---

// ----- Misc -----

/// Sent by kill, sigsend, raise
pub const SI_USER: usize     = 0;

/// Sent by the kernel from somewhere
pub const SI_KERNEL: usize   = 0x80;

/// Sent by siqueue
pub const SI_QUEUE: usize    = -1isize as usize;

/// Sent by timer expiration
pub const SI_TIMER: usize    = -2isize as usize;/* sent by timer expiration */

/// Sent by real time mesq state change
pub const SI_MESGQ: usize    = -3isize as usize;

/// Sent by AIO completion
pub const SI_ASYNCIO: usize  = -4isize as usize;/* sent by AIO completion */

/// Sent by queued SIGIO
pub const SI_SIGIO: usize    = -5isize as usize;

/// Sent by tkill system call
pub const SI_TKILL: usize    = -6isize as usize;

/// Sent by execve() killing subsidiary threads
pub const SI_DETHREAD: usize = -7isize as usize;

/// Sent by glibc async name lookup completion
pub const SI_ASYNCNL: usize  = -60isize as usize;

/// ----- SIGSEGV -----

/// Address not mapped to object
pub const SEGV_MAPERR: usize  = 1;

/// Invalid permissions for mapped object
pub const SEGV_ACCERR: usize  = 2;

/// Failed address bound checks
pub const SEGV_BNDERR: usize  = 3;

/// ADI not enabled for mapped object
pub const SEGV_ACCADI: usize  = 5;

/// Disrupting MCD error
pub const SEGV_ADIDERR: usize  = 6;

/// Precise MCD exception
pub const SEGV_ADIPERR: usize  = 7;

// --- / SigInfo si_code values ---
