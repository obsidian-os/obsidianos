/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Signal mask
//! > The signal mask is a bitmask of blocked signals. It is used to prevent the delivery of a
//! specific signal.
use bitflags::bitflags;

use super::Signal;

// `sigprocmask` actions

/// Block the signal
pub const SIG_BLOCK: usize   = 0;

/// Unblock the signal
pub const SIG_UNBLOCK: usize  = 1;

/// Set the signal mask
pub const SIG_SETMASK: usize = 2;

bitflags! {
    /// The mask of blocked signals
    pub struct SigMask: usize {
        #[allow(missing_docs)]
        const SIGHUP    = 1 << 0;

        #[allow(missing_docs)]
        const SIGINT    = 1 << 1;

        #[allow(missing_docs)]
        const SIGQUIT   = 1 << 2;

        #[allow(missing_docs)]
        const SIGILL    = 1 << 3;

        #[allow(missing_docs)]
        const SIGTRAP   = 1 << 4;

        #[allow(missing_docs)]
        const SIGABRT   = 1 << 5;

        #[allow(missing_docs)]
        const SIGBUS    = 1 << 6;

        #[allow(missing_docs)]
        const SIGFPE    = 1 << 7;

        #[allow(missing_docs)]
        const SIGKILL   = 1 << 8;

        #[allow(missing_docs)]
        const SIGUSR1   = 1 << 9;

        #[allow(missing_docs)]
        const SIGSEGV   = 1 << 10;

        #[allow(missing_docs)]
        const SIGUSR2   = 1 << 11;

        #[allow(missing_docs)]
        const SIGPIPE   = 1 << 12;

        #[allow(missing_docs)]
        const SIGALRM   = 1 << 13;

        #[allow(missing_docs)]
        const SIGTERM   = 1 << 14;

        #[allow(missing_docs)]
        const SIGSTKFLT   = 1 << 15;

        #[allow(missing_docs)]
        const SIGCHLD   = 1 << 16;

        #[allow(missing_docs)]
        const SIGCONT   = 1 << 17;

        #[allow(missing_docs)]
        const SIGSTOP   = 1 << 18;

        #[allow(missing_docs)]
        const SIGTSTP   = 1 << 19;

        #[allow(missing_docs)]
        const SIGTTIN   = 1 << 20;

        #[allow(missing_docs)]
        const SIGTTOUT  = 1 << 21;

        #[allow(missing_docs)]
        const SIGURG    = 1 << 22;

        #[allow(missing_docs)]
        const SIGXCPU   = 1 << 23;

        #[allow(missing_docs)]
        const SIGXFSZ   = 1 << 24;

        #[allow(missing_docs)]
        const SIGVTALRM = 1 << 25;

        #[allow(missing_docs)]
        const SIGPROF   = 1 << 26;

        #[allow(missing_docs)]
        const SIGWINCH  = 1 << 27;

        #[allow(missing_docs)]
        const SIGIO     = 1 << 28;

        #[allow(missing_docs)]
        const SIGPWR    = 1 << 29;

        #[allow(missing_docs)]
        const SIGSYS    = 1 << 30;

        #[allow(missing_docs)]
        const SIGRTMIN    = 1 << 31;

        #[allow(missing_docs)]
        const SIGRT_1    = 1 << 32;

        #[allow(missing_docs)]
        const SIGRT_2    = 1 << 33;

        #[allow(missing_docs)]
        const SIGRT_3    = 1 << 34;

        #[allow(missing_docs)]
        const SIGRT_4    = 1 << 35;

        #[allow(missing_docs)]
        const SIGRT_5    = 1 << 36;

        #[allow(missing_docs)]
        const SIGRT_6    = 1 << 37;

        #[allow(missing_docs)]
        const SIGRT_7    = 1 << 38;

        #[allow(missing_docs)]
        const SIGRT_8    = 1 << 39;

        #[allow(missing_docs)]
        const SIGRT_9    = 1 << 40;

        #[allow(missing_docs)]
        const SIGRT_10    = 1 << 41;

        #[allow(missing_docs)]
        const SIGRT_11    = 1 << 42;

        #[allow(missing_docs)]
        const SIGRT_12    = 1 << 43;

        #[allow(missing_docs)]
        const SIGRT_13    = 1 << 44;

        #[allow(missing_docs)]
        const SIGRT_14    = 1 << 45;

        #[allow(missing_docs)]
        const SIGRT_15    = 1 << 46;

        #[allow(missing_docs)]
        const SIGRT_16    = 1 << 47;

        #[allow(missing_docs)]
        const SIGRT_17    = 1 << 48;

        #[allow(missing_docs)]
        const SIGRT_18    = 1 << 49;

        #[allow(missing_docs)]
        const SIGRT_19    = 1 << 50;

        #[allow(missing_docs)]
        const SIGRT_20    = 1 << 51;

        #[allow(missing_docs)]
        const SIGRT_21    = 1 << 52;

        #[allow(missing_docs)]
        const SIGRT_22    = 1 << 53;

        #[allow(missing_docs)]
        const SIGRT_23    = 1 << 54;

        #[allow(missing_docs)]
        const SIGRT_24    = 1 << 55;

        #[allow(missing_docs)]
        const SIGRT_25    = 1 << 56;

        #[allow(missing_docs)]
        const SIGRT_26    = 1 << 57;

        #[allow(missing_docs)]
        const SIGRT_27    = 1 << 58;

        #[allow(missing_docs)]
        const SIGRT_28    = 1 << 59;

        #[allow(missing_docs)]
        const SIGRT_29    = 1 << 60;

        #[allow(missing_docs)]
        const SIGRT_30    = 1 << 61;

        #[allow(missing_docs)]
        const SIGRT_31    = 1 << 62;

        #[allow(missing_docs)]
        const SIGRT_32    = 1 << 63;
    }
}

impl From<Signal> for SigMask {
    fn from(val: Signal) -> Self {
        use core::mem::transmute;

        unsafe { transmute::<usize, Self>(1 << (val as usize - 1)) }
    }
}
