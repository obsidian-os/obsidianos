/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Signals management
//! *  Signal queues
//! *  Signal actions (handlers defined when a signal occurs)
use alloc::collections::vec_deque::VecDeque;
use core::option::NoneError;

use super::{
    scheduler::{
        SCHEDULER,
        PROCESSES,
    },
    process::Pid,
};
use self::{
    info::SigInfo,
    mask::SigMask,
};

pub mod action;
pub mod mask;
pub mod info;

/// A queue of signals
pub type SignalQueue = VecDeque<(Signal, SigInfo)>;

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone, Copy)]
/// A signal stack used during a signal handler
pub struct SignalStack {
    /// The address of the stack
    pub ss_sp: u64,

    /// The flags of the stack
    pub ss_flags: u32,

    /// The size of the stack
    pub ss_size: usize,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone, Copy)]
#[repr(usize)]
/// The list of all Linux signals
///
/// Taken from both [Wikipedia](https://en.wikipedia.org/wiki/Signal_(IPC))
/// and [the Libc manual](https://www.gnu.org/software/libc/manual/)
pub enum Signal {
    /// Signals that the virtual terminal is closed
    SIGHUP = 1,

    /// Signals that the user wishes to interrupt the process
    SIGINT,

    /// Signals that the process should quit and perform a core dump
    SIGQUIT,

    /// Signals that the process attempts to execute an illegal, malformed, unknown, or privileged instruction
    SIGILL,

    /// Signals that a trap occurs. This signal is mostly raised by the `breakpoint` instruction
    SIGTRAP,

    /// Signals that the process must abort
    SIGABRT,

    /// Signals that a bus error is happening (invalid access to physical memory)
    SIGBUS,

    /// Signals that the process attempts to execute an erroneous arithmetic operation
    SIGFPE,

    /// Signals that the process MUST terminate immediately (without clean-up)
    SIGKILL,

    /// Do whatever you want with this signal
    SIGUSR1,

    /// Signals that the process attempts to use an invalid virtual memory reference
    SIGSEGV,

    /// Do whatever you want with this signal
    SIGUSR2,

    /// Signals that the process attempts to write to a pipe without a process connected to the other end
    SIGPIPE,

    /// Signals that a time limit is ended
    SIGALRM,

    /// Signals that the process should terminate (unlike SIGKILL, it can clean-up)
    SIGTERM,

    /// Signals a stack overflow
    SIGSTKFLT,

    /// Signals that a child process is terminated
    SIGCHLD,

    /// Resumes a stopped process
    SIGCONT,

    /// Stops a process
    SIGSTOP,

    /// A terminal has requested that a process stops
    SIGTSTP,

    /// Attemps to read from a terminal which is in background
    SIGTTIN,

    /// Attemps to write to a terminal which is in background
    SIGTTOUT,

    /// Happens when a socket has urgent datas
    SIGURG,

    /// Happens when a process has used up the CPU for a too long duration
    SIGXCPU,

    /// Happens when a process create a too big file
    SIGXFSZ,

    /// Signals that a virtual timer has expired
    SIGVTALRM,

    /// Signals that the given time for both the process and the system is elapsed
    SIGPROF,

    /// Signals that the window has changed its size
    SIGWINCH,

    /// Signals that a file descriptor is ready to be read or write. This is mostly used for
    /// sockets
    SIGIO,

    /// Signals that a power failure is happening
    SIGPWR,

    /// Signals that the syscall number is invalid
    SIGSYS,
}

impl From<usize> for Signal {
    fn from(val: usize) -> Self {
        use core::mem::transmute;

        unsafe { transmute::<usize, Self>(val) }
    }
}

/// Helper functions to send a signal to the current process
pub fn send_signal(sig: Signal, info: SigInfo) -> Result<(), NoneError> {
    let mut processes = PROCESSES.write();
    let thread = SCHEDULER.get().current()?;
    processes.get_mut(&thread.process)?.signals.push_back(
        (sig, info)
    );

    Ok(())
}

/// Helper functions to send a signal to a specific process
pub fn send_signal_to(pid: Pid, sig: Signal, info: SigInfo) -> Result<(), NoneError> {
    let mut processes = PROCESSES.write();
    let process = processes.get_mut(&pid)?;

    if !process.signal_mask.contains(SigMask::from(sig)) {
        process.signals.push_back(
            (sig, info)
        );
    }

    Ok(())
}
