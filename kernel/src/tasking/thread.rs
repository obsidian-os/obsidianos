/*
 * Copyright (C) 2020-2021 Nicolas Fouquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses.
 */
//! Definition of a thread which will be chosen (by the scheduler) and executed
use alloc::{
    prelude::v1::Box,
    alloc::{
        Layout,
        GlobalAlloc,
    },
};
use core::mem::{size_of, transmute};

use super::{
    context::Registers,
    info::StackWriter,
    process::Pid,
    scheduler::SCHEDULER,
};
use crate::{
    gdt,
    memory::heap::HEAP_ALLOCATOR,
};

const KSTACK_SIZE: usize = 4096;

/// The size of the Streaming SIMD Extensions area
pub const FX_SIZE: usize = 512;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
/// A thread ID
pub struct Tid(pub usize);

impl Tid {
    /// Get a new PID
    pub fn get_new() -> Self {
        Self((0..)
            .find(|i| !SCHEDULER.get().contains(&Tid(*i)))
            .unwrap())
    }
}

#[derive(Debug, Clone)]
/// A thread which is chosen by the scheduler
pub struct Thread {
    /// The thread ID
    pub tid: Tid,

    /// Whether or not it is the first time that the thread is chosen by the scheduler
    pub first_time: bool,

    /// The process which have this thread
    pub process: Pid,

    /// Kernel stack used to store the thread's context
    pub kstack: usize,

    /// The save of the SSE state
    pub fx: u64,

    /// The fs base
    pub fsbase: u64,

    // TODO: Signal mask?
}

impl Thread {
    /// Create a new Thread
    pub fn new(process: Pid, rip: usize, cr3: usize) -> Self {
        let kstack_pointer =
            Box::into_raw(unsafe { Box::from_raw(HEAP_ALLOCATOR.alloc(Layout::from_size_align_unchecked(KSTACK_SIZE, 4096)) as *mut [u8; KSTACK_SIZE]) }) as u64 + KSTACK_SIZE as u64;
        let fx =
            Box::into_raw(unsafe { Box::from_raw(HEAP_ALLOCATOR.alloc(Layout::from_size_align_unchecked(FX_SIZE, 16)) as *mut [u8; FX_SIZE]) }) as u64;

        // Create a new set of registers
        let mut regs = Registers::default();

        // Create a new stack frame (without rsp)
        regs.rip = rip;
        regs.rflags = 1 << 9;
        regs.cs = gdt::GDT_USER_CODE << 3 | 3;
        regs.ds = gdt::GDT_USER_DATA << 3 | 3;
        regs.cr3 = cr3;

        // Push the registers in the kernel's stack
        let mut writer = StackWriter {
            sp: kstack_pointer as usize,
        };
        writer.push_slice(unsafe {
            &transmute::<Registers, [usize; size_of::<Registers>() / 8]>(regs)
        });
        let sp = writer.sp;

        Self {
            tid: Tid::get_new(),
            first_time: true,
            process,
            kstack: sp,
            fx,
            fsbase: 0x0,
        }
    }

    /// Translate the kernel stack into a struct Registers
    pub fn get_kstack(&mut self) -> &mut Registers {
        unsafe { &mut *(self.kstack as *mut Registers) }
    }

    /// Create a new Thread used during the `fork` syscall
    pub fn fork(process: Pid, mut regs: Registers, fsbase: u64) -> Self {
        let kstack_pointer =
            Box::into_raw(unsafe { Box::from_raw(HEAP_ALLOCATOR.alloc(Layout::from_size_align_unchecked(KSTACK_SIZE, 4096)) as *mut [u8; KSTACK_SIZE]) }) as u64 + KSTACK_SIZE as u64;
        let fx =
            Box::into_raw(unsafe { Box::from_raw(HEAP_ALLOCATOR.alloc(Layout::from_size_align_unchecked(FX_SIZE, 16)) as *mut [u8; KSTACK_SIZE]) }) as u64;

        // Set 0 in rax (return value of the `fork` syscall in the child process)
        regs.rax = 0;

        // Push the registers in the kernel's stack
        let mut writer = StackWriter {
            sp: kstack_pointer as usize,
        };
        writer.push_slice(unsafe {
            &transmute::<Registers, [usize; size_of::<Registers>() / 8]>(regs)
        });
        let sp = writer.sp;

        Self {
            tid: Tid::get_new(),
            first_time: true,
            process,
            kstack: sp,
            fx,
            fsbase,
        }
    }

    /// Create a new Thread used during the `clone` syscall
    pub fn clone_(process: Pid, mut regs: Registers, fsbase: u64, stack_pointer: usize) -> Self {
        let kstack_pointer =
            Box::into_raw(unsafe { Box::from_raw(HEAP_ALLOCATOR.alloc(Layout::from_size_align_unchecked(KSTACK_SIZE, 4096)) as *mut [u8; KSTACK_SIZE]) }) as u64 + KSTACK_SIZE as u64;
        let fx =
            Box::into_raw(unsafe { Box::from_raw(HEAP_ALLOCATOR.alloc(Layout::from_size_align_unchecked(FX_SIZE, 16)) as *mut [u8; KSTACK_SIZE]) }) as u64;

        // Set 0 in rax (return value of the `fork` syscall in the child process)
        regs.rax = 0;

        // Set rsp to the stack_pointer
        regs.rsp = stack_pointer;

        // Push the registers in the kernel's stack
        let mut writer = StackWriter {
            sp: kstack_pointer as usize,
        };
        writer.push_slice(unsafe {
            &transmute::<Registers, [usize; size_of::<Registers>() / 8]>(regs)
        });
        let sp = writer.sp;

        Self {
            tid: Tid::get_new(),
            first_time: true,
            process,
            kstack: sp,
            fx,
            fsbase,
        }
    }

    /// Exit the thread
    ///
    /// This function deallocates the kernel stack and the SSE area
    pub fn exit(&self) {
        unsafe {
            HEAP_ALLOCATOR.dealloc((self.kstack - KSTACK_SIZE + size_of::<Registers>()) as *mut u8, Layout::from_size_align_unchecked(KSTACK_SIZE, 4096));
            HEAP_ALLOCATOR.dealloc((self.fx) as *mut u8, Layout::from_size_align_unchecked(FX_SIZE, 16));
        }
    }
}
