#! /bin/sh
# Copyright (C) 2020-2021 Nicolas Fouquet

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see https://www.gnu.org/licenses.

# Update packages
apt-get update

mknod /dev/loop0 b 7 0

# Install Qemu, the KVM plugin for Qemu, an assembler: NASM and the LLVM linker: lld
apt-get install -y qemu qemu-kvm nasm xorriso make build-essential lld grub2-common wget ca-certificates gcc fdisk
DEBIAN_FRONTEND=noninteractive \
apt-get \
-o Dpkg::Options::="--force-confnew" \
--force-yes \
-fuy \
install grub-pc

# Install Rust
export RUSTUP_HOME=/usr/local/rustup
export CARGO_HOME=/usr/local/cargo
export PATH=/usr/local/cargo/bin:$PATH
export URL="https://static.rust-lang.org/rustup/dist/x86_64-unknown-linux-gnu/rustup-init"
wget "$URL"
chmod +x rustup-init
./rustup-init -y --no-modify-path --default-toolchain nightly
rm rustup-init
chmod -R a+w $RUSTUP_HOME $CARGO_HOME
rustup --version
cargo --version
rustc --version

# Add the user in the KVM group
addgroup $USER kvm

# Add the rust-src component
rustup component add rust-src

# Install Xargo
cargo install xargo
xargo --version

# Add the musl target
rustup target add x86_64-unknown-linux-musl

# Run tests
make test_ci

# Make an ISO
make clean user compile compile_rust_normal link cdrom_ci
