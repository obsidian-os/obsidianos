#! /bin/sh
# Copyright (C) 2020-2021 Nicolas Fouquet

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see https://www.gnu.org/licenses.

if [ -z "${os}" ] && [ -z "${dist}" ]
then
  # some systems dont have lsb-release yet have the lsb_release binary and
  # vice-versa
  if [ -e /etc/lsb-release ]; then
    . /etc/lsb-release

    if [ "${ID}" = "raspbian" ]; then
      os=${ID}
      dist=`cut --delimiter='.' -f1 /etc/debian_version`
    else
      os=${DISTRIB_ID}
      dist=${DISTRIB_CODENAME}

      if [ -z "$dist" ]; then
        dist=${DISTRIB_RELEASE}
      fi
    fi

  elif [ `which lsb_release 2>/dev/null` ]; then
    dist=`lsb_release -c | cut -f2`
    os=`lsb_release -i | cut -f2 | awk '{ print tolower($1) }'`

  elif [ -e /etc/debian_version ]; then
    # some Debians have jessie/sid in their /etc/debian_version
    # while others have '6.0.7'
    os=`cat /etc/issue | head -1 | awk '{ print tolower($1) }'`
    if grep -q '/' /etc/debian_version; then
      dist=`cut --delimiter='/' -f1 /etc/debian_version`
    else
      dist=`cut --delimiter='.' -f1 /etc/debian_version`
    fi

  else
    unknown_os
  fi
fi

if [ -z "$dist" ]; then
  unknown_os
fi
# remove whitespace from OS and dist name
os=$(echo "${os}" | sed "s/ //")
dist=$(echo "${dist}" | sed "s/ //")

echo "Detected operating system as $os/$dist."
if [ "$os" != "Ubuntu" ] && [ "$os" != "debian" ]
then
  echo "Sorry! This operating system is not supported. Try with Debian or with
  Ubuntu"
else
  # Install Qemu, the KVM plugin for Qemu and an assembler: NASM
  sudo apt-get install qemu qemu-kvm nasm xorriso make build-essential fdisk

  # Add the user in the KVM group
  sudo addgroup $USER kvm

  # Install Rustup
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

  # Add the cargo bin's directory in the PATH
  . ~/.cargo/env

  # Install and set the nightly toolchain as the default toolchain
  rustup toolchain install nightly
  rustup override set nightly

  # Install Xargo
  cargo install xargo

  # Add the rust-src component
  rustup component add rust-src

  # Install ld.lld (the package name change between the OSes)
  if [ "$os" = "Ubuntu" ]
  then
    # Install ld.lld-6.0 in Ubuntu
    sudo apt-get install lld-6.0
  else
    # Try to install the generic package
    sudo apt-get install lld
  fi
fi
make
