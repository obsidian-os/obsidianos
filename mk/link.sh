#! /bin/sh
# Copyright (C) 2020-2021 Nicolas Fouquet

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see https://www.gnu.org/licenses.

# This script is used to choose the good linker for the current OS
if [ -z "${os}" ] && [ -z "${dist}" ]
then
  # some systems dont have lsb-release yet have the lsb_release binary and
  # vice-versa
  if [ -e /etc/lsb-release ]; then
    . /etc/lsb-release

    if [ "${ID}" = "raspbian" ]; then
      os=${ID}
      dist=`cut --delimiter='.' -f1 /etc/debian_version`
    else
      os=${DISTRIB_ID}
      dist=${DISTRIB_CODENAME}

      if [ -z "$dist" ]; then
        dist=${DISTRIB_RELEASE}
      fi
    fi

  elif [ `which lsb_release 2>/dev/null` ]; then
    dist=`lsb_release -c | cut -f2`
    os=`lsb_release -i | cut -f2 | awk '{ print tolower($1) }'`

  elif [ -e /etc/debian_version ]; then
    # some Debians have jessie/sid in their /etc/debian_version
    # while others have '6.0.7'
    os=`cat /etc/issue | head -1 | awk '{ print tolower($1) }'`
    if grep -q '/' /etc/debian_version; then
      dist=`cut --delimiter='/' -f1 /etc/debian_version`
    else
      dist=`cut --delimiter='.' -f1 /etc/debian_version`
    fi

  else
    unknown_os
  fi
fi

if [ -z "$dist" ]; then
  unknown_os
fi
# remove whitespace from OS and dist name
os=$(echo "${os}" | sed "s/ //")
dist=$(echo "${dist}" | sed "s/ //")

result_type="$1"
start="$2"
end="$3"

if [ "$os" = "Ubuntu" ]
then
  # Use ld.lld-6.0 in Ubuntu 18.04
  if [ "$result_type" = "kernel" ]
  then
    ld.lld-6.0 build/asm/* build/libobsidianos_kernel.a -o build/kernel.bin -m elf_x86_64 -error-limit=0 -T kernel/src/arch/x86_64/linker.ld
  else
    ld.lld-6.0 -entry main -m elf_x86_64 "$start" -o "$end"
  fi
else
  # Try to use the generic package
  if [ "$result_type" = "kernel" ]
  then
    ld.lld build/asm/* build/libobsidianos_kernel.a -o build/kernel.bin -m elf_x86_64 -error-limit=0 -T kernel/src/arch/x86_64/linker.ld
  else
    ld.lld -entry main -m elf_x86_64 "$start" -o "$end"
  fi
fi
