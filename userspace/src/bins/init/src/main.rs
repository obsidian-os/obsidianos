use std::{
    fs::File,
    io::{Read, Write},
};
use driver_lib::packet::Packet;
use driver_lib::action_number::*;

fn main() {
    println!("Init process launched!");
    println!("Try to create a file and to write 4×42 to it.");
    let mut file = File::create("/bin/test.txt").unwrap();
    let data = &[42; 4];
    file.write(data).unwrap();
    drop(file);

    println!("Try to read the created file.");
    let mut file = File::open("/bin/test.txt").unwrap();
    let buf = &mut [0; 4];
    file.read(buf).unwrap();
    println!("Result: {:?}", buf);

    println!("Try to write a file in a non-existent directory, this should fail.");
    if let Err(e) = File::create("/etc/file.txt") {
        println!("Error received: {:?}", e);
    } else {
        println!("It should fail... but it works.");
    }

    println!("Try to write to the PCI socket");
    let mut socket = File::open("/tmp/pci-socket").unwrap();

    socket.write(&Packet::new(
        READ,
        100,
        66,
        42,
        10,
        101,
    )).unwrap();
}
