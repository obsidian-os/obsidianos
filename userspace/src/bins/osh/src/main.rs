use std::io::{self, Read, Write};
use std::process::Command;

const KEY_BACKSPACE: u8 = 0x8;
const KEY_ENTER: u8 = 0x0a;

fn main() {
    loop {
        print!("osh>");
        io::stdout()
            .flush()
            .unwrap();

        let mut line = String::new();
        loop {
            let mut input = [0];
            match io::stdin().read(&mut input) {
                Ok(_) => {
                    let key = input[0];
                    if key != 0 {
                        if key == KEY_ENTER {
                            let mut iter = line.split(" ");
                            if let Some(bin) = iter.next() {
                                if bin != String::from("") {
                                    // TODO: Handle non existing programs
                                    println!("");
                                    Command::new(bin)
                                        .args(iter)
                                        .spawn()
                                        .unwrap();
                                    break;
                                } else {
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else if key == KEY_BACKSPACE {
                            line.pop();
                            print!("{}", key as char);
                            io::stdout()
                                .flush()
                                .unwrap();
                        } else {
                            line.push(key as char);
                            print!("{}", key as char);
                            io::stdout()
                                .flush()
                                .unwrap();
                        }
                    }
                }
                Err(error) => println!("error: {}", error),
            }
        }
    }
}
