use std::{
    fs::File,
    io::Read,
};
use libc::*;

use driver_lib::packet::Packet;

fn main() {
    println!("pci-driver launch");

    unsafe { mkfifo("/tmp/pci-socket\0" as *const _ as *const i8, 0o666) };

    let mut socket = File::open("/tmp/pci-socket").unwrap();

    loop {
        let mut buf = Packet::default();
        if socket.read(&mut buf).unwrap() != 0 {
            println!("{:#?}", buf);
        }
    }
}
