#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  // This program maps the /dev/mem character device to memory to read things
  // from the physical memory
  int fd = open("/dev/mem", O_RDONLY|O_SYNC);
  unsigned char *mem = mmap(NULL, 4096, PROT_READ, MAP_SHARED, fd, 0x0);

  printf("Value read from 0x0 in physical memory: 0x%x\n", (int)mem[0]);

  return 0;
}
