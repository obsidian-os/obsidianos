use std::{
    ops::{Deref, DerefMut},
    slice,
    mem,
};

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Default)]
pub struct Packet {
    cmd: usize,
    arg1: usize,
    arg2: usize,
    arg3: usize,
    arg4: usize,
    arg5: usize,
}

impl Packet {
    pub fn new(cmd: usize, arg1: usize, arg2: usize, arg3: usize, arg4: usize, arg5: usize) -> Self {
        Self {
            cmd,
            arg1,
            arg2,
            arg3,
            arg4,
            arg5
        }
    }
}

impl Deref for Packet {
    type Target = [u8];
    fn deref(&self) -> &[u8] {
        unsafe {
            slice::from_raw_parts(self as *const Packet as *const u8, mem::size_of::<Packet>()) as &[u8]
        }
    }
}

impl DerefMut for Packet {
    fn deref_mut(&mut self) -> &mut [u8] {
        unsafe {
            slice::from_raw_parts_mut(self as *mut Packet as *mut u8, mem::size_of::<Packet>()) as &mut [u8]
        }
    }
}
